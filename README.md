# OpenGL Tiled Deferred Renderer

![picture alt](screenshot.png)

OpenGL Tiled Deferred Renderer library. This project can be used as a standalone Dll with only glm as a dependency which can be found [here](https://github.com/g-truc/glm). Currently only the
windows based build is supported. An example project using this library can be found [here](https://gitlab.com/jamesadams800/tiled-renderer-demo)

The Features of this project include.

* Dynamic Shader recompilation.
* Normal Mapping.
* Tiled deferred rendering with support for:
  * Blinn Phong BRDF lighting.
  * Point light sources.
  * Directional light sources.
  * Frustum based light culling.
  * AABB based light culling.
  * 2.5D culling discussed [here](https://www.slideshare.net/takahiroharada/a-25d-culling-for-forward-siggraph-asia-2012)
* Cascaded shadow maps:
  * Variable kernel size PCF.
  * Shadow interpolation.
* High-dynamic-range rendering using:
  * Reinhard Tone mapping.
  * Filmic Tone mapping as discussed [here](https://www.gdcvault.com/play/1012351/Uncharted-2-HDR).
* Bloom
* FXAA
          
To use this library. Simply inherit from GLApplication and implement the pure virtuals, instanciate the class 
and call "Run()".

```C++

class ExampleApplication : public renderer::GLApplication {

  ...

  void InitScene(Scene& scene) override {
    // Do some scene initialisation here.
  }

  void Update(Scene& scene, float dt) override {
    // Do some scene updating here...
  }

}

void Main() {
  ExampleApplication app;
  app.Run();
}

```

This project uses Open Asset Import Library to allow for assets to be imported into the scene.  This can be done
using the "SceneLoader" utlities found in "SceneLoader.h".  

```C++
  SceneLoader::LoadScene("models\\sponza.obj", scene, SceneLoader::ASSIMP);
```

TODO: 
* Implement builds for other platforms.
* PBR Asset loading.
* PBR based pipeline.
* Add spot lights and spot light shadows.
* Various optimizations.