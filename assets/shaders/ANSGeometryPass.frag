#version 450
// In
layout(location = 0) in vec3 world_position;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec2 tex_coord;
layout(location = 3) in vec3 tangent;
layout(location = 4) in vec3 bitangent;

layout(binding = 0) uniform sampler2D albedo_map;
layout(binding = 1) uniform sampler2D specular_map;
layout(binding = 2) uniform sampler2D normal_map;

layout(std140) uniform MaterialConstants {
  vec4 diffuse;
  vec4 specular;
  vec4 emissive;
  float shininess_exp;
} constants;

// Out
layout(location = 0) out vec3 out_position;
layout(location = 1) out vec3 out_normal;
layout(location = 2) out vec4 out_albedo;
layout(location = 3) out vec4 out_specular;

void main() {
  // World space positions.
  out_position = world_position;

  // World space Normal
  vec3 tangent_normal = texture(normal_map, tex_coord).rgb;
  tangent_normal = normalize(tangent_normal * 2.0 - 1.0);
  mat3 world_transform = mat3(tangent, bitangent, normal);
  out_normal = normalize(world_transform * tangent_normal.xyz);
  
  // Albedo
  vec3 albedo = texture(albedo_map, tex_coord).rgb;
  float specular = texture(specular_map, tex_coord).r;
  out_albedo = vec4(albedo.xyz, 1.0f);
  out_specular = vec4(specular.rrr, constants.shininess_exp);
}