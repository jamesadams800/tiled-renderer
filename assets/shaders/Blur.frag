#version 450

#define MAX_SUPPORTED_ATTACHMENT 8
#define MAX_KERNEL_SIZE 100

// IN 
layout( location = 0 ) in vec2 in_tex_coord;
layout( binding = 0 ) uniform sampler2D input_texture[MAX_SUPPORTED_ATTACHMENT];

uniform int pass;
uniform int input_attachment;
uniform int output_attachment;

layout (std140) uniform BlurKernel {
  uint kernel_size;
  float kernel[MAX_KERNEL_SIZE];
};

// OUT
layout( location = 0 ) out vec4 channel[MAX_SUPPORTED_ATTACHMENT];;

void main() {
  vec3 total = vec3(0.0f);
  vec2 texel_offset = 1.0f / textureSize(input_texture[input_attachment], 0);
  int kernel_offset = -int(kernel_size / 2);
  if(pass == 1) {
    // Horizontal blur.
    for(int i = 0; i < int(kernel_size); i++) {
      vec2 offset_coord = vec2(in_tex_coord.x + texel_offset.x * float(i + kernel_offset), in_tex_coord.y);
      total += texture(input_texture[input_attachment], offset_coord).rgb * kernel[i];
    }
  }
  else {
    // Vertical blur.
    for(int i = 0; i < int(kernel_size); i++) {
      float inner_offset = float(i) + float(kernel_offset);
      vec2 offset_coord = vec2(in_tex_coord.x, in_tex_coord.y + texel_offset.y * inner_offset);
      total += texture(input_texture[input_attachment], offset_coord).rgb * kernel[i];
    }
  }
 
  channel[output_attachment] = vec4(total, 1.0f);
}