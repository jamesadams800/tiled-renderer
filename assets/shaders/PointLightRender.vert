#version 450

// IN
layout( location = 0 ) in vec3 position;

struct PointLight {
  mat4 model_matrix;
  vec4 color;
  vec4 position;
  float radius;
  float linear_attenuation;
  float quadratic_attenuation;
};

layout(std140) buffer PointLights {
  PointLight lights[];
};

uniform mat4 vp_matrix;

layout( location = 0 ) flat out uint id;

void main() {
  id = gl_InstanceID;
  gl_Position = vp_matrix * lights[gl_InstanceID].model_matrix * vec4(position, 1.0);
}