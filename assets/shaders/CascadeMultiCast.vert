#version 450
#extension GL_NV_viewport_array2 : enable
#extension GL_AMD_vertex_shader_viewport_index : enable

// IN
layout( location = 0 ) in vec3 position;

uniform mat4 model_matrix;

// Cascade uniforms.
#define MAX_CASCADES 4
#define MAX_SHADOW_MAP 5
struct CascadeData {
  float camera_far_planes[MAX_CASCADES];
  mat4 cascade_view_projection[MAX_CASCADES];
  vec2 cascade_map_dimensions;
};

layout(std140) uniform CascadeShadowData {
  CascadeData cascade_data[MAX_SHADOW_MAP];
};

uniform int light_index;

void main() {
  mat4 light_view_projection = cascade_data[light_index].cascade_view_projection[gl_InstanceID];
  gl_ViewportIndex = gl_InstanceID;
  gl_Position = light_view_projection * model_matrix * vec4(position, 1.0);
}