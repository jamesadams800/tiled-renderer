#version 450

// IN
#define TILE_SIZE_X 16
#define TILE_SIZE_Y 16
layout(local_size_x = TILE_SIZE_X, local_size_y = TILE_SIZE_Y) in;

// Tile Data.
struct TileData {
  vec4 frustum_planes[4];
  vec4 aabb_max;
  vec4 aabb_min;
  uint tile_mask;
};

layout(std140) buffer TileDataBuffer {
  TileData tile_buffer[];
};

// Point Light input Uniforms
#define TILE_MAX_POINT_LIGHT 2048
struct PointLight {
  mat4 model_matrix;
  vec4 color;
  vec4 position;
  float radius;
  float linear_attenuation;
  float quadratic_attenuation;
};

layout(std140) buffer PointLights {
  PointLight point_lights[];
};

shared uint tile_point_light_indices[TILE_MAX_POINT_LIGHT];
shared uint tile_point_light_count;
// Directional light input uniforms.
#define MAX_CASCADES 4
#define MAX_SHADOW_MAP 5

struct DirectionalLight {
  vec4 direction;
  vec4 color;
  bool has_shadow;
};

layout(std140) buffer DirectionalLights {
  DirectionalLight directional_lights[];
};

// Cascade uniforms.
struct CascadeData {
  float camera_far_planes[MAX_CASCADES];
  mat4 cascade_view_projection[MAX_CASCADES];
  vec2 cascade_map_size;
};

layout(std140) uniform CascadeShadowData {
  CascadeData cascade_data[MAX_SHADOW_MAP];
};

// Camera uniforms.
uniform vec4 world_camera_position;
uniform mat4 camera_view_matrix;
uniform mat4 camera_proj_matrix;
uniform mat4 inv_camera_view_matrix;
uniform mat4 inv_camera_proj_matrix;

layout(binding = 0) uniform sampler2D positions_map;
layout(binding = 1) uniform sampler2D normal_map;
layout(binding = 2) uniform sampler2D albedo_map;
layout(binding = 3) uniform sampler2D specular_map;
layout(binding = 4) uniform sampler2D depth_map;
layout(binding = 5) uniform sampler2D cascade_shadow_maps[MAX_SHADOW_MAP];

// Output image.
layout(binding = 0, rgba16f) restrict writeonly uniform image2D out_color;

// Credit to http://theorangeduck.com/page/avoiding-shader-conditionals
// for these branchless conditional functions. 
float WhenGt(float x, float y) {
  return max(sign(x - y), 0.0);
}

///////////////////////////////////////////////////////////////////////////////////////////////////
// Lighting Functions.
///////////////////////////////////////////////////////////////////////////////////////////////////
float Attenutate(float linear_attenuation, float quadratic_attenuation, float distance) {
  return 1.0f / 
        (1.0f + (linear_attenuation * distance) +
        (quadratic_attenuation * (distance * distance)));
}

vec3 BlinnPhongBrdf( in vec3 v, in vec3 l, in vec3 h, in vec3 n,
                     in vec3 albedo, in float specularity, in float shininess, in float energy_conservation ) {                
  // Diffuse coefficent.
  float diff_coef = max( dot(l, n), 0.0f);
  // Specular coefficient. Using Blinn phong model.
  float spec_coef = WhenGt(diff_coef, 0.0f) * energy_conservation * 
                    pow(max( dot(n, h), 0.0f), shininess);
  return (diff_coef * albedo) + (specularity * spec_coef);
} 

vec3 LambertianBrdf( in vec3 v, in vec3 l, in vec3 n, in vec3 albedo ) {                
  // Diffuse coefficent.
  float diff_coef = max( dot(l, n), 0.0f);
  return (diff_coef * albedo) ;
}  

///////////////////////////////////////////////////////////////////////////////////////////////////
// Shadow map Utility Functions.
///////////////////////////////////////////////////////////////////////////////////////////////////
vec2 CascadeMapCoords( in vec2 screen_space_coords, in uint cascade_index ) {
  const float x_offset = float(cascade_index) / float(MAX_CASCADES);
  const float x_scale = 1.0f / float(MAX_CASCADES);
  return vec2( ((screen_space_coords.x * x_scale) + x_offset), screen_space_coords.y );
}

float InterpolatedSample( in sampler2D shadow_map, in vec2 coords, in float depth) {
  vec2 map_size = textureSize(shadow_map, 0);
  vec2 texel_size = 1.0f / map_size;
  vec2 t = fract(coords * map_size + 0.5);
  vec2 center_uv = floor(coords * map_size + 0.5) / map_size;

  float lb = step( depth, texture(shadow_map, center_uv + texel_size * vec2(0.0, 0.0)).r);
  float lt = step( depth, texture(shadow_map, center_uv + texel_size * vec2(0.0, 1.0)).r);
  float rb = step( depth, texture(shadow_map, center_uv + texel_size * vec2(1.0, 0.0)).r);
  float rt = step( depth, texture(shadow_map, center_uv + texel_size * vec2(1.0, 1.0)).r);
  float dy1 = mix(lb, lt, t.y);
  float dy2 = mix(rb, rt, t.y);
  float shadow_factor = mix(dy1, dy2, t.x);
  return shadow_factor;
}

float PcfShadowed(in mat4 cascade_view_proj, 
                  in sampler2D shadow_map,
                  in vec2 cascade_map_size,
                  in uint cascade,
                  in vec3 position,
                  in int kernel_dim = 3) {
  vec4 ndc_space = cascade_view_proj * vec4(position, 1.0f);
  vec3 clip_space = ndc_space.xyz / ndc_space.w;
  vec3 screen_coords = clip_space * 0.5f + 0.5f;
  vec2 cascade_coord = CascadeMapCoords(screen_coords.xy, cascade );
  // The further away from the camera the fragment is the larger the amount of bias used.
  //const float bias = screen_coords.z * max(0.05 * (1.0 - dot(surface_normal, light_direction)), 0.001);
  const float bias = 0.005f;//max(0.0005f * dot(surface_normal, light_direction), 0.008f); 
  const float depth_adjusted_bias = max(screen_coords.z * bias, 0.005f);
  
  float shadow = 0.0f;
  vec2 texel_size = 1.0f / cascade_map_size;
  const int kernel_center_offset = kernel_dim / 2;
  for(int x = -kernel_center_offset; x <= kernel_center_offset; x++) {
    for(int y = -kernel_center_offset; y <= kernel_center_offset; y++) {
      vec2 offset_coords = CascadeMapCoords(screen_coords.xy + (vec2(x, y) * texel_size), cascade );
      shadow += InterpolatedSample( shadow_map, offset_coords, screen_coords.z - bias);
    }    
  }

  return shadow / float(kernel_dim * kernel_dim);
}

///////////////////////////////////////////////////////////////////////////////////////////////////
// Tiled Utility Functions.
///////////////////////////////////////////////////////////////////////////////////////////////////
bool SphereAABBIntersect( in vec3 sphere_centre,
                          in float radius,
                          in vec3 aabb_min,
                          in vec3 aabb_max) {
  vec3 closest_aabb_point =  min( max( sphere_centre, aabb_min ), aabb_max );
  vec3 delta = (closest_aabb_point - sphere_centre);
  float dist = dot( delta, delta );
  return dist <= radius * radius;
}

void main() {
  tile_point_light_count = 0;
  barrier();
  // 1. Light Culling
  ///////////////////////////////////////////////////////////////////////////////////////////////////
  // Take bounds calculated in previous compute shader along with depth mask.
  uint tile_index = gl_WorkGroupID.x + gl_WorkGroupID.y * gl_NumWorkGroups.x; 
  uint threads_per_tile = gl_WorkGroupSize.x * gl_WorkGroupSize.y;
  
  uint tile_depth_mask = tile_buffer[tile_index].tile_mask;
  
  vec3 plane0 = tile_buffer[tile_index].frustum_planes[0].xyz;
  vec3 plane1 = tile_buffer[tile_index].frustum_planes[1].xyz;
  vec3 plane2 = tile_buffer[tile_index].frustum_planes[2].xyz;
  vec3 plane3 = tile_buffer[tile_index].frustum_planes[3].xyz;
  
  vec3 aabb_min = tile_buffer[tile_index].aabb_min.xyz;
  vec3 aabb_max = tile_buffer[tile_index].aabb_max.xyz;
  
  float tile_max_z = aabb_min.z;
  float tile_min_z = aabb_max.z;
  float range_recip = 32.0f / (tile_max_z - tile_min_z);
  
  for(uint i = gl_LocalInvocationIndex; i < point_lights.length(); i += threads_per_tile) {
    vec4 view_position = camera_view_matrix * vec4(point_lights[i].position.xyz, 1.0f);
    float radius = point_lights[i].radius;
    
    float light_max_z = view_position.z - radius;
    float light_min_z = view_position.z + radius;
    
    uint light_mask_start_index = max(0, min(32, uint(floor((light_min_z - tile_min_z) * range_recip))));
    uint light_mask_end_index = max(0, min(32, uint(floor((light_max_z - tile_min_z) * range_recip))));
    uint light_mask = 0xFFFFFFFF;
    light_mask >>= 31 - (light_mask_end_index - light_mask_start_index);
    light_mask <<= light_mask_start_index;
    if( ( dot( plane0.xyz, view_position.xyz ) < radius ) &&
        ( dot( plane1.xyz, view_position.xyz ) < radius ) &&
        ( dot( plane2.xyz, view_position.xyz ) < radius ) &&
        ( dot( plane3.xyz, view_position.xyz ) < radius ) &&
          SphereAABBIntersect( view_position.xyz, radius, aabb_min, aabb_max) &&
          (light_mask & tile_depth_mask) != 0) {
      uint id = atomicAdd(tile_point_light_count, 1);
      tile_point_light_indices[id] = i;
    }
    
  }
  
  barrier();
  ivec2 image_coords = ivec2(gl_GlobalInvocationID.xy);
  ivec2 out_framebuffer_size = imageSize(out_color);
  if(image_coords.x >= out_framebuffer_size.x ||
     image_coords.y >= out_framebuffer_size.y) {
    return;
  }
  vec3 total_color;
  if(tile_point_light_count > 400) {
    total_color = vec3(1.0f, 1.0f, 1.0f);
  }
  else if( tile_point_light_count > 261 ) {
    total_color = vec3(0.67, 0.67f, 0.67f);
  }
  else if( tile_point_light_count > 170 ) {
    total_color = vec3(0.67, 0.0f, 0.67f);
  }
  else if( tile_point_light_count > 111 ) {
    total_color = vec3(0.67, 0.0f, 0.0f);
  }
  else if( tile_point_light_count > 48 ) {
    total_color = vec3(0.67, 0.0f, 0.1f);
  }
  else if( tile_point_light_count > 31 ) {
    total_color = vec3(1.0f, 1.0f, 0.0f);
  }
  else if( tile_point_light_count > 20 ) {
    total_color = vec3(0.67, 0.67f, 0.0f);
  }
  else if( tile_point_light_count > 14 ) {
    total_color = vec3(0.0, 0.34, 0.0f);
  }
  else if( tile_point_light_count > 9 ) {
    total_color = vec3(0.0, 0.67f, 0.0f);
  }
  else if( tile_point_light_count > 4 ) {
    total_color = vec3(0.0, 0.67f, 0.0f);
  }
  else if( tile_point_light_count == 3 ) {
    total_color = vec3(0.0, 0.0f, 1.0f);
  }
  else if( tile_point_light_count == 2 ) {
    total_color = vec3(0.0, 0.67f, 0.67f);
  }
  else if( tile_point_light_count == 1 ) {
    total_color = vec3(0.0, 1.0f, 1.0f);
  }
  else {
    total_color = vec3(0.0f, 0.0f, 0.0f);
  }
  
  imageStore(out_color, image_coords, vec4(total_color, 1.0f));
}