#version 450
// IN 
layout( location = 0 ) in vec3 position;
layout( location = 1 ) in vec2 in_tex_coord;
// OUT
layout( location = 0 ) out vec2 out_tex_coord;

void main() {
  out_tex_coord = in_tex_coord;
  gl_Position = vec4(position, 1.0f);
}