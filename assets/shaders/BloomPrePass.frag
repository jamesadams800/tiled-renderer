#version 450

// IN 
layout( location = 0 ) in vec2 in_tex_coord;
layout( binding = 0 ) uniform sampler2D input_hdr_buffer;

// OUT
layout( location = 0 ) out vec4 high_intensity;

void main() {
  vec3 input_intensity = texture( input_hdr_buffer, in_tex_coord ).rgb;
  const vec3 rgb_to_lumin = vec3(0.212f, 0.715f, 0.072f);
  float luminosity = dot(rgb_to_lumin, input_intensity);
  if( luminosity > 1.0f ) {
    high_intensity = vec4(input_intensity, 1.0f);
  }
  else {
    //discard;
  }
}