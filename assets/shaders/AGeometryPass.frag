#version 450
// In
layout(location = 0) in vec3 world_position;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec2 tex_coord;

layout(binding = 0) uniform sampler2D albedo_map;

layout(std140) uniform MaterialConstants {
  vec4 diffuse;
  vec4 specular;
  vec4 emissive;
  float shininess_exp;
} constants;

// Out
layout(location = 0) out vec3 out_position;
layout(location = 1) out vec3 out_normal;
layout(location = 2) out vec4 out_albedo;
layout(location = 3) out vec4 out_specular;

void main() {
  // World space positions.
  out_position = world_position;

  // World space Normal
  out_normal = normal;
  
  // Albedo JUST PUT IN NEUTAL COLOUR FOR NOW.
  vec3 albedo = texture(albedo_map, tex_coord).rgb;
  out_albedo = vec4(albedo, 1.0f);
  out_specular = vec4(albedo, constants.shininess_exp);
}