#version 450
// IN 
layout( location = 0 ) in vec2 in_tex_coord;

layout( binding = 0 ) uniform sampler2D hdr_map;
uniform float gamma;

// OUT
layout( location = 0 ) out vec4 color;

void main() {
  vec3 hdr_color = texture(hdr_map, in_tex_coord).rgb;
  // Uncharted 2 style filmic tone mapping from.
  // https://www.slideshare.net/ozlael/hable-john-uncharted2-hdr-lighting
  const float a = 0.15f; // a = shoulder strength
  const float b = 0.50f; // b = linear strength
  const float c = 0.10f; // c = linear angle
  const float d = 0.20f; // d = toe strength
  const float e = 0.02f; // e = toe numerator
  const float f = 0.30f; // f = toe denominator
  const float linear_white = 11.2f;
  vec3 f_hdr_color = ((hdr_color * (a * hdr_color + c * b) + d * e) / 
                      (hdr_color * (a * hdr_color + b) + d * f)) - e / f;
  const float f_white = ((linear_white * (a * linear_white + c * b) + d * e) /
                         (linear_white * (a * linear_white + b) + d * f)) - e / f;
  vec3 tone_mapped = f_hdr_color / f_white;

  // Gamma correction.
  vec3 gamma_corrected = pow(tone_mapped, vec3(1.0f / gamma));
  color = vec4(gamma_corrected, 1.0f);
}