#version 450
// IN 
layout( location = 0 ) in vec2 in_tex_coord;

layout( binding = 0 ) uniform sampler2D hdr_map;

uniform float gamma;

const float white = 11.2f;

// OUT
layout( location = 0 ) out vec4 color;

float Reinhard( float luminence ) {
  return (luminence * (1 + luminence /(white * white)) / (1 + luminence));
}

void main() {
  // Reinhard tone mapping.
  vec3 hdr_color = texture(hdr_map, in_tex_coord).rgb;
  float luminence = dot(hdr_color, vec3(0.2126f, 0.7152f, 0.0722f));
  float mapped_luminence = Reinhard(luminence);
  float scale = mapped_luminence / luminence;
  vec3 tone_mapped = hdr_color * scale;
  // Gamma correction.
  vec3 gamma_corrected = pow(tone_mapped, vec3(1.0f / gamma ));
  color = vec4(gamma_corrected, 1.0f);
}