#version 450
// IN 
layout( location = 0 ) in vec2 in_tex_coord;

layout( binding = 0 ) uniform sampler2D hdr_map;

// OUT
layout( location = 0 ) out vec4 out_color;

// Implementation has been adapted from the Nvidia white paper "FXAA"
// located at: http://developer.download.nvidia.com/assets/gamedev/files/sdk/11/FXAA_WhitePaper.pdf

// Defines are taken from the values defined as "high quality" in the "Tuning Defines" subsection.
#define FXAA_EDGE_THRESHOLD ( 1.0f / 8.0f )
#define FXAA_EDGE_THRESHOLD_MIN ( 1.0f / 16.0f )

#define FXAA_MAX_EDGE_SEARCH_ITERATIONS 11

#define FXAA_SUBPIXEL_TRIM 0.25f
// Estimate lum from only R and G channels.  As described in white paper blue channel
// aliasing rarely appears in game content.  The hdr_map texture is stored in sRGB color space
// so does not require any conversions.
float Fxaalum( vec3 rgb ) {
  return rgb.y * (0.587f / 0.299f) + rgb.x;
}

//TODO: Optimisation pass on this.... i'm sure i'm doing redundant calculations.
void main() {
  vec2 texel_size = 1.0f / textureSize(hdr_map, 0);
  
  // color values of center up down left and right pixels. 
  vec3 color_c = texture(hdr_map, in_tex_coord).rgb;
  vec3 color_u = texture(hdr_map, in_tex_coord + texel_size * vec2(0, 1)).rgb;
  vec3 color_d = texture(hdr_map, in_tex_coord + texel_size * vec2(0, -1)).rgb;
  vec3 color_l = texture(hdr_map, in_tex_coord + texel_size * vec2(-1, 0)).rgb;
  vec3 color_r = texture(hdr_map, in_tex_coord + texel_size * vec2(1, 0)).rgb;
  
  float lum_c = Fxaalum(color_c);
  float lum_u = Fxaalum(color_u);
  float lum_d = Fxaalum(color_d);
  float lum_l = Fxaalum(color_l);
  float lum_r = Fxaalum(color_r);
  
  float range_min = min( lum_c, min( min( lum_u, lum_d ), min(lum_l, lum_r)));
  float range_max = max( lum_c, max( max( lum_u, lum_d ), max(lum_l, lum_r)));
  
  float range = range_max - range_min;
  if( range < max(FXAA_EDGE_THRESHOLD_MIN, range_max * FXAA_EDGE_THRESHOLD)) {
    out_color = vec4(color_c, 1.0f);
    return;
  }
  
  vec3 color_ur = texture(hdr_map, in_tex_coord + texel_size * vec2(1, 1)).rgb;
  vec3 color_dr = texture(hdr_map, in_tex_coord + texel_size * vec2(1, -1)).rgb;
  vec3 color_ul = texture(hdr_map, in_tex_coord + texel_size * vec2(-1, 1)).rgb;
  vec3 color_dl = texture(hdr_map, in_tex_coord + texel_size * vec2(-1, -1)).rgb;
   
  float lum_ur = Fxaalum(color_ur);
  float lum_dr = Fxaalum(color_dr);
  float lum_ul = Fxaalum(color_ul);
  float lum_dl = Fxaalum(color_dl);
  
  float vertical_edge = abs((0.25f * lum_ul) + (-0.5f * lum_u) + (0.25f * lum_ur))+
                        abs((0.5f * lum_l)   + (-1.0f * lum_c) + (0.5f * lum_r))  +
                        abs((0.25f * lum_dl) + (-0.5f * lum_d) + (0.25f * lum_dr));
                        
  float horizontal_edge = abs((0.25f * lum_ul) + (-0.5f * lum_l) + (0.25f * lum_dl))+
                          abs((0.5f * lum_u)   + (-1.0f * lum_c) + (0.5f * lum_d))  +
                          abs((0.25f * lum_ur) + (-0.5f * lum_r) + (0.25f * lum_dr));

  bool is_horizontal = horizontal_edge >= vertical_edge;

  if(is_horizontal) {
    out_color = vec4(1.0f,0.0f,0.0f,1.0f);
  }
  else {
   out_color = vec4(0.0f,1.0f,0.0f,1.0f);
  }
  //return;
  // Work out direction of gradient and then set offet to between edge of texel.
  float neighbour_1_lum = is_horizontal ? lum_d : lum_l;
  float neighbour_2_lum = is_horizontal ? lum_u : lum_r;
  
  float positive_gradient = neighbour_1_lum - lum_c;
  float negative_gradient = neighbour_2_lum - lum_c;
  float scaled_gradient = 0.25f * max(abs(positive_gradient), abs(negative_gradient));
  
  float edge_offset_distance = is_horizontal ? texel_size.y : texel_size.x;
  float local_lum_average = 0.0f;
  
  if( abs(positive_gradient) >= abs(negative_gradient) ) {
    edge_offset_distance = -edge_offset_distance;
    local_lum_average = 0.5f * (neighbour_1_lum + lum_c);
  }
  else {
    local_lum_average = 0.5f * (neighbour_2_lum + lum_c);
  }
  
  // Calculate offset position on edge and the cumulative offset used
  // to walk along the edge.
  vec2 current_tex_coords = in_tex_coord;
  vec2 edge_direction_offset = vec2(0.0f);
  if(is_horizontal) {
    current_tex_coords.y += edge_offset_distance * 0.5f;
    edge_direction_offset = vec2(texel_size.x, 0.0f);
  }
  else {
    current_tex_coords.x += edge_offset_distance * 0.5f;  
    edge_direction_offset = vec2(0.0f, texel_size.y);
  }
  
  // Begin walking along the edge searching for its extremities.
  vec2 negative_coord = current_tex_coords;
  vec2 positive_coord = current_tex_coords;
  float lumanance_negative_end = 0.0f;
  float lumanance_positive_end = 0.0f;
  bool extremity_reached_p = false;
  bool extremity_reached_n = false;
  for( int i = 0; i < FXAA_MAX_EDGE_SEARCH_ITERATIONS; i++ ) {
    if( extremity_reached_p && extremity_reached_p ) {
      break;
    }
    if( !extremity_reached_n ) {
      negative_coord -= edge_direction_offset;
      lumanance_negative_end = Fxaalum(texture( hdr_map, negative_coord).rgb) - local_lum_average;
    }
    if( !extremity_reached_p ) {
      positive_coord += edge_direction_offset;
      lumanance_positive_end = Fxaalum(texture( hdr_map, positive_coord).rgb) - local_lum_average;
    }
    
    extremity_reached_n = abs(lumanance_negative_end) >= scaled_gradient;
    extremity_reached_p = abs(lumanance_positive_end) >= scaled_gradient;
  }
  
  float negative_distance = is_horizontal ? (in_tex_coord.x - negative_coord.x) : 
                                            (in_tex_coord.y - negative_coord.y);
  
  float positive_distance = is_horizontal ? (positive_coord.x - in_tex_coord.x) : 
                                            (positive_coord.y - in_tex_coord.y);
                                            
  float shortest_distance = min(negative_distance, positive_distance);
  bool is_negative_shortest = negative_distance < positive_distance;
  float edge_length = positive_distance + negative_distance;
  
  float tex_offset = -shortest_distance / edge_length + 0.5f;
  // Check that extremities lum is coherent with the luma at the current pixel. 
  bool center_lum_smaller = (lum_c < local_lum_average);
  float shortest_edge_lum = is_negative_shortest ? lumanance_negative_end : lumanance_positive_end;
  if( (shortest_edge_lum < 0.0f ) == center_lum_smaller ) {
    tex_offset = 0.0f;
  }
  
  // Finally do sub pixel aliasing test.
  float luma_average = (1.0/12.0) * (2.0 * (lum_u + lum_d + lum_l + lum_r) + 
                                           (lum_ul + lum_dl + lum_ur + lum_dr));
  float sub_offset = clamp(abs(luma_average - lum_c)/range,0.0,1.0);
  sub_offset = (-2.0 * sub_offset + 3.0) * sub_offset * sub_offset;
  sub_offset = sub_offset * sub_offset * 0.75f;

  // Pick the biggest of the two offsets.
  tex_offset = max(tex_offset,sub_offset);
  
  vec2 final_uv = in_tex_coord;
  if(is_horizontal){
      final_uv.y += tex_offset * edge_offset_distance;
  } else {
      final_uv.x += tex_offset * edge_offset_distance;
  }
  
  out_color = texture(hdr_map, final_uv);
}