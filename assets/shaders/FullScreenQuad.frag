#version 450
// IN 
layout( location = 0 ) in vec2 in_tex_coord;
// OUT
layout( location = 0 ) out vec4 color;

layout(binding = 0) uniform sampler2D positions_map;
layout(binding = 1) uniform sampler2D normals;
layout(binding = 2) uniform sampler2D albedo_map;

void main() {
  vec4 albedo = texture( albedo_map, in_tex_coord );
  color = vec4(albedo.rgb, 1.0f);
}