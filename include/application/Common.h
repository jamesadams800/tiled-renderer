#ifndef OGL_COMMON_H_
#define OGL_COMMON_H_

#ifdef _WIN32
#ifdef DLL_BUILD
#define RENDER_API __declspec(dllexport)
#else
#define RENDER_API __declspec(dllimport)
#endif

#else 

#define RENDER_API

#endif // _WIN32

#endif // OGL_COMMON_H_
