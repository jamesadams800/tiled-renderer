#ifndef OGL_SCENELOADER_H_
#define OGL_SCENELOADER_H_

#include <stdexcept>
#include <functional>

#include "Common.h"
#include "Scene.h"
#include "Mesh.h"

namespace renderer {

class SceneLoadException : public std::runtime_error {
public:
  explicit SceneLoadException(const std::string& err)
    : std::runtime_error(err) {
  }
private:
};

class SceneLoader {
public:
  enum FileType {
    OBJ,
    UNKNOWN
  };

  enum LoaderType {
    ASSIMP,
    DEFAULT
  };
  static RENDER_API void LoadScene(std::string file_name,
                                      Scene& scene, 
                                      LoaderType = DEFAULT);
  static RENDER_API std::vector<std::shared_ptr<Mesh>> LoadMeshesFromFile(std::string file_name, 
                                                                             Scene & output_scene,
                                                                             LoaderType = DEFAULT);
private:
  static bool FileNameEndsIn(const std::string& file_name, const std::string& extension);
  static FileType GetFileType(const std::string& file_name);
  static const std::string kObjExtension_;
};

} // namespace renderer 

#endif // OGL_SCENELOADER_H_
