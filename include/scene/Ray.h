#ifndef OGL_RAY_H_
#define OGL_RAY_H_

#pragma warning(push, 0)  
#include <glm/glm.hpp>
#include <glm/ext.hpp>
#pragma warning(pop)

#include "Object.h"

namespace renderer {

class Mesh;

class Ray : public Object {
public:
  RENDER_API static std::shared_ptr<Ray> Create();
  explicit Ray();
  explicit Ray(const glm::vec3&, const glm::vec3&);

  RENDER_API void SetOrigin(const glm::vec3& origin);
  RENDER_API void SetDirection(const glm::vec3& direction);
  RENDER_API const glm::vec3& GetOrigin() const;
  RENDER_API const glm::vec3& GetDirection() const;

  // Mesh Intersect
  RENDER_API bool Intersect(const Mesh& mesh, float& t, glm::vec3& barycentric_coords);
  RENDER_API bool Intersect(const Mesh& mesh, float& t, glm::vec3& barycentric_coords, uint32_t& closest_index);
  // Triangle Intersect
  RENDER_API bool Intersect(const glm::vec3& v1, const glm::vec3& v2, const glm::vec3& v3,
                               float& t, glm::vec3& barycentric_coords);

private:
  glm::vec3 origin_;
  glm::vec3 direction_;
};

} // namespace renderer

#endif // OGL_RAY_H_