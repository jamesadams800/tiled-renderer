#ifndef OGL_SHAPEFACTORY_H_
#define OGL_SHAPEFACTORY_H_

#include "Mesh.h"

namespace renderer {

class ShapeFactory {
public:
  RENDER_API static std::shared_ptr<Mesh> BuildSphereShared(float radius, uint32_t resolution = 64);
  RENDER_API static std::shared_ptr<Mesh> BuildCuboidShared(float width, float height, float length);
  RENDER_API static std::shared_ptr<Mesh> BuildPlaneShared(float width, float length);

  static Mesh* BuildSphere(float radius, uint32_t resolution = 64);
  static Mesh* BuildCuboid(float width, float height, float length);
  static Mesh* BuildPlane(float width, float length);
};

} // namespace renderer

#endif // OGL_SHAPEFACTORY_H_