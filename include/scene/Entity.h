#ifndef OGL_ENTITY_H_
#define OGL_ENTITY_H_

#pragma warning(push, 0)  
#include <glm/glm.hpp>
#include <glm/ext.hpp>
#pragma warning(pop)

#include "Object.h"

namespace renderer {

class Entity : public Object {
public:
  Entity();
  virtual ~Entity();

  RENDER_API virtual void SetTransform(const glm::mat4& transform);
  RENDER_API glm::vec3 GetTranslation() const;
  RENDER_API virtual const glm::mat4& GetWorldTransform() const;
  RENDER_API virtual void AddTranslation(const glm::vec3& vec);
  RENDER_API virtual void SetTranslation(const glm::vec3& vec);
  RENDER_API virtual void SetTranslation(float x, float y, float z);
  RENDER_API virtual void SetRotation(const glm::vec3& axis, float rad);

protected:
  glm::mat4 transform_matrix_;
};

} // namespace renderer

#endif // OGL_ENTITY_H_