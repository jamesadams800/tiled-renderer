#ifndef OGL_MATERIAL_H_
#define OGL_MATERIAL_H_

#include <array>

#pragma warning(push, 0)  
#include <glm/glm.hpp>
#include <glm/ext.hpp>
#pragma warning(pop)

#include "Object.h"

namespace renderer {

template <class T> class Texture;
template <class T, class ...Args> class UniformBuffer;

class Material : public Object {
public:
  struct ConstantComponents {
    glm::vec4 diffuse_color;
    glm::vec4 specular_color;
    glm::vec4 emissive_color;
    float shininess;
  };

  using LayerPresence = uint32_t;

  enum LayerIdentifier {
    ALBEDO_MAP_ID = 0,
    SPECULAR_MAP_ID = 1,
    NORMAL_MAP_ID = 2,
    ALPHA_MAP_ID = 3
  };

  enum Layers : LayerPresence {
    ALBEDO_MAP = 1 << ALBEDO_MAP_ID,
    SPECULAR_MAP = 1 << SPECULAR_MAP_ID,
    NORMAL_MAP = 1 << NORMAL_MAP_ID,
    ALPHA_MAP = 1 << ALPHA_MAP_ID
  };

  RENDER_API static std::shared_ptr< Material> Create();
  Material();

  RENDER_API void SetName(const std::string& name);
  std::string GetName() const {
    return material_name_;
  }

  void Bind() const;

  LayerPresence GetLayersPresent() const {
    return layer_presence_;
  }

  uint32_t GetUuid() const {
    return uuid_;
  }

  const UniformBuffer<ConstantComponents>& GetConstantsUniform() const;
  const Texture<uint8_t>* GetTexture(LayerIdentifier type);

  RENDER_API void SetTexture(std::shared_ptr<Texture<uint8_t>>, LayerIdentifier type);
  RENDER_API void SetEmissive(const glm::vec3& color);
  RENDER_API void SetDiffuse(const glm::vec3& color);
  RENDER_API void SetSpecular(const glm::vec3& color);
  RENDER_API void SetShininess(float shininess);

protected:
  LayerPresence layer_presence_;
  static constexpr int32_t MAX_TEXTURES = 32;
  std::array< std::shared_ptr<Texture<uint8_t>>, MAX_TEXTURES> textures_;
private:
  mutable std::unique_ptr<UniformBuffer<ConstantComponents>> material_constants_;
  std::string material_name_;
  const uint32_t uuid_;
  static uint32_t uuid_counter_;
};

} // namespace renderer

#endif // OGL_MATERIAL_H_