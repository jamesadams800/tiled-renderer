#ifndef OGL_CAMERA_H_
#define OGL_CAMERA_H_

#include <array>

#include "Entity.h"
#include "Ray.h"

namespace renderer {

class Camera : public Entity {
public:
  Camera();
  explicit Camera(float aspect_ratio);

  // Pure virtuals.
  RENDER_API virtual void CalculateProjectionMatrix() = 0;
  RENDER_API virtual std::array<glm::vec4, 8> GetSplitFrustrum(float near, float far) const = 0;
  RENDER_API virtual std::array<glm::vec4, 8> GetWorldSpaceSplitFrustrum(float near, float far) const = 0;

  // and the rest....
  RENDER_API const glm::mat4& GetViewMatrix() const;
  RENDER_API const glm::mat4& GetInvViewMatrix() const;
  RENDER_API const glm::mat4& GetProjectionMatrix() const;
  RENDER_API const glm::mat4& GetInvProjectionMatrix() const;
  RENDER_API glm::vec4 GetPosition() const;
  RENDER_API const glm::vec3& GetAtVec() const;
  RENDER_API const glm::vec3& GetUpVec() const;
  RENDER_API void Resize(uint32_t x_res, uint32_t y_res);
  RENDER_API float GetAspectRatio() const;
  RENDER_API void SetAspectRatio(float aspect);
  RENDER_API float GetNearPlane() const;
  RENDER_API void SetNearPlane(float near_plane_distance);
  RENDER_API float GetFarPlane() const;
  RENDER_API void SetFarPlane(float far_plane_distance);
  RENDER_API void LookAt(float x, float y, float z);
  RENDER_API void LookAt(const glm::vec3& at_vec);
  RENDER_API const std::array<glm::vec4, 8>& GetFrustrumCorners() const;
  RENDER_API void CalculateViewMatrix() const;
  // Takes float xcoord, float ycoord in normalised screen space.
  RENDER_API Ray CreateCameraRay(float xcoord, float ycoord) const;

protected:
  virtual void CalculateFrustrum() = 0;

  mutable glm::mat4 projection_matrix_;
  mutable glm::mat4 view_matrix_;
  mutable glm::mat4 inv_view_matrix_;
  mutable glm::mat4 inv_projection_matrix_;

  glm::vec3 at_;
  glm::vec3 up_;
  glm::vec3 eye_;

  std::array<glm::vec4, 8> camera_space_frustrum_corners_;
private:
  float aspect_ratio_;

  float near_plane_;
  float far_plane_;
};

} // namespace renderer

#endif // OGL_CAMERA_H_