#ifndef OGL_VERTEXBUFFER_H_
#define OGL_VERTEXBUFFER_H_

#include <memory>
#include <vector>
#include <map>

#include "Object.h"

namespace renderer {

class VertexBuffer : public Object {
public:

  RENDER_API static std::shared_ptr<VertexBuffer> Create();

  enum VertexAttribute : uint64_t {
    POSITION,
    NORMAL,
    TEXCOORD,
    TANGENT,
    BITANGENT,
    TEXCOORD0,
    TEXCOORD1,
    TEXCOORD2,
    TEXCOORD3,
    TEXCOORD4,
    TEXCOORD5,
    COLOR
  };

  struct VertexAttributeFormat {
    uint32_t vertex_offset; // Index offset in data vector (For bytes * by float)
    uint32_t vertex_size; // Number of components per vertex attribute
    uint32_t vertex_stride; // Stride between vertex attributes
  };

  friend class Proxy;
  struct RENDER_API Proxy {
    Proxy(VertexBuffer& vertex_buffer,
          const VertexAttributeFormat& attribute)
      : vertex_buffer_{ vertex_buffer },
        attribute_format_{ attribute } {
    }

    inline float* operator[](uint32_t index) {
      auto& array = vertex_buffer_.vertex_array_;
      return &array[attribute_format_.vertex_offset +
                    (attribute_format_.vertex_size + attribute_format_.vertex_stride) *
                    index];
    }

  private:
    VertexBuffer& vertex_buffer_;
    const VertexAttributeFormat attribute_format_;
  };

  friend class ConstProxy;
  struct RENDER_API ConstProxy {
    ConstProxy(const VertexBuffer& vertex_buffer,
               const VertexAttributeFormat& attribute)
      : vertex_buffer_{ vertex_buffer },
        attribute_format_{ attribute } {
    }

    inline const float* operator[](uint32_t index) const {
      auto& array = vertex_buffer_.vertex_array_;
      return &array[attribute_format_.vertex_offset +
                    (attribute_format_.vertex_size + attribute_format_.vertex_stride) *
                    index];
    }

  private:
    const VertexBuffer& vertex_buffer_;
    const VertexAttributeFormat attribute_format_;
  };

  VertexBuffer();
  ~VertexBuffer();

  VertexBuffer(const VertexBuffer& other);
  VertexBuffer& operator=(VertexBuffer other);
  VertexBuffer(VertexBuffer&& other);
  VertexBuffer& operator=(const VertexBuffer&& other);
  RENDER_API friend void swap(VertexBuffer& first, VertexBuffer& second);
  RENDER_API virtual std::shared_ptr<VertexBuffer> Clone();
  RENDER_API size_t Size() const;
  RENDER_API const std::map< VertexAttribute, VertexAttributeFormat >& GetVertexAttributeMap() const;
  RENDER_API void UpdateVertexArray(VertexAttribute attrib, const std::vector<float>& data);
  RENDER_API void AddVertexAttributeData(VertexAttribute attrib,
                                            uint32_t vertex_size,
                                            uint32_t stride,
                                            const std::vector<float>& data);
  RENDER_API bool IsAttributePresent(VertexAttribute attribute_type) const;
  RENDER_API VertexAttributeFormat GetAttributeFormat(VertexAttribute attribute_type) const;

  RENDER_API Proxy operator[](VertexAttribute);
  RENDER_API ConstProxy operator[](VertexAttribute) const;

  void Bind() const;

  float& operator[](uint32_t i) {
    return vertex_array_[i];
  }

  void CreateGpuObject();
  RENDER_API void UpdateGpuObject();
  void DeleteGpuObject();
private:

  uint32_t vertex_buffer_handle_;
  size_t vertex_count_;
  std::vector<float> vertex_array_;
  std::map< VertexAttribute, VertexAttributeFormat > attribute_format_;
};

} // namespace renderer

#endif // OGL_VERTEXBUFFER_H_