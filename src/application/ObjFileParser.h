#ifndef OGL_OBJFILEPARSER_H_
#define OGL_OBJFILEPARSER_H_

#include <fstream>
#include <vector>
#include <memory>
#include <list>

namespace renderer {

class Model;
class Scene;
class VertexBuffer;
class IndexBuffer;
class Mesh;
class ARenderableFactory;

class ObjFileParser {
public:
  ObjFileParser(const std::string& file_name, bool parse = true);
  void ParseFile();
  void LoadScene(Scene& scene) const;
  std::vector<std::shared_ptr<Mesh>> GetMeshList() const;

private:
  enum ObjEntry {
    COMMENT,
    VERTEX,
    NORMAL,
    PARAMETER,
    TEXCOORD,
    MATERIAL_FILE,
    USE_MATERIAL,
    GROUP,
    FACE,
    OBJECT,
    LINE,
    SMOOTHING_GROUP,
    INVALID
  };
  ObjEntry GetObjEntry(std::stringstream& ss);

  // Parser specific functions.
  void ParseLine(std::string&);

  template<typename T>
  void ExtractValue(std::stringstream&, T& output);

  template<typename T, uint32_t EXPECTED_EXTRACT>
  void ExtractToVector(std::stringstream&, std::vector<T>&);


  void ParseFaceEntry(std::stringstream&);
  void ParseFaceToken(const std::string& token, std::vector<uint32_t>&,
                     std::vector<uint32_t>&, std::vector<uint32_t>&);

  // Buffer/Mesh building functions.
  void AlignVertexAttributes();
  VertexBuffer* BuildVertexBuffer();
  IndexBuffer* BuildIndexBuffer();
  std::shared_ptr<Mesh> BuildMesh();

  std::string obj_file_name_;
  std::ifstream obj_file_stream_;
  std::string mtl_file_name_;

  // Index offset is used to offset obj file indecies, we are going to construct meshes
  // assuming seperate vertex buffers so every time a new mesh is constructed the offset
  // needs to be stored.
  uint32_t vertex_index_offset_;
  uint32_t normal_index_offset_;
  uint32_t tex_coord_index_offset_;

  std::vector<float> vertex_data_;
  std::vector<float> normal_data_;
  std::vector<float> tex_coord_data_;

  std::vector<uint32_t> vertex_index_;
  std::vector<uint32_t> normal_index_;
  std::vector<uint32_t> tex_coord_index_;
  // I make myself sick.....
  // This is just a stopgap until i implement some kind of FSM.
  bool parsing_face_data_;
  std::string current_group_name_;

  std::vector<std::shared_ptr<Mesh>> mesh_list_;
};

template<typename T>
inline void ObjFileParser::ExtractValue(std::stringstream& ss_line, T & output) {
  T val;
  if (ss_line >> val) {
    output = val;
  }
  else {
    throw std::runtime_error("ObjFileParser::ExtractValue Expected value missing.");
  }
};

template<typename T, uint32_t EXPECTED_EXTRACT>
inline void ObjFileParser::ExtractToVector(std::stringstream & ss_line, std::vector<T>& output_vector) {
  T val;
  uint32_t count = 0;
  while (count < EXPECTED_EXTRACT && ss_line >> val) {
    output_vector.push_back(val);
    count++;
  }
  if (count != EXPECTED_EXTRACT) {
    throw std::runtime_error("ObjFileParser::ExtractToVector tokens extracted ["
      + std::to_string(count) + " < EXPECTED_EXTRACT");
  }
};

#endif // OGL_OBJFILEPARSER_H_

} // renderer