#include "GLFWManager.h"

#include <string>
#include <iostream>

namespace renderer {

GLFWManager::GLFWManager() {
  if (!glfwInit()) {
    throw std::runtime_error("Error Initialising GLFW. Abandon all hope.");
  }
  // Create window to allow for GLEW Initialisation.  We have to call glfw Terminate 
  // in all error legs here as I do not have a delegating constructor.
  GLFWwindow* window = glfwCreateWindow(640, 480, "GLEW Initialisation", NULL, NULL);
  if (!window) {
    glfwTerminate();
    throw std::runtime_error("Error Creating GLFW Window for GLEW Initialisation.");
  }
  glfwMakeContextCurrent(window);
  GLenum err = glewInit();
  if (GLEW_OK != err) {
    glfwTerminate();
    throw std::runtime_error("Error Creating GLEW Instance Error code[" + std::to_string(err) + "]");
  }

  // Check GL version then unbind temp window.
  this->PrintGLVersion();
  glfwMakeContextCurrent(nullptr);
  glfwDestroyWindow(window);
}

GLFWManager::~GLFWManager() {
  glfwTerminate();
}

void GLFWManager::BindWindow(GLFWwindow * window) {
  glfwMakeContextCurrent(window);
}

void GLFWManager::PrintGLVersion() const {
  int gl_version[2] = { -1, -1 };

  glGetIntegerv(GL_MAJOR_VERSION, &gl_version[0]);
  glGetIntegerv(GL_MINOR_VERSION, &gl_version[1]);

  std::string vendor_string((char*)glGetString(GL_VENDOR));
  std::string renderer_string((char*)glGetString(GL_RENDERER));

  std::cout << vendor_string << std::endl;
  std::cout << renderer_string << std::endl;
  std::cout << "Using OpenGL Version: " << gl_version[0] << "." << gl_version[1] << " :GLError = " << glGetError() << std::endl;
}

} // namespace renderer
