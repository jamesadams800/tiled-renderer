#include "GLApplication.h"
#include <iostream>

#include "Scene.h"
#include "GLApplicationImpl.h"

namespace renderer {

GLApplication::GLApplication(const std::string& name)
  : impl_{ new GLApplication::Impl(*this, name) } {
}

GLApplication::~GLApplication() {
}

void GLApplication::Run() {
  impl_->Run();
}

void GLApplication::AddKeyListener(IKeyListener* listener) {
  impl_->AddKeyListener(listener);
}

void GLApplication::AddMouseListener(IMouseListener* listener) {
  impl_->AddMouseListener(listener);
}

} // namespace renderer