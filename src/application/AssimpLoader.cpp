#include "AssimpLoader.h"

#include <assimp\Importer.hpp>
#include <assimp\postprocess.h>
#include <assimp\scene.h>
#include <assimp\scene.h>
#include <assimp\mesh.h>
#include <assimp\camera.h>

#include "Mesh.h"
#include "Light.h"
#include "Camera.h"
#include "Texture.h"
#include "SceneLoader.h"

namespace renderer {

uint64_t AssimpLoader::file_material_offset_ = 0;

glm::mat4 BuildMatrix(const aiMatrix4x4& assimp_matrix) {
  glm::mat4 out_matrix;

  out_matrix[0][0] = assimp_matrix.a1;
  out_matrix[1][0] = assimp_matrix.a2;
  out_matrix[2][0] = assimp_matrix.a3;
  out_matrix[3][0] = assimp_matrix.a4;

  out_matrix[0][1] = assimp_matrix.b1;
  out_matrix[1][1] = assimp_matrix.b2;
  out_matrix[2][1] = assimp_matrix.b3;
  out_matrix[3][1] = assimp_matrix.b4;

  out_matrix[0][2] = assimp_matrix.c1;
  out_matrix[1][2] = assimp_matrix.c2;
  out_matrix[2][2] = assimp_matrix.c3;
  out_matrix[3][2] = assimp_matrix.c4;

  out_matrix[0][3] = assimp_matrix.d1;
  out_matrix[1][3] = assimp_matrix.d2;
  out_matrix[2][3] = assimp_matrix.d3;
  out_matrix[3][3] = assimp_matrix.d4;

  return out_matrix;
}

AssimpLoader::AssimpLoader()
  : material_count_{ 0 },
    file_material_id_to_scene_id_{} {
}

std::vector<std::shared_ptr<Mesh>> AssimpLoader::ExtractMeshes(const std::string& file_name, Scene& scene) {
  std::vector<std::shared_ptr<Mesh>> meshes;
  Assimp::Importer importer;
  const aiScene* assimp_scene = importer.ReadFile(file_name, aiProcess_Triangulate);
  if (!assimp_scene) {
    throw SceneLoadException("raytracer::Scene::LoadScene :"
      " Failed to load scene from file [" + std::string(file_name) +
      "] : " + importer.GetErrorString());
  }

  if (assimp_scene->HasMeshes()) {
    for (uint32_t i = 0; i < assimp_scene->mNumMeshes; i++) {
      const aiMesh* assimp_mesh = assimp_scene->mMeshes[i];
      const aiNode* node = FindNode(assimp_scene->mRootNode, assimp_mesh->mName);
      meshes.push_back(BuildMesh(scene, assimp_mesh, node));
    }
  }

  return meshes;
}

void AssimpLoader::LoadScene(const std::string& file_name, Scene & output_scene) {
  Assimp::Importer importer;
  const aiScene* assimp_scene = importer.ReadFile(file_name,
                                                  aiProcess_CalcTangentSpace | aiProcess_Triangulate);
  if (!assimp_scene) {
    throw SceneLoadException("AssimpLoader::LoadScene :"
      " Failed to load scene from file [" + std::string(file_name) +
      "] : " + importer.GetErrorString());
  }

  file_material_offset_++;

  if (assimp_scene->HasMaterials()) {
    for (uint32_t i = 0; i < assimp_scene->mNumMaterials; i++) {
      material_count_ += 1;
      uint64_t scene_material_id = output_scene.GetNextMaterialId();
      uint64_t file_material_id = this->GetGlobalMaterialIndex(i);
      file_material_id_to_scene_id_.insert(std::make_pair(file_material_id, scene_material_id));
      output_scene.AddMaterial(scene_material_id,
                               BuildMaterial(output_scene,
                                             assimp_scene->mMaterials[i]));
    }
  }

  if (assimp_scene->HasMeshes()) {
    for (uint32_t i = 0; i < assimp_scene->mNumMeshes; i++) {
      const aiMesh* assimp_mesh = assimp_scene->mMeshes[i];
      const aiNode* node = FindNode(assimp_scene->mRootNode, assimp_mesh->mName);
      output_scene.AddRenderable(BuildMesh(output_scene, assimp_mesh, node));
    }
  }
}

uint64_t AssimpLoader::GetGlobalMaterialIndex(uint32_t local_index) {
  assert(material_count_ < 256);
  return (file_material_offset_ << 8) + local_index;
}

const aiNode * AssimpLoader::FindNode(const aiNode* node, const aiString & component_name) {
  if (node->mName == component_name) {
    return node;
  }
  for (uint32_t i = 0; i < node->mNumChildren; i++) {
    auto found_node = FindNode(node->mChildren[i], component_name);
    if (found_node != nullptr) {
      return found_node;
    }
  }
  return nullptr;
}


std::shared_ptr<Mesh> AssimpLoader::BuildMesh(Scene& scene, 
                                              const aiMesh* assimp_mesh,
                                              const aiNode* node) {
  assert(assimp_mesh);
  std::unique_ptr<VertexBuffer> vertex_buffer = std::make_unique<VertexBuffer>();
  std::vector<float> vertices;
  vertices.reserve(assimp_mesh->mNumVertices * 3);
  for (uint32_t i = 0; i < assimp_mesh->mNumVertices; i++) {
    vertices.push_back(assimp_mesh->mVertices[i].x);
    vertices.push_back(assimp_mesh->mVertices[i].y);
    vertices.push_back(assimp_mesh->mVertices[i].z);
  }
  vertex_buffer->AddVertexAttributeData(VertexBuffer::POSITION, 3, 0, vertices);

  if (assimp_mesh->HasNormals()) {
    vertices.clear();
    for (uint32_t i = 0; i < assimp_mesh->mNumVertices; i++) {
      vertices.push_back(assimp_mesh->mNormals[i].x);
      vertices.push_back(assimp_mesh->mNormals[i].y);
      vertices.push_back(assimp_mesh->mNormals[i].z);
    }
    vertex_buffer->AddVertexAttributeData(VertexBuffer::NORMAL, 3, 0, vertices);
  }

  if (assimp_mesh->HasTextureCoords(0)) {
    vertices.clear();
    for (uint32_t i = 0; i < assimp_mesh->mNumVertices; i++) {
      vertices.push_back(assimp_mesh->mTextureCoords[0][i].x);
      vertices.push_back(assimp_mesh->mTextureCoords[0][i].y);
    }
    // TODO: Fix this.
    vertex_buffer->AddVertexAttributeData(VertexBuffer::TEXCOORD, 2, 0, vertices);
  }

  if (assimp_mesh->HasTangentsAndBitangents()) {
    vertices.clear();
    for (uint32_t i = 0; i < assimp_mesh->mNumVertices; i++) {
      vertices.push_back(assimp_mesh->mTangents[i].x);
      vertices.push_back(assimp_mesh->mTangents[i].y);
      vertices.push_back(assimp_mesh->mTangents[i].z);
    }
    vertex_buffer->AddVertexAttributeData(VertexBuffer::TANGENT, 3, 0, vertices);

    vertices.clear();
    for (uint32_t i = 0; i < assimp_mesh->mNumVertices; i++) {
      vertices.push_back(assimp_mesh->mBitangents[i].x);
      vertices.push_back(assimp_mesh->mBitangents[i].y);
      vertices.push_back(assimp_mesh->mBitangents[i].z);
    }
    vertex_buffer->AddVertexAttributeData(VertexBuffer::BITANGENT, 3, 0, vertices);
  }

  // Index buffer population.
  std::unique_ptr<IndexBuffer> index_buffer = std::make_unique<IndexBuffer>();
  std::vector<uint32_t> indices;
  indices.reserve(assimp_mesh->mNumFaces * 3);
  for (uint32_t i = 0; i < assimp_mesh->mNumFaces; i++) {
    for (uint32_t j = 0; j < assimp_mesh->mFaces[i].mNumIndices; j++) {
      indices.push_back(assimp_mesh->mFaces[i].mIndices[j]);
    }
  }
  index_buffer->AddIndexData(indices);

  std::shared_ptr<Mesh> mesh = std::shared_ptr<Mesh>(new Mesh(std::move(vertex_buffer),
                                                              std::move(index_buffer)),
                                                     Object::BaseDeleter());
  uint64_t file_material_id = this->GetGlobalMaterialIndex(assimp_mesh->mMaterialIndex);
  uint64_t scene_material_id = file_material_id_to_scene_id_[file_material_id];

  auto material = scene.GetMaterial(scene_material_id);
  if (material) {
    mesh->SetMaterial(material);
  }
  else {
    std::cout << "Unable to find material [" << file_material_id << "] for mesh ["
              << assimp_mesh->mName.C_Str() << "]" << std::endl;
  }

  if (node == nullptr) {
    std::cout << "Mesh Node \'" << assimp_mesh->mName.C_Str()
              << "\' is not contained in scene graph." << std::endl;
  }
  else {
    mesh->SetTransform(BuildMatrix(node->mTransformation));
  }

  return mesh;
}

std::shared_ptr<Camera> AssimpLoader::BuildCamera(Scene&,
                                                  const aiCamera*,
                                                  const aiNode*) {
#if 0
  Camera* camera = new Camera(assimp_camera->mHorizontalFOV, assimp_camera->mAspect);
  if (node == nullptr) {
    std::cout << "Camera Node \'" << assimp_camera->mName.C_Str()
      << "\' is not contained in scene graph." << std::endl;
  }
  else {
    camera->SetTransform(BuildMatrix(node->mTransformation));
  }
  return camera;
#else
  return nullptr;
#endif
}

std::shared_ptr<Light> AssimpLoader::BuildLight(Scene &,
                                                const aiLight*,
                                                const aiNode*) {
#if 0
  Light* light = new PointLight({ assimp_light->mColorDiffuse.r,
                                  assimp_light->mColorDiffuse.g ,
                                  assimp_light->mColorDiffuse.b });
  light->SetIntensity(2.0f);
  light->SetTranslation(assimp_light->mPosition.x,
    assimp_light->mPosition.y,
    assimp_light->mPosition.z);
  if (node == nullptr) {
    std::cout << "Light Node \'" << assimp_light->mName.C_Str()
      << "\' is not contained in scene graph." << std::endl;
  }
  else {
    light->SetTransform(BuildMatrix(node->mTransformation));
  }
  return light;
#else
  return nullptr;
#endif
}

std::shared_ptr<Material> AssimpLoader::BuildMaterial(Scene & scene, const aiMaterial* assimp_material) {
  std::shared_ptr<Material> material = std::make_shared<Material>();

  aiString name;
  if (aiReturn_SUCCESS == assimp_material->Get(AI_MATKEY_NAME, name)) {
    material->SetName(name.C_Str());
  }

  aiColor3D color;
  if (aiReturn_SUCCESS == assimp_material->Get(AI_MATKEY_COLOR_DIFFUSE, color)) {
    material->SetDiffuse(glm::vec3(color.r, color.g, color.b));
  }
  if (aiReturn_SUCCESS == assimp_material->Get(AI_MATKEY_COLOR_EMISSIVE, color)) {
    material->SetEmissive(glm::vec3(color.r, color.g, color.b));
  }
  if (aiReturn_SUCCESS == assimp_material->Get(AI_MATKEY_COLOR_SPECULAR, color)) {
    material->SetSpecular(glm::vec3(color.r, color.g, color.b));
  }
  float float_value;
  if (aiReturn_SUCCESS == assimp_material->Get(AI_MATKEY_SHININESS, float_value)) {
    material->SetShininess(float_value);
  }


  uint32_t texture_count = assimp_material->GetTextureCount(aiTextureType_DIFFUSE);
  if (texture_count == 1) {
    aiString path;
    if (aiReturn_SUCCESS == assimp_material->GetTexture(aiTextureType_DIFFUSE, 0, &path)) {
      auto texture = scene.GetTexture(path.C_Str());
      if (!texture) {
        texture = this->BuildTexture(scene, path, true);
      }
      material->SetTexture(texture, Material::ALBEDO_MAP_ID);
    }
  }
  texture_count = assimp_material->GetTextureCount(aiTextureType_SPECULAR);
  if (texture_count == 1) {
    aiString path;
    if (aiReturn_SUCCESS == assimp_material->GetTexture(aiTextureType_SPECULAR, 0, &path)) {
      auto texture = scene.GetTexture(path.C_Str());
      if (!texture) {
        texture = this->BuildTexture(scene, path);
      }
      material->SetTexture(texture, Material::SPECULAR_MAP_ID);
    }
  }

  texture_count = assimp_material->GetTextureCount(aiTextureType_HEIGHT);
  if (texture_count > 0) {
    aiString path;
    if (aiReturn_SUCCESS == assimp_material->GetTexture(aiTextureType_HEIGHT, 0, &path)) {
      auto texture = scene.GetTexture(path.C_Str());
      if (!texture) {
        texture = this->BuildTexture(scene, path);
      }
      if (texture->GetChannels() == 1) {
        //std::cout << "HEIGHT MAP " << path.C_Str() << std::endl;
        // Height map. TODO: Convert to normal map.
      }
      else {
        //std::cout << "NORMAL MAP " << path.C_Str() << std::endl;
        // Otherwise it's a Normal map.  Assimp seems to be unable to differentiate the two for
        // obj file formats.
      }
      material->SetTexture(texture, Material::NORMAL_MAP_ID);
    }
  }
  texture_count = assimp_material->GetTextureCount(aiTextureType_NORMALS);
  if (texture_count > 0) {
    aiString path;
    if (aiReturn_SUCCESS == assimp_material->GetTexture(aiTextureType_NORMALS, 0, &path)) {
      auto texture = scene.GetTexture(path.C_Str());
      if (!texture) {
        texture = this->BuildTexture(scene, path);
      }
      material->SetTexture(texture, Material::NORMAL_MAP_ID);
    }
  }

  texture_count = assimp_material->GetTextureCount(aiTextureType_OPACITY);
  if (texture_count > 0) {
    aiString path;
    if (aiReturn_SUCCESS == assimp_material->GetTexture(aiTextureType_OPACITY, 0, &path)) {
      auto texture = scene.GetTexture(path.C_Str());
      if (!texture) {
        texture = this->BuildTexture(scene, path);
      }
      material->SetTexture(texture, Material::ALPHA_MAP_ID);
    }
  }

  return material;
}

std::shared_ptr<Texture<uint8_t>> AssimpLoader::BuildTexture(Scene & scene, const aiString& path, bool srgb_to_linear) {
  aiString full_path("assets/");
  full_path.Append(path.C_Str());
  auto texture = TextureLoader::LoadFromFile(full_path.C_Str(), srgb_to_linear);
  scene.AddTexture(full_path.C_Str(), texture);
  return texture;
}

} // namespace renderer