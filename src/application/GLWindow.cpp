#include "GLWindow.h"

#include <iostream>
#include <map>

#include "GLFWManager.h"
#include "GLApplicationImpl.h"
#include "TiledDeferredRenderer.h"
#include "SceneImpl.h"
#include "FrameClock.h"

namespace renderer {

// 1000 ms divided by 60hz gives time step to check keyboard
static const float KEY_POLL_TIME_DELTA = 1000.0f / 60.0f;

GLWindow::GLWindow(const std::string& title,
                   GLApplication::Impl& application,
                   int32_t x_size,
                   int32_t y_size)
  : glfw_manager_{},
    window_title_ {title},
    x_window_size_{ x_size },
    y_window_size_{ y_size },
    vsync_{false},
    fullscreen_{ false },
    key_poll_time_{ KEY_POLL_TIME_DELTA },
    mouse_free_{ false },
    application_{ application },
    window_{ nullptr },
    renderer_{ new TiledDeferredRenderer( x_size, y_size ) } {

  window_ = glfwCreateWindow(x_window_size_, y_window_size_, window_title_.c_str(), nullptr, nullptr);
  if (!window_) {
    throw std::runtime_error("Failed to create window context [" + title + "]");
  }
  glfwSetWindowUserPointer(window_, this);

  auto resize_callback = [](GLFWwindow * w, int x_size, int y_size) {
    GLWindow* window = reinterpret_cast<GLWindow*>(glfwGetWindowUserPointer(w));
    window->ResizeCallback(x_size, y_size);
  };
  glfwSetWindowSizeCallback(window_, resize_callback);

  auto mouse_callback = [](GLFWwindow* w, double xpos, double ypos) {
    GLWindow* window = reinterpret_cast<GLWindow*>(glfwGetWindowUserPointer(w));
    window->MouseMovedCallback(xpos, ypos);
  };
  glfwSetCursorPosCallback(window_, mouse_callback);

  auto mouse_button_callback = [](GLFWwindow* w, int button, int action, int mods) {
    GLWindow* window = reinterpret_cast<GLWindow*>(glfwGetWindowUserPointer(w));
    window->MousePressedCallback(button, action, mods);
  };
  glfwSetMouseButtonCallback(window_, mouse_button_callback);
  glfwSetInputMode(window_, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
}

GLWindow::~GLWindow() {
  glfwDestroyWindow(window_);
}

void GLWindow::ResizeCallback(uint32_t size_x, uint32_t size_y) {
  if (size_x > 0 && size_y > 0) {
    std::cout << "Window resized to " << size_x << "x" << size_y << std::endl;
    renderer_->Resize(size_x, size_y);
    application_.GetScene().GetCamera()->Resize(size_x, size_y);
  }
}

void GLWindow::ToggleMouseFree() {
  mouse_free_ = !mouse_free_;
  if (!mouse_free_) {
    glfwSetInputMode(window_, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
  }
  else {
    glfwSetInputMode(window_, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
  }
}

void GLWindow::SetFullScreen() {
  auto monitor = glfwGetPrimaryMonitor();
  const GLFWvidmode* mode = glfwGetVideoMode(monitor);
  x_window_size_ = mode->width;
  y_window_size_ = mode->height;
  glfwSetWindowMonitor(window_, monitor, 0, 0, mode->width, mode->height,
                       mode->refreshRate);
  fullscreen_ = true;
}

void GLWindow::SetWindowed(uint32_t size_x, uint32_t size_y) {
  auto monitor = glfwGetPrimaryMonitor();
  const GLFWvidmode* mode = glfwGetVideoMode(monitor);
  x_window_size_ = size_x;
  y_window_size_ = size_y;
  glfwSetWindowMonitor(window_, nullptr, mode->width / 4, mode->height / 4,
                       size_x, size_y, mode->refreshRate);
  fullscreen_ = false;
}

void GLWindow::PollKeyStates(float dt) {
  // My lazy conversion method.
  static std::map< uint32_t, IKeyListener::Key > key_map = {
    { GLFW_KEY_W, IKeyListener::KEY_W },
    { GLFW_KEY_A, IKeyListener::KEY_A },
    { GLFW_KEY_S, IKeyListener::KEY_S },
    { GLFW_KEY_D, IKeyListener::KEY_D },
    { GLFW_KEY_F, IKeyListener::KEY_F },
    { GLFW_KEY_U, IKeyListener::KEY_U },
    { GLFW_KEY_I, IKeyListener::KEY_I },
    { GLFW_KEY_O, IKeyListener::KEY_O },
    { GLFW_KEY_J, IKeyListener::KEY_J },
    { GLFW_KEY_K, IKeyListener::KEY_K },
    { GLFW_KEY_L, IKeyListener::KEY_L },
    { GLFW_KEY_R, IKeyListener::KEY_R },
    { GLFW_KEY_T, IKeyListener::KEY_T },
    { GLFW_KEY_P, IKeyListener::KEY_P },
    { GLFW_KEY_SPACE, IKeyListener::KEY_SPACE },
    { GLFW_KEY_ENTER, IKeyListener::KEY_ENTER },
    { GLFW_KEY_EQUAL, IKeyListener::KEY_PLUS },
    { GLFW_KEY_MINUS, IKeyListener::KEY_MINUS }
  };

  if ((key_poll_time_ -= dt) <= 0) {
    key_poll_time_ = KEY_POLL_TIME_DELTA;
    // First check for special window keys
    // Bit of a hack but I don't really need a better solution right now...
    if (glfwGetKey(window_, GLFW_KEY_ESCAPE)) {
      glfwSetWindowShouldClose(window_, 1);
    }
    else if (glfwGetKey(window_, GLFW_KEY_F10)) {
      if (fullscreen_) {
        this->SetWindowed(640, 480);
      }
      else {
        this->SetFullScreen();
      }
    }
    else if (glfwGetKey(window_, GLFW_KEY_SPACE)) {
      this->ToggleMouseFree();
    }
    for (auto key_value : key_map) {
      auto state = glfwGetKey(window_, key_value.first);
      if (state == GLFW_PRESS) {
        application_.KeyNotify(key_value.second, IKeyListener::KEY_PRESSED);
      }
      else if (state == GLFW_RELEASE) {
        application_.KeyNotify(key_value.second, IKeyListener::KEY_RELEASED);
      }
    }
  }
}

void GLWindow::MouseMovedCallback(double xpos, double ypos) {
  application_.MouseNotify(xpos / x_window_size_, ypos / y_window_size_);
  // Restore Cursor Position.
  if (!mouse_free_) {
    double window_centre_x = x_window_size_ * 0.5;
    double window_centre_y = y_window_size_ * 0.5;
    glfwSetCursorPos(window_, window_centre_x, window_centre_y);
  }
}

void GLWindow::MousePressedCallback(int button, int action, int) {
  static std::map< uint32_t, IMouseListener::MouseButton> mouse_button_map = {
    { GLFW_MOUSE_BUTTON_1, IMouseListener::MOUSE_1 },
    { GLFW_MOUSE_BUTTON_2, IMouseListener::MOUSE_2 },
    { GLFW_MOUSE_BUTTON_3, IMouseListener::MOUSE_3 }
  };

  // Get mouse position.
  double xpos, ypos;
  glfwGetCursorPos(window_, &xpos, &ypos);
  // Event
  IMouseListener::MouseEvent event = IMouseListener::MOUSE_BUTTON_RELEASED;
  if (action == GLFW_PRESS) {
    event = IMouseListener::MOUSE_BUTTON_PRESSED;
  }
  else if (action == GLFW_REPEAT) {
    event = IMouseListener::MOUSE_BUTTON_HELD;
  }
  // Callback
  application_.MouseButtonNotify(xpos, ypos, mouse_button_map[button], event);
}

void GLWindow::WindowLoop() {
  glfw_manager_.BindWindow(this->window_);
  if (!vsync_) {
    glfwSwapInterval(0);
  }
  renderer_->InitRenderer();
  application_.InitCallback();
  this->ResizeCallback(x_window_size_, y_window_size_);

  FrameClock frame_clock;
  while (!glfwWindowShouldClose(this->window_)) {
    // Getting Frame time.
    float frame_time_ms = frame_clock.FrameTimeMs();
    frame_clock.FrameStart();
    // Callback to Application. (Convert back to ms)
    application_.FrameCallback(frame_time_ms);
    // Do render.
    renderer_->Render(application_.GetScene());
    glfwSwapBuffers(this->window_);
    frame_clock.FrameEnd();
    glfwPollEvents();
    this->PollKeyStates(frame_time_ms);
  }
}

}// namespace renderer
