#include "ObjFileParser.h"

#include <string>
#include <sstream>
#include <unordered_map>
#include <array>
#include <algorithm>
#include <memory>

#include "Scene.h"
#include "VertexBuffer.h"
#include "IndexBuffer.h"
#include "Mesh.h"

namespace renderer {

ObjFileParser::ObjFileParser(const std::string& file_name, bool parse)
  : obj_file_name_{ file_name },
    obj_file_stream_{ file_name },
    mtl_file_name_{},
    vertex_index_offset_{ 0 },
    normal_index_offset_{ 0 },
    tex_coord_index_offset_{ 0 },
    vertex_data_{},
    normal_data_{},
    tex_coord_data_{},
    vertex_index_{},
    normal_index_{},
    tex_coord_index_{},
    parsing_face_data_{ false },
    current_group_name_{ "" },
    mesh_list_{} {
  if (parse) {
    this->ParseFile();
  }
}

void ObjFileParser::LoadScene(Scene& scene) const {
  // For now just this, will expand as i add functionality.
  for(auto mesh : mesh_list_) {
    scene.AddRenderable(mesh);
  }
}

std::vector<std::shared_ptr<Mesh>> ObjFileParser::GetMeshList() const {
  return mesh_list_;
}

void ObjFileParser::ParseFile() {
  if (!obj_file_stream_.is_open()) {
    throw std::runtime_error("ObjFileParser: Could not open file " + obj_file_name_);
  }

  std::string line;
  while (std::getline(obj_file_stream_, line)) {
    ParseLine(line);
  }
  // We have reached the end of the file but have not created the final mesh
  // described in the obj file.
  if (!vertex_data_.empty()) {
    mesh_list_.push_back(this->BuildMesh());
  }
}

ObjFileParser::ObjEntry ObjFileParser::GetObjEntry(std::stringstream& ss) {
  std::string entry_token;
  ss >> entry_token;
  static std::unordered_map< std::string, ObjFileParser::ObjEntry > enum_map = {
    { "#", COMMENT },
    { "v", VERTEX },
    { "vn", NORMAL },
    { "vt", TEXCOORD },
    { "vp", PARAMETER },
    { "f", FACE },
    { "l", LINE },
    { "mtllib", MATERIAL_FILE },
    { "usemtl", USE_MATERIAL },
    { "g", GROUP },
    { "o", OBJECT },
    { "s", SMOOTHING_GROUP }
  };
  auto result = enum_map.find(entry_token);
  if (result == enum_map.end()) {
    return INVALID;
  }
  else {
    return result->second;
  }
}

void ObjFileParser::ParseLine(std::string& line) {
  std::stringstream ss(line);
  // First token eaten by GetObjEntry.  Just data should be left.
  ObjEntry object_entry_type = this->GetObjEntry(ss);
  switch (object_entry_type) {
  case VERTEX: {
    // We have found a vertex after parsing face data, for now treat this as a new mesh.
    if (parsing_face_data_) {
      mesh_list_.push_back(this->BuildMesh());
      parsing_face_data_ = false;
    }
    this->ExtractToVector<float, 3>(ss, vertex_data_);
    break;
  }
  case NORMAL: {
    this->ExtractToVector<float, 3>(ss, normal_data_);
    break;
  }
  case TEXCOORD: {
    this->ExtractToVector<float, 2>(ss, tex_coord_data_);
    break;
  }
  case FACE: {
    parsing_face_data_ = true;
    this->ParseFaceEntry(ss);
    break;
  }
  case GROUP: {
    this->ExtractValue<std::string>(ss, current_group_name_);
    break;
  }
  case MATERIAL_FILE: {
    this->ExtractValue<std::string>(ss, mtl_file_name_);
    break;
  }
  case COMMENT: {
    break;
  }
  case USE_MATERIAL:
  case PARAMETER:
  case LINE:
  case SMOOTHING_GROUP:
  case OBJECT: // Make new Model.
  default: {
    std::cerr << "ParseLine: Unsupported token [" << object_entry_type << "] Found. Raw text = " << line << "\n";
    break;
  }
  }
}

// This could probably all be done much more effeciently with raw pointers
// but i just wanted to do something quick and dirty to get started on the 
// actual project.
void ObjFileParser::ParseFaceEntry(std::stringstream & ss) {
  // Buffers for storing each index face entry before triangulation.
  std::vector<uint32_t> pre_triangulated_vertex_index;
  std::vector<uint32_t> pre_triangulated_normal_index;
  std::vector<uint32_t> pre_triangulated_texcoord_index;
  std::string face_token;
  while (ss >> face_token) {
    ParseFaceToken(face_token, pre_triangulated_vertex_index,
      pre_triangulated_normal_index, pre_triangulated_texcoord_index);
  }
  // Triangulate polygon face.
  for (uint32_t i = 2; i < pre_triangulated_vertex_index.size(); i++) {
    vertex_index_.push_back(pre_triangulated_vertex_index[0]);
    vertex_index_.push_back(pre_triangulated_vertex_index[i - 1]);
    vertex_index_.push_back(pre_triangulated_vertex_index[i]);
    if (pre_triangulated_vertex_index.size() == pre_triangulated_normal_index.size()) {
      normal_index_.push_back(pre_triangulated_normal_index[0]);
      normal_index_.push_back(pre_triangulated_normal_index[i - 1]);
      normal_index_.push_back(pre_triangulated_normal_index[i]);
    }

    if (pre_triangulated_vertex_index.size() == pre_triangulated_texcoord_index.size()) {
      tex_coord_index_.push_back(pre_triangulated_texcoord_index[0]);
      tex_coord_index_.push_back(pre_triangulated_texcoord_index[i - 1]);
      tex_coord_index_.push_back(pre_triangulated_texcoord_index[i]);
    }
  }
}

void ObjFileParser::ParseFaceToken(const std::string& token,
  std::vector<uint32_t>& vertex,
  std::vector<uint32_t>& normal,
  std::vector<uint32_t>& tex_coord) {
  std::stringstream ss(token);
  uint64_t delimiter_count = std::count(std::begin(token), std::end(token), '/');
  uint32_t face_entry_count = 0;
  std::string face_entry;
  std::array< uint32_t, 3 > entry;
  while (std::getline(ss, face_entry, '/')) {
    if (face_entry.find_first_not_of(' ') != std::string::npos) {
      entry[face_entry_count] = std::stoi(face_entry);
      face_entry_count++;
    }
  }
  // Indexes are calculated as (Raw obj file index) - (offset saved from last group) - 1
  uint32_t vertex_index = entry[0] - vertex_index_offset_ - 1;
  assert(vertex_index >= 0);
  vertex.push_back(vertex_index);
  // x/y/z
  if (delimiter_count == 2 && face_entry_count == 3) {
    uint32_t tex_coord_index = entry[1] - tex_coord_index_offset_ - 1;
    uint32_t normal_index = entry[2] - normal_index_offset_ - 1;
    assert(tex_coord_index >= 0);
    assert(normal_index >= 0);
    tex_coord.push_back(tex_coord_index);
    normal.push_back(normal_index);
  }
  // x//y
  else if (delimiter_count == 2 && face_entry_count == 2) {
    uint32_t normal_index = entry[1] - normal_index_offset_ - 1;
    assert(normal_index >= 0);
    normal.push_back(normal_index);
  }
  // x/y
  else if (delimiter_count == 1 && face_entry_count == 2) {
    uint32_t tex_coord_index = entry[1] - tex_coord_index_offset_ - 1;
    assert(tex_coord_index >= 0);
    tex_coord.push_back(tex_coord_index);
  }
}

// Have to align all data in vertex buffers as OpenGL does not support multiple index
// buffers. Again, naive solution, probably a faster and smarter way of doing this.
void ObjFileParser::AlignVertexAttributes() {
  std::vector<float> temp_vertex_attribute;
  temp_vertex_attribute.reserve(vertex_data_.size());
  if (!normal_index_.empty()) {
    for (uint32_t i = 0; i < normal_index_.size(); i++) {
      temp_vertex_attribute[vertex_index_[i]] = normal_data_[normal_index_[i]];
    }
    normal_data_ = temp_vertex_attribute;
    temp_vertex_attribute.clear();
  }
  if (!tex_coord_data_.empty()) {
    for (uint32_t i = 0; i < tex_coord_index_.size(); i++) {
      temp_vertex_attribute[vertex_index_[i]] = tex_coord_data_[tex_coord_index_[i]];
    }
    tex_coord_data_ = temp_vertex_attribute;
  }
}

// Create Vertex buffer and reset our temp buffers.
VertexBuffer* ObjFileParser::BuildVertexBuffer() {
  std::unique_ptr<VertexBuffer> vertex_buffer = std::make_unique<VertexBuffer>();
  vertex_buffer->AddVertexAttributeData(VertexBuffer::POSITION, 3, 0, vertex_data_);
  if (!normal_data_.empty()) {
    vertex_buffer->AddVertexAttributeData(VertexBuffer::NORMAL, 3, 0, normal_data_);
  }
  if (!tex_coord_data_.empty()) {
    vertex_buffer->AddVertexAttributeData(VertexBuffer::TEXCOORD, 2, 0, tex_coord_data_);
  }
  vertex_data_.clear();
  normal_data_.clear();
  tex_coord_data_.clear();
  return vertex_buffer.release();
}

// Create Index buffer and reset our temp buffers.
IndexBuffer* ObjFileParser::BuildIndexBuffer() {
  std::unique_ptr<IndexBuffer> index_buffer = std::make_unique<IndexBuffer>();
  index_buffer->AddIndexData(vertex_index_);
  vertex_index_offset_ = vertex_index_offset_ + *std::max_element(std::begin(vertex_index_), std::end(vertex_index_));
  vertex_index_.clear();
  if (!normal_index_.empty()) {
    normal_index_offset_ = normal_index_offset_ + *std::max_element(std::begin(normal_index_), std::end(normal_index_));
    normal_index_.clear();
  }
  if (!tex_coord_index_.empty()) {
    tex_coord_index_offset_ = tex_coord_index_offset_ + *std::max_element(std::begin(tex_coord_index_), std::end(tex_coord_index_));
    tex_coord_index_.clear();
  }
  return index_buffer.release();
}

std::shared_ptr<Mesh> ObjFileParser::BuildMesh() {
  this->AlignVertexAttributes();
  std::shared_ptr<Mesh> mesh = std::make_shared<Mesh>();
  mesh->GetGeometry().SetVertexBuffer(std::unique_ptr<VertexBuffer>(BuildVertexBuffer()));
  mesh->GetGeometry().SetIndexBuffer(std::unique_ptr<IndexBuffer>(BuildIndexBuffer()));
  return mesh;
}

} // namespace renderer