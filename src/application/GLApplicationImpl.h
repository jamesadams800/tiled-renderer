#ifndef OGL_GLAPPLICATIONIMPL_H_
#define OGL_GLAPPLICATIONIMPL_H_

#include <vector>
#include <memory>

#include "GLApplication.h"
#include "Scene.h"

namespace renderer {

class GLWindow;

class GLApplication::Impl {
public:
  explicit Impl(GLApplication& application, const std::string& name);
  virtual ~Impl();

  void Run();

  // Scene Initialization for rendering
  virtual void InitCallback() final;

  // Callback called once a frame.  Any scene modification to be done in here
  // dt is time delta in ms.
  virtual void FrameCallback(float dt) final;

  const Scene::Impl& GetScene() const {
    return *library_scene_;
  }

  Scene::Impl& GetScene() {
    return *library_scene_;
  }

  void AddKeyListener(IKeyListener* listener);
  void AddMouseListener(IMouseListener* listener);
  void KeyNotify(IKeyListener::Key key_code, IKeyListener::KeyEvent);
  void MouseNotify(double dx, double dy);
  void MouseButtonNotify(double xpos, double ypos,
                          IMouseListener::MouseButton,
                          IMouseListener::MouseEvent);

protected:
  GLApplication& application_;
  std::unique_ptr<GLWindow> window_;
  std::vector<IKeyListener*> key_listeners_;
  std::vector<IMouseListener*> mouse_listeners_;
  std::unique_ptr<Scene::Impl> library_scene_;
  std::unique_ptr<Scene> application_scene_;
};

} // namespace renderer 

#endif // OGL_GLAPPLICATIONIMPL_H_
