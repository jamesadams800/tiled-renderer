#include "GLApplicationImpl.h"

#include "GLWindow.h"
#include "Scene.h"
#include "SceneImpl.h"

namespace renderer {

GLApplication::Impl::Impl(GLApplication & application, const std::string& name)
  : application_{ application },
    window_{ std::make_unique<GLWindow>(name, *this, 1024, 768) },
    key_listeners_{},
    mouse_listeners_{},
    library_scene_{nullptr},
    application_scene_{ nullptr } {
}

GLApplication::Impl::~Impl() {
}

void GLApplication::Impl::Run() {
  window_->WindowLoop();
}

void GLApplication::Impl::InitCallback() {
  // Scene must be created here as it is after BindWindow has been called in the 
  // window main loop. Otherwise all OpenGL functions fail to work.
  library_scene_ = std::make_unique<Scene::Impl>();
  application_scene_ = std::make_unique<Scene>(*library_scene_);
  application_.InitScene(*application_scene_);
  library_scene_->UpdateBuffers();
}

void GLApplication::Impl::FrameCallback(float dt) {
  application_.Update(*application_scene_, dt);
  library_scene_->UpdateBuffers();
}

void GLApplication::Impl::AddKeyListener(IKeyListener * listener) {
  key_listeners_.push_back(listener);
}

void GLApplication::Impl::AddMouseListener(IMouseListener * listener) {
  mouse_listeners_.push_back(listener);
}

void GLApplication::Impl::KeyNotify(IKeyListener::Key key_code, IKeyListener::KeyEvent key_event) {
  for (auto* listener : key_listeners_) {
    listener->KeyEventPerformed(key_code, key_event);
  }
}

void GLApplication::Impl::MouseNotify(double xpos, double ypos) {
  for (auto* listener : mouse_listeners_) {
    listener->MouseMoved(xpos, ypos);
  }
}

void GLApplication::Impl::MouseButtonNotify(double xpos,
                                            double ypos, 
                                            IMouseListener::MouseButton button,
                                            IMouseListener::MouseEvent event) {
  for (auto* listener : mouse_listeners_) {
    listener->MousePressed(xpos, ypos, button, event);
  }
}

} // namespace renderer
