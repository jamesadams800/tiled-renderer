#ifndef OGL_GLWINDOW_H_
#define OGL_GLWINDOW_H_

#include <string>
#include <memory>

#include "Renderer.h"
#include "GLApplication.h"
#include "GLFWManager.h"

struct GLFWwindow;

namespace renderer {

class GLWindow {
public:
  explicit GLWindow(const std::string& title, GLApplication::Impl& application,
                    int32_t x_size = 1024, int32_t y_size = 768);
  ~GLWindow();

  void WindowLoop();
  void SetFullScreen();
  void SetWindowed(uint32_t size_x, uint32_t size_y);
  void ResizeCallback(uint32_t size_x, uint32_t size_y);
  void ToggleMouseFree();
  void PollKeyStates(float dt);
  void MouseMovedCallback(double xpos, double ypos);
  void MousePressedCallback(int button, int action, int mods);
  void SetVsync(bool vsync) {
    vsync_ = vsync;
  }
private:
  GLFWManager glfw_manager_;
  std::string window_title_;
  int32_t x_window_size_;
  int32_t y_window_size_;
  bool vsync_;
  bool fullscreen_;
  float key_poll_time_;
  bool mouse_free_;
  GLApplication::Impl&  application_;
  GLFWwindow* window_;
  std::unique_ptr<Renderer> renderer_;
};

}// namespace renderer

#endif //OGL_GLWINDOW_H_