#ifndef OGL_FRAMECLOCK_H_
#define OGL_FRAMECLOCK_H_

#include <chrono>

namespace renderer {

class FrameClock {

public:
  FrameClock()
    : frame_time_duration_{},
    frame_start_{} {
  }

  void FrameStart() {
    frame_start_ = std::chrono::high_resolution_clock::now();
  }
  void FrameEnd() {
    frame_time_duration_ = std::chrono::high_resolution_clock::now() - frame_start_;
  }
  float FrameTimeMs() {
    std::chrono::microseconds frame_time =
      std::chrono::duration_cast<std::chrono::microseconds>(frame_time_duration_);
    return frame_time.count() / 1000.0f;
  }
private:

  std::chrono::duration<float> frame_time_duration_;
  std::chrono::steady_clock::time_point frame_start_;
};

} // namespace renderer
#endif // OGL_FRAMECLOCK_H_