#include "Object.h"

namespace renderer {

void Object::BaseDeleter::operator()(Object * base) {
  delete base;
}

} // namespace renderer 
