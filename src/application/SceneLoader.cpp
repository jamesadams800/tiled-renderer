#include "SceneLoader.h"

#include <iostream>

#include "ObjFileParser.h"
#include "PerspectiveCamera.h"
#include "AssimpLoader.h"

namespace renderer {

const std::string SceneLoader::kObjExtension_ = ".obj";

bool SceneLoader::FileNameEndsIn(const std::string & file_name, const std::string & extension) {
  if (file_name.length() > extension.length()) {
    if (!file_name.compare(file_name.length() - extension.length(),
        extension.length(), extension)) {
      return true;
    }
  }
  return false;
}

SceneLoader::FileType SceneLoader::GetFileType(const std::string & file_name) {
  if (SceneLoader::FileNameEndsIn(file_name, kObjExtension_)) {
    return OBJ;
  }

  return UNKNOWN;
}

void SceneLoader::LoadScene(std::string file_name, Scene & output_scene, LoaderType type) {
  if (type == DEFAULT) {
    auto mesh_list = SceneLoader::LoadMeshesFromFile(file_name, output_scene, type);
    for (auto mesh : mesh_list) {
      output_scene.AddRenderable(mesh);
    }
  }
  else if (type == ASSIMP) {
    AssimpLoader loader;
    loader.LoadScene(file_name, output_scene);
  }
}

std::vector<std::shared_ptr<Mesh>> SceneLoader::LoadMeshesFromFile(std::string file_name,
                                                                   Scene & output_scene, 
                                                                   LoaderType type) {
  std::vector<std::shared_ptr<Mesh>> list;
  if (type == ASSIMP) {
    AssimpLoader loader;
    list = loader.ExtractMeshes(file_name, output_scene);
  }
  switch (SceneLoader::GetFileType(file_name)) {
    case OBJ: {
      ObjFileParser parser(file_name);
      list = parser.GetMeshList();
      break;
    }
    case UNKNOWN:
    default: {
      throw std::runtime_error("ModelLoader: File[" + file_name +
                               "] is of unrecognised file type.");
    }
  }

  return list;
}

} // namespace renderer
