#ifndef OGL_ASSIMPLOADER_H_
#define OGL_ASSIMPLOADER_H_

#include <string>
#include <vector>
#include <map>

#pragma warning(push, 0)  
#include <glm/glm.hpp>
#include <glm/ext.hpp>
#pragma warning(pop)

struct aiNode;
struct aiMesh;
struct aiCamera;
struct aiLight;
struct aiMaterial;
struct aiString;

namespace renderer {

// Application foward declarations.
class Light;
class Mesh;
class Camera;
class Material;
template <class T> class Texture;
class Scene;

class AssimpLoader {
public:
  AssimpLoader();
  void LoadScene(const std::string& file_name, Scene& scene);
  std::vector<std::shared_ptr<Mesh>> ExtractMeshes(const std::string& file_name, Scene& scene);

private:
  uint64_t GetGlobalMaterialIndex(uint32_t local_index);

  static uint64_t file_material_offset_;
  uint32_t material_count_;
  std::map<uint64_t, uint64_t> file_material_id_to_scene_id_;

  std::shared_ptr<Texture<uint8_t>> dummy_normal_map_;
  std::shared_ptr<Texture<uint8_t>> dummy_alpha_map_;

  std::shared_ptr<Mesh> BuildMesh(Scene& scene, const aiMesh*, const aiNode*);
  std::shared_ptr<Camera> BuildCamera(Scene& scene, const aiCamera*, const aiNode*);
  std::shared_ptr<Light> BuildLight(Scene& scene, const aiLight*, const aiNode*);
  std::shared_ptr<Material> BuildMaterial(Scene& scene, const aiMaterial*);
  std::shared_ptr<Texture<uint8_t>> BuildTexture(Scene& scene, const aiString& path, bool srgb_to_linear = false);
  const aiNode* FindNode(const aiNode* node, const aiString& component_name);
};

} // namespace renderer

#endif // OGL_ASSIMPLOADER_H_