#ifndef OGL_GLFWMANAGER_H_
#define OGL_GLFWMANAGER_H_

#include <gl/glew.h>
#include <GLFW/glfw3.h>

namespace renderer {

class GLWindow;

class GLFWManager {
public:
  friend GLWindow;
  ~GLFWManager();

  void BindWindow(GLFWwindow* window);

  void PrintGLVersion() const;
private:
  GLFWManager();
};

} // namespace renderer

#endif // OGL_GLFWMANAGER_H_