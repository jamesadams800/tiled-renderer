#ifndef OGL_TONEMAPPASS_H_
#define OGL_TONEMAPPASS_H_

#include "RenderPass.h"

namespace renderer {

class TonemapPass : public RenderPass {
public:
  enum class Technique {
    REINHARD,
    FILMIC
  };

  TonemapPass(ShaderLibrary& lib)
    : RenderPass{ lib },
      technique_{ Technique::REINHARD },
      gamma_{ 2.2f } {
  }

  void SetTechnique(Technique tech) {
    technique_ = tech;
  }

  void SetGamma(float gamma) {
    gamma_ = gamma;
  }

  float GetGamma() const {
    return gamma_;
  }

  void Execute(const Scene::Impl&, const Framebuffer& input, Framebuffer & output) override;

protected:
  void InitPass() override;
private:
  Technique technique_;
  float gamma_;
};

} // namespace renderer

#endif // OGL_TONEMAPPASS_H_