#ifndef OGL_TILEDDEFERREDLIGHTINGPASS_H_
#define OGL_TILEDDEFERREDLIGHTINGPASS_H_

#include "RenderPass.h"
#include "Mesh.h"
#include "TextureFramebuffer.h"
#include "ShaderBuffer.h"

namespace renderer {

class TiledDeferredLightingPass : public RenderPass {
public:
  TiledDeferredLightingPass(ShaderLibrary& lib)
    : RenderPass{ lib },
      tile_data_{nullptr},
      current_work_group_size_x_{ 0 },
      current_work_group_size_y_{ 0 } {
  }
  virtual ~TiledDeferredLightingPass() {}

  void Execute(const Scene::Impl&, const Framebuffer& input, Framebuffer & output) override;

protected:
  void InitPass() override;

private:
  struct TileData : Std140ShaderObject {
    glm::vec4 frustum_planes[4];
    glm::vec4 aabb_corners[2];
    uint32_t tile_mask;
  };
  void ResizeStorage(uint32_t work_group_x, uint32_t work_group_y);
  std::unique_ptr<StorageBuffer<TileData[], GL_STATIC_COPY>> tile_data_;
  
  uint32_t current_work_group_size_x_;
  uint32_t current_work_group_size_y_;
  static const uint32_t TILE_SIZE_X = 16;
  static const uint32_t TILE_SIZE_Y = 16;
};

} // namespace renderer

#endif // OGL_TILEDDEFERREDLIGHTINGPASS_H_