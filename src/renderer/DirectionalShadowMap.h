#ifndef OGL_DIRECTIONALSHADOWMAP_H_
#define OGL_DIRECTIONALSHADOWMAP_H_

#include "ShadowMap.h"

namespace renderer {

class DirectionalShadowMap : public ShadowMap {
public:
  static constexpr uint32_t MAX_DIRECTIONAL_SHADOW_MAPS = 5;

  explicit DirectionalShadowMap();
  explicit DirectionalShadowMap(const glm::vec3& light_direction);
  explicit DirectionalShadowMap(uint32_t x, uint32_t y, const glm::vec3& light_direction);

  uint32_t BindRead(uint32_t start) const override {
    this->Framebuffer::BindRead();
    glActiveTexture(GL_TEXTURE0 + start);
    glBindTexture(GL_TEXTURE_2D, depth_texture_);
    return 1;
  }

  const glm::mat4& GetLightViewProjection() const {
    return light_view_matrix_;
  }

  virtual void UpdateLightDirection(const glm::vec3& direction);
protected:
  glm::vec3 light_direction_;
private:
  void ResizeTarget(uint32_t x, uint32_t y) override;
  void CalculateViewProjectionMatrix();

  // AABB containing all renderables that could act as shadow casters.
  glm::mat4 light_view_matrix_;
};

} // namespace renderer

#endif // OGL_DIRECTIONALSHADOWMAP_H_