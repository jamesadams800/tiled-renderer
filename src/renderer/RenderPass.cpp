#include "RenderPass.h"

#include <GL/glew.h>

#include "Mesh.h"

namespace renderer {

std::vector<float> RenderPass::kFsqPositions_ = {
  -1.0f, -1.0f, 0.0f,
  1.0f, -1.0f, 0.0f,
  -1.0f,  1.0f, 0.0f,
  -1.0f,  1.0f, 0.0f,
  1.0f, -1.0f, 0.0f,
  1.0f,  1.0f, 0.0f
};

std::vector<float> RenderPass::kFsqTexCoords_ = {
  0.0f, 0.0f,
  1.0f, 0.0f,
  0.0f, 1.0f,
  0.0f, 1.0f,
  1.0f, 0.0f,
  1.0f, 1.0f
};

std::unique_ptr<Mesh> RenderPass::kFsq_ = nullptr;

void RenderPass::Init() {
  if (!kFsq_) {
    auto vertex_buffer = std::make_unique<VertexBuffer>();
    vertex_buffer->AddVertexAttributeData(VertexBuffer::POSITION, 3, 0, kFsqPositions_);
    vertex_buffer->AddVertexAttributeData(VertexBuffer::TEXCOORD, 2, 0, kFsqTexCoords_);
    kFsq_.reset(new Mesh(std::move(vertex_buffer)));
  }
  this->InitPass();
}

void RenderPass::DrawFullScreenQuad() const {
  kFsq_->GetGeometry().Bind();
  glDrawArrays(GL_TRIANGLES, 0, 6);
}

} // namespace renderer
