#include "DirectionalShadowMap.h"

#include <iostream>

#include "ShadowPass.h"

namespace renderer {

DirectionalShadowMap::DirectionalShadowMap()
  : DirectionalShadowMap(glm::vec3{ 0.0f, -1.0f, 0.0f }) {
}

DirectionalShadowMap::DirectionalShadowMap(const glm::vec3& light_direction)
  : DirectionalShadowMap(SHADOW_MAP_X_RESOLUTION,
                         SHADOW_MAP_Y_RESOLUTION,
                         light_direction) {
}

DirectionalShadowMap::DirectionalShadowMap(uint32_t x,
                                           uint32_t y,
                                           const glm::vec3 & light_direction)
  : ShadowMap(x, y),
    light_direction_{ light_direction },
    light_view_matrix_{ 1 } {
  this->CalculateViewProjectionMatrix();
}

void DirectionalShadowMap::UpdateLightDirection(const glm::vec3& direction) {
  light_direction_ = direction;
  this->CalculateViewProjectionMatrix();
}

void DirectionalShadowMap::ResizeTarget(uint32_t, uint32_t) {}

void DirectionalShadowMap::CalculateViewProjectionMatrix() {
  // TODO: Implement this? Kind of redundant now cascaded shadow maps are in.
}

} // namespace renderer
