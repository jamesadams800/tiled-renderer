#ifndef OGL_DEFERREDGEOMETRYPASS_H_
#define OGL_DEFERREDGEOMETRYPASS_H_

#include "RenderPass.h"

namespace renderer {

class DeferredGeometryPass : public RenderPass {
public:
  DeferredGeometryPass(ShaderLibrary& lib)
    : RenderPass{ lib } {
  }
  virtual ~DeferredGeometryPass() {}

  void Execute(const Scene::Impl&, const Framebuffer& input, Framebuffer & output) override;

protected:
  void InitPass() override;
private:
};

} // namespace renderer

#endif // OGL_DEFERREDGEOMETRYPASS_H_