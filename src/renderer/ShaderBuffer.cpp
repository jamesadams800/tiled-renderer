#include "ShaderBuffer.h"

namespace renderer {

std::unique_ptr<BufferBase::BindingPointAllocator> BufferBase::binding_allocator_ = nullptr;
std::mutex BufferBase::allocator_construct_lock_;

BufferBase::BufferBase() 
  : binding_point_{ 0 } {
  // This is far from ideal.  I dislike everything about the way in which this has been done.
  // Quick and dirty to make the allocator for block IDs static rather than designing the
  // system correctly.
  if (!binding_allocator_) {
    std::unique_lock<std::mutex> lock(allocator_construct_lock_);
    if (!binding_allocator_) {
      binding_allocator_.reset(new BindingPointAllocator());
    }
  }
  binding_point_ = binding_allocator_->AllocateIndex();
}

BufferBase::~BufferBase() {
  binding_allocator_->FreeIndex(binding_point_);
}

BufferBase::BindingPointAllocator::BindingPointAllocator() 
  : set_lock_{},
  available_indices_{} {
  for (GLuint i = 0; i < MAX_BINDING_POINTS; i++) {
    available_indices_.emplace(i);
  }
}

BufferBase::BindingPointAllocator::~BindingPointAllocator() {
}

GLuint BufferBase::BindingPointAllocator::AllocateIndex() {
  std::unique_lock<std::mutex> lock(set_lock_);
  auto front = available_indices_.begin();
  if (front == available_indices_.end()) {
    throw std::runtime_error("BindingPointAllocator::AllocateIndex : Unable to allocate "
                             "binding point for uniform, all binding points allocated.");
  }
  GLuint index = *front;
  available_indices_.erase(front);
  return index;
}

void BufferBase::BindingPointAllocator::FreeIndex(GLuint index) {
  std::unique_lock<std::mutex> lock(set_lock_);
  auto find = available_indices_.find(index);
  if (find != available_indices_.end()) {
    throw std::runtime_error("BindingPointAllocator::FreeIndex : Attempting to"
                             " free unallocated binding index " + index);
  }
  available_indices_.insert(index);
}

} // namespace renderer