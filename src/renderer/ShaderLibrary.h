#ifndef OGL_ShaderLibrary_H_
#define OGL_ShaderLibrary_H_

#include <unordered_map>
#include <memory>
#include <string>
#include <filesystem>
#include <thread>
#include <mutex>

#include "Shader.h"
#include "Material.h"

#ifdef _EXPERIMENTAL_FILESYSTEM_
namespace fs = std::experimental::filesystem;
#else
namespace fs = std::filesystem;
#endif

namespace renderer {

class ShaderLibrary {
public:
  explicit ShaderLibrary(const std::string& directory = DEFAULT_SHADER_DIRECTORY);
  ~ShaderLibrary();

  const Shader* GetGeometryPassShader(Material::LayerPresence presence);
  const Shader* GetShader(const std::string& name);
  void Init();
  void CreateShader(const std::string& shader_name);

private:
  struct ShaderEntry {
    std::vector<fs::directory_entry> sources;
    // Only need to store one last write as all shaders will
    // need to be recompiled anyway...
    fs::file_time_type last_write;
    // Swap shader holds partially constructed shader, containing all read source.
    // I've seperated these out onto another thread as the file operations are slow.
    std::unique_ptr<Shader> swap_shader;
    std::unique_ptr<Shader> shader;
  };
  std::unordered_map<std::string, ShaderEntry> shader_storage_;

  void RebuildShaderEntry(ShaderEntry& entry);
  void CheckEntries();
  void CheckShaderDirectory();

  static constexpr const char* DEFAULT_SHADER_DIRECTORY = "assets/shaders/";
  std::string shader_directory_;

  std::unordered_map<Material::LayerPresence, ShaderEntry*> geometry_pass_shaders_;
  // Work around until i write a GLSL code generator.
  void RegisterGeometryPassShaders();
  static constexpr const char * GEOMETRY_PASS_SHADER_SUFFIX = "GeometryPass";
  static std::unordered_map<char, Material::Layers> kGeometryPresence_;

  void WatcherThreadMain();
  std::unique_ptr<Shader> CreateShaderFromSources(const std::vector<fs::directory_entry>& sources);

  // Monitor thread.
  bool exit_thread_;
  std::unique_ptr<std::thread> file_watcher_thread_;
  std::mutex file_watcher_thread_mutex_;
};

} // namespace renderer 

#endif // OGL_ShaderLibrary_H_
