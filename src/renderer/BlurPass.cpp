#include "BlurPass.h"

#include <assert.h>

#include "Framebuffer.h"
#include "ShaderLibrary.h"

namespace renderer {


void BlurPass::InitPass() {
  blur_kernel_ = std::make_unique<UniformBuffer<Std140BoundedArray<float, MAX_KERNEL_SIZE>>>();
  this->GenerateBlurKernel();
}


void BlurPass::Execute(const Scene::Impl&, const Framebuffer& input, Framebuffer& output) {
  assert(pingpong_);
  if (pingpong_->GetX() != input.GetX() ||
      pingpong_->GetY() != input.GetY()) {
    pingpong_->Resize(input.GetX(), input.GetY());
  }
  const Shader* shader = shader_lib_.GetShader("Blur");
  assert(shader);
  shader->Attach();
  input.BindRead();
  pingpong_->BindWrite();
  glClear(GL_COLOR_BUFFER_BIT);
  shader->BindUniform("BlurKernel", *blur_kernel_);
  shader->BindUniform("input_attachment", attachment_);
  shader->BindUniform("output_attachment", 0);
  shader->BindUniform("pass", 1);
  this->DrawFullScreenQuad();

  pingpong_->BindRead();
  output.BindWrite();
  shader->BindUniform("input_attachment", 0);
  shader->BindUniform("output_attachment", attachment_);
  shader->BindUniform("pass", 2);
  this->DrawFullScreenQuad();
}

void BlurPass::GenerateBlurKernel() {
  switch (technique_) {
    case Technique::BOX: this->GenerateBoxKernel(); break;
    case Technique::GAUSSIAN: this->GenerateGaussianKernel(); break;
    default: this->GenerateGaussianKernel(); break;
  }
}

void BlurPass::GenerateBoxKernel() {
  const float kernel_value = 1.0f / kernel_size_;
  (*blur_kernel_)->element_count = kernel_size_;
  for (uint32_t i = 0; i < kernel_size_; i++) {
    (*blur_kernel_)->data[i] = kernel_value;
  }
  blur_kernel_->UpdateBuffer();
}

void BlurPass::GenerateGaussianKernel() {
  (*blur_kernel_)->element_count = kernel_size_;
  int32_t kernel_mid_index = kernel_size_ / 2;
  for (int32_t i = 0; i < int32_t(kernel_size_); i++) {
    (*blur_kernel_)->data[i] = 
      this->GenerateGaussianWeight(sigma_, float(std::abs(kernel_mid_index - i)));
  }
  blur_kernel_->UpdateBuffer();
}

float BlurPass::GenerateGaussianWeight(float sigma, float distance) {
  static constexpr float PI = 3.1415f;
  float exponent = -((distance * distance) / 2.0f * (sigma * sigma));
  return (1.0f / std::sqrtf(2.0f * PI * (sigma * sigma)) * std::exp(exponent));
}

} // namespace renderer
