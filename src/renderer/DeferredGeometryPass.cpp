#include "DeferredGeometryPass.h"

#include "SceneImpl.h"
#include "Framebuffer.h"
#include "Shader.h"
#include "ShaderLibrary.h"
#include "RenderVisitors.h"

namespace renderer {

void DeferredGeometryPass::InitPass() {
}

void DeferredGeometryPass::Execute(const Scene::Impl& scene, const Framebuffer&, Framebuffer& output) {
  output.BindWrite();
  glViewport(0, 0, output.GetX() , output.GetY());
  glEnable(GL_CULL_FACE);
  glCullFace(GL_BACK);
  glEnable(GL_DEPTH_TEST);
  glDepthMask(GL_TRUE);
  glDisable(GL_BLEND);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glm::vec3 camera_position = scene.GetCamera()->GetPosition();
  const auto& projection_matrix = scene.GetCamera()->GetProjectionMatrix();
  const auto& view_matrix = scene.GetCamera()->GetViewMatrix();

  glm::mat4 projection_view = projection_matrix * view_matrix;
  uint32_t material_id = 0;
  Material::LayerPresence layers_required = 0xFFFFFFFF;
  const Shader* geometry_pass_shader = nullptr;
  FillRenderVisitor visitor;
  for (auto& renderable : scene.GetRenderables()) {
    if (auto* material = renderable->GetMaterial()) {
      // Only switch geometry pass shader if the current material requires a new shader type.
      // This prevents redundant shader switches.
      if (layers_required != material->GetLayersPresent()) {
        geometry_pass_shader = shader_lib_.GetGeometryPassShader(material->GetLayersPresent());
        geometry_pass_shader->Attach();
        layers_required = material->GetLayersPresent();
      }
      // Check we are not performing a redundant material bind.
      if (material_id != material->GetUuid()) {
        material_id = material->GetUuid();
        material->Bind();
        geometry_pass_shader->BindUniform("MaterialConstants", material->GetConstantsUniform());
      }
      glm::mat4 model_matrix = renderable->GetWorldTransform();
      geometry_pass_shader->BindUniform("mvp_matrix", projection_view * model_matrix);
      geometry_pass_shader->BindUniform("normal_matrix", glm::transpose(glm::inverse(model_matrix)));
      geometry_pass_shader->BindUniform("model_matrix", model_matrix);
    }
    renderable->Accept(visitor);
  }
}

} // namespace renderer
