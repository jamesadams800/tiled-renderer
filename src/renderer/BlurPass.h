#ifndef OGL_BLURPASS_H_
#define OGL_BLURPASS_H_

#include <assert.h>

#include "ShaderBuffer.h"
#include "RenderPass.h"

namespace renderer {

class BlurPass : public RenderPass {
public:
  template <class T>
  BlurPass(ShaderLibrary& lib)
    : pingpong_{ std::make_unique<T>() },
      attachment_{0},
      sigma_{DEFAULT_SIGMA},
      kernel_size_{7},
      technique_{ Technique::GAUSSIAN },
      blur_kernel_{nullptr},
      RenderPass(lib) {
  }

  BlurPass(ShaderLibrary& lib)
    : pingpong_{ nullptr },
      attachment_{ 0 },
      sigma_{DEFAULT_SIGMA},
      kernel_size_{ 7 },
      technique_{ Technique::GAUSSIAN },
      blur_kernel_{ nullptr },
      RenderPass(lib) {
  }

  enum class Technique {
    BOX,
    GAUSSIAN
  };

  void SetTechnique(Technique tech) {
    technique_ = tech;
  }

  void SetKernelSize(uint32_t size) {
    if (size >= MAX_KERNEL_SIZE) {
      size = 99;
    }
    // Only support odd number kernel sizes.  If an even size kernel is input
    // increment it to be odd.
    if (size % 2 == 0) {
      ++size;
    }
    kernel_size_ = size;
  }

  void SetSigma(float sigma) {
    sigma_ = sigma;
  }

  void SetAttachment(int32_t att) {
    assert(att < MAX_INDEXABLE_ATTACHMENT);
    attachment_ = att;
  }

  template <class T, typename ...Args >
  void CreatePingPong(Args&&... args) {
    pingpong_ = std::make_unique<T>(std::forward<Args>(args)...);
  }

  void Execute(const Scene::Impl&, const Framebuffer & input, Framebuffer & output) override;
  void InitPass() override;
private:
  static constexpr int32_t MAX_INDEXABLE_ATTACHMENT = 8;

  void GenerateBlurKernel();
  void GenerateBoxKernel();
  void GenerateGaussianKernel();
  static constexpr float DEFAULT_SIGMA = 0.84089f;
  float GenerateGaussianWeight(float sigma, float distance);

  std::unique_ptr<Framebuffer> pingpong_;
  int32_t attachment_;
  float sigma_;
  uint32_t kernel_size_;
  Technique technique_;
  static constexpr uint32_t MAX_KERNEL_SIZE = 100;
  std::unique_ptr<UniformBuffer<Std140BoundedArray<float, MAX_KERNEL_SIZE>>> blur_kernel_;
};

}

#endif // OGL_BLURPASS_H_