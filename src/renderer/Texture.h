#ifndef OGL_TEXTURE_H_
#define OGL_TEXTURE_H_

#include <stdint.h>
#include <vector>
#include <memory>

#pragma warning(push, 0)  
#include <glm/glm.hpp>
#include <glm/ext.hpp>
#pragma warning(pop)
#include <GL/glew.h>
#include <stb_image.h>

namespace renderer {

struct StbDeleter {
  void operator()(uint8_t* data) {
    stbi_image_free(data);
  }
};

template <class T>
using StbImagePtr = std::unique_ptr<T, StbDeleter>;

class TextureLoadError : public std::runtime_error {
public:
  TextureLoadError(const std::string& what)
    : std::runtime_error(what) {
  }
private:
};

template <class T>
class Texture {
public:
  Texture();
  explicit Texture(uint32_t width, uint32_t height, uint32_t channels, T* data);
  ~Texture();

  Texture(const Texture&) = delete;
  Texture& operator=(const Texture&) = delete;

  void LoadTexture(uint32_t width, uint32_t height, uint32_t channels, T* data);

  uint32_t GetChannels() const {
    return channels_;
  }

  uint32_t GetWidth() const {
    return width_;
  }

  uint32_t GetHeight() const {
    return height_;
  }

  void Bind() const {
    glBindTexture(GL_TEXTURE_2D, texture_handle_);
  }

private:
  void CreateGpuObject();
  void UpdateGpuObject();
  void DeleteGpuObject();

  // Not the best place for this really.  If i need to do this elseware i'll move it.
  constexpr static GLuint GlType() {
    if (std::is_same<T, uint8_t>::value) {
      return GL_UNSIGNED_BYTE;
    }
    else if (std::is_same<T, float>::value) {
      return GL_FLOAT;
    }
    else {
      return 0;
    }
  }
  static_assert(GlType(), "GlType : Type missing from GlType function.  Function must be extended.");

  static GLuint Channels(uint32_t channels) {
    switch (channels) {
      case 1: return GL_RED;
      case 2: return GL_RG;
      case 3: return GL_RGB;
      case 4: return GL_RGBA;
    }
    assert("Texture:Channels : Unsupported channel count.");
    return 0;
  }

  GLuint texture_handle_;
  StbImagePtr<T> texture_data_;
  uint32_t channels_;
  uint32_t width_;
  uint32_t height_;
};

class TextureLoader {
public:
  static std::shared_ptr<Texture<uint8_t>> LoadFromFile(const std::string& filename, bool srgb_to_linear = false);
  static std::shared_ptr<Texture<uint8_t>> LoadXorTexture(const glm::vec3& color);
  static std::shared_ptr<Texture<uint8_t>> LoadGridTexture(const glm::vec3& color);
  static std::shared_ptr<Texture<uint8_t>> LoadUniformColorTexture(const glm::vec3& color);
  static std::shared_ptr<Texture<uint8_t>> LoadUniformValueTexture(float val);

private:
};

template <class T>
Texture<T>::Texture()
  : texture_handle_{ 0 },
    texture_data_{ nullptr },
    channels_{ 0 },
    width_{ 0 },
    height_{ 0 } {
  this->CreateGpuObject();
}

template <class T>
Texture<T>::Texture(uint32_t width, uint32_t height, uint32_t channels, T* data)
  : texture_handle_{ 0 },
    texture_data_{ data },
    channels_{ channels },
    width_{ height },
    height_{ width } {
  this->CreateGpuObject();
  this->UpdateGpuObject();
}

template <class T>
Texture<T>::~Texture() {
  this->DeleteGpuObject();
}

template <class T>
void Texture<T>::LoadTexture(uint32_t width, uint32_t height, uint32_t channels, T* data) {
  width_ = width;
  height_ = height;
  channels_ = channels;
  texture_data_.reset(data);
  this->UpdateGpuObject();
}

template <class T>
void Texture<T>::CreateGpuObject() {
  assert(texture_handle_ == 0);
  glGenTextures(1, &texture_handle_);
}

template <class T>
void Texture<T>::UpdateGpuObject() {
  glBindTexture(GL_TEXTURE_2D, texture_handle_);
  glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 8);
  glTexImage2D(GL_TEXTURE_2D,
               0,
               Channels(channels_),
               width_,
               height_,
               0,
               Channels(channels_),
               GlType(),
               texture_data_.get());
  glGenerateMipmap(GL_TEXTURE_2D);
}

template <class T>
void Texture<T>::DeleteGpuObject() {
  glBindTexture(GL_TEXTURE_2D, 0);
  glDeleteTextures(1, &texture_handle_);
  texture_handle_ = 0;
}

} //namespace renderer

#endif // CRT_TEXTURE_H_