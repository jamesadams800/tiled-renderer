#ifndef OGL_RENDERER_H_
#define OGL_RENDERER_H_

#include <stdint.h>
#include <array>
#include <memory>

#include "ShaderLibrary.h"
#include "Scene.h"

namespace renderer {

class UserInterface;
class TextUiElement;
class Mesh;

class Renderer {
public:
  explicit Renderer(uint32_t size_x, uint32_t size_y);
  virtual ~Renderer();

  void SetClearColor(float r, float g, float b);

  const std::array<float, 4>& GetClearColor() const {
    return clear_color_;
  }

  void Resize(uint32_t x, uint32_t y);
  void InitRenderer();

  virtual void Render(const Scene::Impl&) = 0;

  // UI Rendering.
  //void Render(const UserInterface&);
  //void Render(const TextUiElement&);

protected:
  virtual void Init() = 0;
  virtual void ResizeCallback() = 0;

  ShaderLibrary shader_lib_;
  std::array<float, 4> clear_color_;
  uint32_t size_x_;
  uint32_t size_y_;

private:
  Renderer(const Renderer&) = delete;
  const Renderer& operator=(const Renderer&) = delete;
};

} // namespace renderer

#endif // OGL_RENDERER_H_