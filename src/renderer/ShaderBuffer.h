#ifndef OGL_SHADERBUFFER_H_
#define OGL_SHADERBUFFER_H_

#include <set>
#include <assert.h>
#include <memory>
#include <mutex>
#include <vector>

#include <GL\glew.h>

namespace renderer {

#pragma warning (disable : 4324)
struct alignas(16) Std140ShaderObject {};

template <class T>
struct Std140ArrayElement : Std140ShaderObject {
  T element;
};

template <class T, uint32_t SIZE>
class Std140Array : Std140ShaderObject {
  Std140ArrayElement<T> data[SIZE];
public:
  T& operator[](size_t index) {
    assert(index < SIZE);
    return data[index].element;
  }

  const T& operator[](size_t index) const {
    assert(index < SIZE);
    return data[index].element;
  }

  size_t Size() const {
    return SIZE;
  }
};

template <class T, uint32_t SIZE>
struct Std140BoundedArray {
  uint32_t element_count;
  Std140Array<T, SIZE> data;
};

class BufferBase {
public:
  virtual ~BufferBase();

  GLuint GetBindPoint() const {
    return binding_point_;
  }

protected:
  BufferBase();
  GLuint binding_point_;  
private:
  class BindingPointAllocator {
  public:
    BindingPointAllocator();
    ~BindingPointAllocator();

    GLuint AllocateIndex();
    void FreeIndex(GLuint index);

  private:
    static const GLuint MAX_BINDING_POINTS = 1000;

    std::mutex set_lock_;
    std::set<GLuint> available_indices_;
  };

  static std::mutex allocator_construct_lock_;
  static std::unique_ptr<BindingPointAllocator> binding_allocator_;
};

// Storage Buffer Objects
//////////////////////////////////////////////////////////////////////////////////////
template <class T, GLenum USAGE = GL_STREAM_DRAW>
class StorageBuffer : public BufferBase {
public:
  using BaseType = typename std::conditional<std::is_array<T>::value,
                                             typename std::remove_extent<T>::type,
                                             T>::type;
  using StorageType = typename std::conditional<std::is_array<T>::value,
                                                std::vector<BaseType>,
                                                BaseType>::type;

  // ArrayOnlyFunction template can be in a way simiular to std::enable_if to enable and disable
  // functions in this class based on whether the inderlying type T is an array or not.
  template <class _T, typename R = void>
  using ArrayOnlyFunction = typename std::enable_if < std::is_array<_T>::value, R>::type;

  StorageBuffer();
  ~StorageBuffer();
  
  // Update entire object.
  void UpdateBuffer();

  // Only used in array case.  Update single or multiple adjacent index in objects.
  template <class _T = T>
  ArrayOnlyFunction<_T> UpdateBuffer(size_t index, size_t elements_to_copy = 1);

  // Only enable [] operator for dynamic array objects.
  template <class _T = T>
  ArrayOnlyFunction<_T, BaseType&> operator[](size_t indx) {
    return data_[indx];
  }
  // Otherwise it is a single struct type so use -> operator like the Uniform buffer objects.
  template <class _T = T>
  typename std::enable_if<!std::is_array<_T>::value, BaseType*>::type operator->() {
    return &data_;
  }

  template <class _T = T>
  ArrayOnlyFunction<_T> Resize(size_t size);

  size_t Size() const {
    return this->Size(std::is_array<T>());
  }

private:
  size_t Size(std::true_type) const {
    return data_.size();
  }

  size_t Size(std::false_type) const {
    return 1;
  }

  void UpdateBuffer(std::true_type);
  void UpdateBuffer(std::false_type);

  void CreateBuffer();
  void CreateBuffer(std::true_type);
  void CreateBuffer(std::false_type);
  void DeleteBuffer();

  GLuint buffer_;
  size_t buffer_element_size_;
  StorageType data_;
};

// Uniform Buffer Objects
//////////////////////////////////////////////////////////////////////////////////////
template <class T, typename... Args>
class UniformBuffer : public BufferBase {
public:
  static_assert(!std::is_array<T>::value, "Dynamic Array types are not"
                                          " supported by uniform buffer objects.");

  explicit UniformBuffer(Args&&... args);
  ~UniformBuffer();

  explicit UniformBuffer(const T& object);
  UniformBuffer& operator=(const T& object);

  void Reset() {
    std::memset(&object_, 0x0, sizeof(T));
  }

  T* operator->() {
    return &object_;
  }

  const T* operator->() const {
    return &object_;
  }

  T& operator*() {
    return object_;
  }

  const T& operator*() const {
    return object_;
  }

  void UpdateBuffer();

private:
  void CreateBuffer();
  void DeleteBuffer();

  GLuint buffer_;
  T object_;
};

// Storage Buffer Implementations.
//////////////////////////////////////////////////////////////////////////////////////
template<class T, GLenum USAGE>
inline StorageBuffer<T, USAGE>::StorageBuffer()
  : buffer_{ 0 },
    buffer_element_size_{0},
    data_{} {
  this->CreateBuffer();
}

template<class T, GLenum USAGE>
inline StorageBuffer<T, USAGE>::~StorageBuffer() {
  this->DeleteBuffer();
}

template <class T, GLenum USAGE>
template <class _T> 
inline StorageBuffer<T, USAGE>::ArrayOnlyFunction<_T>
StorageBuffer<T, USAGE>::UpdateBuffer(size_t index, size_t elements_to_copy) {
  assert(elements_to_copy > 0);
  assert((index + elements_to_copy) <= data_.size());
  glBindBuffer(GL_SHADER_STORAGE_BUFFER, buffer_);
  glBufferSubData(GL_SHADER_STORAGE_BUFFER,
                  index * sizeof(BaseType),
                  sizeof(BaseType) * elements_to_copy,
                  &data_[index]);
  glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);
}

template<class T, GLenum USAGE>
template<class _T>
inline StorageBuffer<T, USAGE>::ArrayOnlyFunction<_T> StorageBuffer<T, USAGE>::Resize(size_t size) {
  data_.resize(size);
}

template<class T, GLenum USAGE>
inline void StorageBuffer<T, USAGE>::UpdateBuffer() {
  glBindBuffer(GL_SHADER_STORAGE_BUFFER, buffer_);
  this->UpdateBuffer(std::is_array<T>());
  glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);
}

template<class T, GLenum USAGE>
inline void StorageBuffer<T, USAGE>::UpdateBuffer(std::true_type) {
  if (data_.size() != buffer_element_size_) {
    buffer_element_size_ = data_.size();
    glBufferData(GL_SHADER_STORAGE_BUFFER,
                 sizeof(BaseType) * data_.size(),
                 data_.data(), 
                 USAGE);
  }
  else {
    GLvoid* buffer = glMapBuffer(GL_SHADER_STORAGE_BUFFER, GL_WRITE_ONLY);
    std::memcpy(buffer, data_.data(), sizeof(BaseType) * data_.size());
    glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);
  }
}

template<class T, GLenum USAGE>
inline void StorageBuffer<T, USAGE>::UpdateBuffer(std::false_type) {
  GLvoid* buffer = glMapBuffer(GL_SHADER_STORAGE_BUFFER, GL_WRITE_ONLY);
  std::memcpy(buffer, &data_, sizeof(BaseType));
  glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);
}

template<class T, GLenum USAGE>
inline void StorageBuffer<T, USAGE>::CreateBuffer() {
  glGenBuffers(1, &buffer_);
  glBindBuffer(GL_SHADER_STORAGE_BUFFER, buffer_);
  glBindBufferBase(GL_SHADER_STORAGE_BUFFER, binding_point_, buffer_);
  this->CreateBuffer(std::is_array<T>());
  glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);
}

// Is array case.
template<class T, GLenum USAGE>
inline void StorageBuffer<T, USAGE>::CreateBuffer(std::true_type) {
  buffer_element_size_ = data_.size();
  glBufferData(GL_SHADER_STORAGE_BUFFER, sizeof(BaseType) * data_.size(), data_.data(), USAGE);
}

template<class T, GLenum USAGE>
inline void StorageBuffer<T, USAGE>::CreateBuffer(std::false_type) {
  buffer_element_size_ = 1;
  glBufferData(GL_SHADER_STORAGE_BUFFER, sizeof(BaseType), &data_, USAGE);
}

template<class T, GLenum USAGE>
inline void StorageBuffer<T, USAGE>::DeleteBuffer() {
  glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);
  glDeleteBuffers(1, &buffer_);
}


// Uniform Buffer Implementations.
//////////////////////////////////////////////////////////////////////////////////////
template <class T, typename... Args>
inline UniformBuffer<T, Args...>::UniformBuffer(Args&&... args)
  : buffer_{ 0 },
    object_{ std::forward<Args>(args)... },
    BufferBase() {
  this->CreateBuffer();
}

template <class T, typename... Args>
inline UniformBuffer<T, Args...>::~UniformBuffer() {
  this->DeleteBuffer();
}

template <class T, typename... Args>
inline UniformBuffer<T, Args...>::UniformBuffer(const T& object)
  : object_{ object } {
  this->CreateBuffer();
}

template <class T, typename... Args>
inline UniformBuffer<T, Args...>& UniformBuffer<T, Args...>::operator=(const T& object) {
  object_ = object;
  this->UpdateBuffer();
  return *this;
}

template <class T, typename... Args>
inline void UniformBuffer<T, Args...>::UpdateBuffer() {
  glBindBuffer(GL_UNIFORM_BUFFER, buffer_);
  GLvoid* buffer = glMapBuffer(GL_UNIFORM_BUFFER, GL_WRITE_ONLY);
  std::memcpy(buffer, &object_, sizeof(T));
  glUnmapBuffer(GL_UNIFORM_BUFFER);
  glBindBuffer(GL_UNIFORM_BUFFER, 0);
}

template <class T, typename... Args>
inline void UniformBuffer<T, Args...>::CreateBuffer() {
  glGenBuffers(1, &buffer_);
  glBindBuffer(GL_UNIFORM_BUFFER, buffer_);
  glBindBufferBase(GL_UNIFORM_BUFFER, binding_point_, buffer_);
  glBufferData(GL_UNIFORM_BUFFER, sizeof(T), &object_, GL_STREAM_READ);
  glBindBuffer(GL_UNIFORM_BUFFER, 0);
}

template <class T, typename... Args>
inline void UniformBuffer<T, Args...>::DeleteBuffer() {
  glBindBuffer(GL_UNIFORM_BUFFER, 0);
  glDeleteBuffers(1, &buffer_);
}

} // namespace renderer

#endif // OGL_SHADERBUFFER_H_