#include "Framebuffer.h"

#include <assert.h>
#include <string>

namespace renderer {

Framebuffer::Framebuffer(uint32_t x, uint32_t y)
    : framebuffer_{0},
      size_x_{x},
      size_y_{y},
      depth_texture_{0},
      has_depth_buffer_{false},
      has_stencil_buffer_{false} {
  glGenFramebuffers(1, &framebuffer_);
}

Framebuffer::~Framebuffer() {
  glBindFramebuffer(GL_FRAMEBUFFER, 0);
  if (depth_texture_) {
    glDeleteTextures(1, &depth_texture_);
  }
  glDeleteFramebuffers(1, &framebuffer_);
  framebuffer_ = 0;
}

GLint Framebuffer::GetInternalFormat() {
  if (has_depth_buffer_ && has_stencil_buffer_) {
    return GL_DEPTH24_STENCIL8;
  } 
  else if (has_depth_buffer_) {
    return GL_DEPTH_COMPONENT32;
  }
  else {
    // https://www.khronos.org/opengl/wiki/Framebuffer_Object_Extension_Examples#Stencil
    // Stencil NEVER EVER MAKE A STENCIL buffer.All GPUs and all drivers do not support an
    // independent stencil buffer.If you need a stencil buffer, then you need to make a 
    // Depth = 24, Stencil = 8 buffer, also called D24S8.Please search for the example 
    // about GL_EXT_packed_depth_stencil on this page.
    throw std::runtime_error("Framebuffer : Attempting to construct invalid"
                           "stencil buffer with no depth.");
  }
}

GLenum Framebuffer::GetFormat() {
  if (has_depth_buffer_ && has_stencil_buffer_) {
    return GL_DEPTH_STENCIL;
  }
  else if (has_depth_buffer_) {
    return GL_DEPTH_COMPONENT;
  }
  else {
    // See GetInternalFormat comment for details...
    throw std::runtime_error("Framebuffer : Attempting to construct invalid"
      "stencil buffer with no depth.");
  }
}

GLenum Framebuffer::GetType() {
  if (has_depth_buffer_ && has_stencil_buffer_) {
    return GL_UNSIGNED_INT_24_8;
  }
  else if (has_depth_buffer_) {
    return GL_FLOAT;
  }
  else {
    // See GetInternalFormat comment for details...
    throw std::runtime_error("Framebuffer : Attempting to construct invalid"
      "stencil buffer with no depth.");
  }
}

void Framebuffer::BuildDepthStencil() {
  glBindFramebuffer(GL_FRAMEBUFFER, framebuffer_);
  if (!depth_texture_) {
    glGenTextures(1, &depth_texture_);
  }
  glBindTexture(GL_TEXTURE_2D, depth_texture_);
  glTexImage2D(GL_TEXTURE_2D,
               0,
               this->GetInternalFormat(),
               size_x_,
               size_y_,
               0,
               this->GetFormat(),
               this->GetType(),
               nullptr);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  if (has_depth_buffer_) {
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depth_texture_, 0);
  }
  if (has_stencil_buffer_) {
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_STENCIL_ATTACHMENT, GL_TEXTURE_2D, depth_texture_, 0);
  }
  GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
  if (status != GL_FRAMEBUFFER_COMPLETE) {
    throw std::runtime_error(
      "Framebuffer::BuildDepthStencil : failed to resize framebuffer. Status [" +
      std::to_string(status) + "]");
  }
  glBindTexture(GL_TEXTURE_2D, 0);
  glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void Framebuffer::AddDepthBuffer() {
  has_depth_buffer_ = true;
  this->BuildDepthStencil();
}

void Framebuffer::AddStencilBuffer() {
  has_stencil_buffer_ = true;
  this->BuildDepthStencil();
}

void Framebuffer::Resize(uint32_t x, uint32_t y) {
  size_x_ = x;
  size_y_ = y;
  if (depth_texture_) {
    glBindTexture(GL_TEXTURE_2D, depth_texture_);
    glTexImage2D(GL_TEXTURE_2D, 
                 0,
                 this->GetInternalFormat(),
                 size_x_,
                 size_y_,
                 0,
                 this->GetFormat(),
                 this->GetType(),
                 nullptr);
  }
  this->ResizeTarget(x, y);
  GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
  if (status != GL_FRAMEBUFFER_COMPLETE) {
    throw std::runtime_error(
        "Framebuffer::Resize : failed to resize framebuffer. Status [" +
        std::to_string(status) + "]");
  }
}

}  // namespace renderer
