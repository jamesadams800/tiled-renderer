#ifndef OGL_TILEDDEFERREDRENDERER_H_
#define OGL_TILEDDEFERREDRENDERER_H_

#include "Renderer.h"
#include "GBuffer.h"
#include "TextureFramebuffer.h"

#include "ShadowPass.h"
#include "DeferredGeometryPass.h"
#include "TiledDeferredLightingPass.h"
#include "BloomPass.h"
#include "UiRenderPass.h"
#include "TonemapPass.h"
#include "FxaaPass.h"
#include "ForwardPass.h"

namespace renderer {

class TiledDeferredRenderer : public Renderer {
public:
  explicit TiledDeferredRenderer(uint32_t size_x, uint32_t size_y);
  ~TiledDeferredRenderer();

  void Init() override;
  void Render(const Scene::Impl& scene) override;

private:
  void ResizeCallback() override;

  ShadowPass shadow_pass_;
  DeferredGeometryPass geometry_pass_;
  TiledDeferredLightingPass lighting_pass_;
  ForwardPass forward_pass_;
  BloomPass bloom_pass_;
  TonemapPass tone_map_pass_;
  FxaaPass fxaa_pass_;
  UiRenderPass ui_pass_;

  std::unique_ptr<DefaultFramebuffer> default_framebuffer_;
  std::unique_ptr<GBuffer> gbuffer_;
  std::unique_ptr<HDRFramebuffer> hdr_buffer_;
  std::unique_ptr<HDRFramebuffer> aa_buffer_;
};

} // namespace renderer

#endif // OGL_TILEDDEFERREDRENDERER_H_