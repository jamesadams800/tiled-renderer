#ifndef OGL_SHADER_H_
#define OGL_SHADER_H_

#include <string>
#include <array>
#include <map>
#include <mutex>

#include <GL\glew.h>
#pragma warning(push, 0)  
#include <glm/glm.hpp>
#include <glm/ext.hpp>
#pragma warning(pop)

#include "ShaderBuffer.h"

namespace renderer {

class Shader {
public:
  enum ShaderType {
    VERTEX_SHADER = 0,
    TESSELATION_CONTROL_SHADER,
    TESSELATION_EVAL_SHADER,
    GEOMETRY_SHADER,
    FRAGMENT_SHADER,
    COMPUTE_SHADER,
    MAX_SHADER_INDEX,
    UNKNOWN_SHADER_TYPE
  };

  static constexpr const char* VERTEX_SHADER_EXTENSION = ".vert";
  static constexpr const char* TESS_CONTROL_SHADER_EXTENSION = ".tesc";
  static constexpr const char* TESS_EVAL_SHADER_EXTENSION = ".tese";
  static constexpr const char* GEOMETRY_SHADER_EXTENSION = ".geom";
  static constexpr const char* FRAGMENT_SHADER_EXTENSION = ".frag";
  static constexpr const char* COMPUTE_SHADER_EXTENSION = ".comp";

  static inline ShaderType TypeFromExtension(const std::string& name) {
    if (name == VERTEX_SHADER_EXTENSION) {
      return VERTEX_SHADER;
    }
    else if (name == TESS_CONTROL_SHADER_EXTENSION) {
      return TESSELATION_CONTROL_SHADER;
    }
    else if (name == TESS_EVAL_SHADER_EXTENSION) {
      return TESSELATION_EVAL_SHADER;
    }
    else if (name == GEOMETRY_SHADER_EXTENSION) {
      return GEOMETRY_SHADER;
    }
    else if (name == FRAGMENT_SHADER_EXTENSION) {
      return FRAGMENT_SHADER;
    }
    else if (name == COMPUTE_SHADER_EXTENSION) {
      return COMPUTE_SHADER;
    }
    else {
      return UNKNOWN_SHADER_TYPE;
    }
  }

  static inline std::string ExtensionFromType(ShaderType type) {
    switch (type) {
      case VERTEX_SHADER: return VERTEX_SHADER_EXTENSION;
      case TESSELATION_CONTROL_SHADER: return TESS_CONTROL_SHADER_EXTENSION;
      case TESSELATION_EVAL_SHADER: return TESS_EVAL_SHADER_EXTENSION;
      case GEOMETRY_SHADER: return GEOMETRY_SHADER_EXTENSION;
      case FRAGMENT_SHADER: return FRAGMENT_SHADER_EXTENSION;
      case COMPUTE_SHADER: return COMPUTE_SHADER_EXTENSION;
      default: return "";
    }
  }

  Shader();
  ~Shader();

  Shader(const Shader&) = delete;
  Shader(const Shader&&) = delete;

  Shader& operator=(const Shader&) = delete;
  Shader& operator=(const Shader&&) = delete;

  glm::uvec2 GetRequiredWorkgroupSize(uint32_t x, uint32_t y) const {
    assert(shader_handles_[COMPUTE_SHADER] != 0);
    glm::uvec2 ret(0);
    ret.x = (x + work_group_size_.x - 1) / work_group_size_.x;
    ret.y = (y + work_group_size_.y - 1) / work_group_size_.y;
    return ret;
  }

  void DispatchComputeBlocking(uint32_t x, uint32_t y, uint32_t z) const {
    this->DispatchCompute(x, y, z);
    glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT | GL_SHADER_IMAGE_ACCESS_BARRIER_BIT);
  }

  void DispatchCompute(uint32_t x, uint32_t y, uint32_t z) const {
    assert(shader_handles_[COMPUTE_SHADER] != 0);
    glDispatchCompute(x, y, z);
  }

  void ReloadSource();
  void DestroyShader();
  void AddSource(ShaderType, const std::string& source);

  void BuildShader();

  void Attach() const {
    glUseProgram(shader_program_);
  }

  void Detach() const {
    glUseProgram(0);
  }

  template <class T>
  void BindUniform(const std::string& uniform_name, const UniformBuffer<T>& uniform) const {
    GLuint block_index = glGetUniformBlockIndex(shader_program_, uniform_name.c_str());
    glUniformBlockBinding(shader_program_, block_index, uniform.GetBindPoint());
  }

  template <class T, GLenum USAGE>
  void BindStorage(const std::string& resource_name, const StorageBuffer<T, USAGE>& buffer) const {
    GLuint block_index = glGetProgramResourceIndex(shader_program_,
                                                   GL_SHADER_STORAGE_BLOCK,
                                                   resource_name.c_str());
    glShaderStorageBlockBinding(shader_program_, block_index, buffer.GetBindPoint());
  }

  void BindUniform(const std::string& uniform_name, int32_t uniform) const;
  void BindUniform(const std::string& uniform_name, float uniform) const;
  void BindUniform(const std::string& uniform_name, const glm::vec2& uniform) const;
  void BindUniform(const std::string& uniform_name, const glm::vec3& uniform) const;
  void BindUniform(const std::string& uniform_name, const glm::vec4& uniform) const;
  void BindUniform(const std::string& uniform_name, const glm::mat4& uniform) const;

private:
  void BuildShaderStage(ShaderType);
  void LinkShaderProgram();
  GLint CompileShader(GLenum shader_type, const std::string& source_file, GLuint& shader_handle);
  void LoadSource(const std::string& source_file, ShaderType);

  std::mutex source_mutex_;
  std::array<std::string, MAX_SHADER_INDEX> source_files_;
  std::array<std::string, MAX_SHADER_INDEX> source_cache_;
  std::array<GLuint, MAX_SHADER_INDEX> shader_handles_;
  GLuint shader_program_;
  glm::ivec3 work_group_size_;
};

} // namespace renderer

#endif // OGL_SHADER_H_