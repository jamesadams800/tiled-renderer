#ifndef OGL_CASCADEDSHADOWMAP_H_
#define OGL_CASCADEDSHADOWMAP_H_

#include <array>

#include "DirectionalShadowMap.h"
#include "Camera.h"
#include "AABB.h"

namespace renderer {

class Scene;
class ShadowPass;

class CascadedShadowMap : public DirectionalShadowMap {
public:
  static constexpr uint32_t MAX_CASCADES = 4;
  struct ShaderData;

  explicit CascadedShadowMap(const Camera&);
  explicit CascadedShadowMap(const Camera&, const glm::vec3& light_direction);
  ~CascadedShadowMap();

  void BindViewports() const {
    for (uint32_t i = 0; i < MAX_CASCADES; i++) {
      glViewportIndexedf(i,
                         static_cast<float>(i * SHADOW_MAP_X_RESOLUTION),
                         0.0f,
                         static_cast<float>(SHADOW_MAP_X_RESOLUTION),
                         static_cast<float>(SHADOW_MAP_Y_RESOLUTION));
    }
  }

  uint32_t GetNoCascades() const {
    return MAX_CASCADES;
  }

  uint32_t GetCascadeMapSizeX() const {
    return cascade_x_;
  }

  uint32_t GetCascadeMapSizeY() const {
    return cascade_y_;
  }

  void UpdateLightDirection(const glm::vec3& direction) override {
    light_direction_ = direction;
  }
  void CalculateCascadeMatrices();

  const std::array<glm::mat4, MAX_CASCADES>&  GetViewProjUniform() const {
    return cascade_view_projections_;
  }

  const std::array<float, MAX_CASCADES>& GetViewFarPlanesUniform() const {
    return view_frustrum_far_planes_;
  }

  void WriteShaderData(ShaderData&) const;

private:
  const Camera& view_camera_;
  float near_z_;
  float far_z_;
  uint32_t cascade_x_;
  uint32_t cascade_y_;

  std::array<glm::mat4, MAX_CASCADES> cascade_view_projections_;
  std::array<float, MAX_CASCADES> view_frustrum_far_planes_;
  static std::array<float, MAX_CASCADES> kCascadeFarPlanes_;
};

} // namespace renderer

#endif // 
