#ifndef OGL_RENDERPASS_H_
#define OGL_RENDERPASS_H_

#include <stdint.h>
#include <array>
#include <vector>
#include <memory>

#include "Scene.h"

namespace renderer {

class ShaderLibrary;
class Framebuffer;
class Mesh;

class RenderPass {
public:
  RenderPass(ShaderLibrary& lib) 
    : shader_lib_{ lib } {
  }
  virtual ~RenderPass() {}

  void Init();
  virtual void Execute(const Scene::Impl&, const Framebuffer& input, Framebuffer & output) = 0;

protected:
  void DrawFullScreenQuad() const;

  virtual void InitPass() = 0;
  ShaderLibrary& shader_lib_;

private:
  static std::vector<float> kFsqPositions_;
  static std::vector<float> kFsqTexCoords_;
  static std::unique_ptr<Mesh> kFsq_;
};

} // namespace renderer

#endif // OGL_RENDERPASS_H_