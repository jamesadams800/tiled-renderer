#include "TiledDeferredLightingPass.h"

#include "SceneImpl.h"
#include "GBuffer.h"
#include "ShaderLibrary.h"

namespace renderer {

void TiledDeferredLightingPass::InitPass() {
  tile_data_ = std::make_unique< StorageBuffer<TileData[], GL_STATIC_COPY>>();
}

void TiledDeferredLightingPass::ResizeStorage(uint32_t work_group_x, uint32_t work_group_y) {
  tile_data_->Resize(work_group_x * work_group_y);
  tile_data_->UpdateBuffer();
}

void TiledDeferredLightingPass::Execute(const Scene::Impl& scene, 
                                        const Framebuffer & input, 
                                        Framebuffer & output) {
  output.BindWrite();
  if (&input != &output) {
    input.BindRead();
    glViewport(0, 0, output.GetX(), output.GetY());
    glBlitFramebuffer(0, 0, input.GetX(), input.GetY(),
                      0, 0, output.GetX(), output.GetY(),
                      GL_DEPTH_BUFFER_BIT, GL_NEAREST);
  }
  const Camera& camera = *scene.GetCamera();
  glm::vec4 camera_position = camera.GetPosition();
  // Prepare Shaders and relevant framebuffers.
  /////////////////////////////////////////////////////////////////////////////////////////////////
  const Shader* tiled_bounds_shader = shader_lib_.GetShader("TiledFrustumConstruction");
  const Shader* tiled_shader = shader_lib_.GetShader("TiledLighting");
  assert(tiled_bounds_shader && tiled_shader);
  glm::uvec2 work_group_size = tiled_shader->GetRequiredWorkgroupSize(output.GetX(), output.GetY());
  if (current_work_group_size_x_ != work_group_size.x ||
      current_work_group_size_y_ != work_group_size.y) {
    current_work_group_size_x_ = work_group_size.x;
    current_work_group_size_y_ = work_group_size.y;
    this->ResizeStorage(work_group_size.x, work_group_size.y);
  }

  // Bind Textures.
  /////////////////////////////////////////////////////////////////////////////////////////////////
  uint32_t texture_index = 0;
  texture_index += input.BindRead();
  texture_index += input.BindReadDepthBuffer(texture_index);
  for (auto& directional_light : scene.GetDirectionalLights()) {
    if (auto* shadow_map = directional_light->GetShadowMap()) {
      shadow_map->BindReadDepthBuffer(texture_index);
    }
    texture_index++;
  }

  // Pass 1. Do parrelel reduction to calculate tiles aabb, frustrum and 2.5d depth masks.
  /////////////////////////////////////////////////////////////////////////////////////////////////
  tiled_bounds_shader->Attach();
  tiled_bounds_shader->BindStorage("TileDataBuffer", *tile_data_);
  tiled_bounds_shader->BindUniform("world_camera_position", camera_position);
  tiled_bounds_shader->BindUniform("camera_view_matrix", camera.GetViewMatrix());
  tiled_bounds_shader->BindUniform("inv_camera_view_matrix", camera.GetInvViewMatrix());
  tiled_bounds_shader->BindUniform("inv_camera_proj_matrix", camera.GetInvProjectionMatrix());
  tiled_bounds_shader->DispatchComputeBlocking(work_group_size.x, work_group_size.y, 1);

  // Pass 2. Perform light culling and lighting calculations.
  /////////////////////////////////////////////////////////////////////////////////////////////////
  output.BindWrite();
  output.BindWriteCompute(0);
  tiled_shader->Attach();
  tiled_shader->BindUniform("world_camera_position", camera_position);
  tiled_shader->BindUniform("camera_view_matrix", camera.GetViewMatrix());
  tiled_shader->BindUniform("inv_camera_view_matrix", camera.GetInvViewMatrix());
  tiled_shader->BindUniform("inv_camera_proj_matrix", camera.GetInvProjectionMatrix());
  tiled_shader->BindStorage("TileDataBuffer", *tile_data_);
  tiled_shader->BindStorage("PointLights", scene.GetPointLightBuffer());
  tiled_shader->BindStorage("DirectionalLights", scene.GetDirectLightBuffer());
  tiled_shader->BindUniform("CascadeShadowData", scene.GetCascadeData());

  tiled_shader->DispatchComputeBlocking(work_group_size.x, work_group_size.y, 1);
}

} // namespace renderer
