#ifndef OGL_UIRENDERPASS_H_
#define OGL_UIRENDERPASS_H_

#pragma warning(push, 0)  
#include <glm/glm.hpp>
#include <glm/ext.hpp>
#pragma warning(pop)

#include "RenderPass.h"

namespace renderer {

class UserInterface;
class TextUiElement;

class UiRenderPass : public RenderPass {
public:
  UiRenderPass(ShaderLibrary& lib)
    : projection_matrix_{ 1 },
      RenderPass {lib} {
  }

  void Execute(const Scene::Impl&, const Framebuffer& input, Framebuffer & output) override;
  void Render(const TextUiElement & element);

protected:
  void InitPass() override;
private:
  glm::mat4 projection_matrix_;
};

} // namespace renderer

#endif // OGL_UIRENDERPASS_H_