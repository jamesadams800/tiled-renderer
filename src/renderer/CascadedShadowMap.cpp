#include "CascadedShadowMap.h"

#include "Scene.h"
#include "ShaderData.h"
#include "ShadowPass.h"

namespace renderer {

std::array<float, CascadedShadowMap::MAX_CASCADES> CascadedShadowMap::kCascadeFarPlanes_ = {
  { 0.1f,
    0.3f,
    0.6f,
    1.0f }
};

CascadedShadowMap::CascadedShadowMap(const Camera& camera)
  : CascadedShadowMap{ camera, glm::vec3(0.0f, -1.0f, 0.0f) } {
}

CascadedShadowMap::CascadedShadowMap(const Camera& camera,
                                     const glm::vec3 & light_direction)
  : DirectionalShadowMap(SHADOW_MAP_X_RESOLUTION * MAX_CASCADES,
                         SHADOW_MAP_Y_RESOLUTION,
                         light_direction),
  view_camera_{ camera },
  cascade_view_projections_{},
  cascade_x_{ SHADOW_MAP_X_RESOLUTION },
  cascade_y_{ SHADOW_MAP_Y_RESOLUTION } {
}

CascadedShadowMap::~CascadedShadowMap() {
}

void CascadedShadowMap::CalculateCascadeMatrices() {
  float near = view_camera_.GetNearPlane();
  for (uint32_t i = 0; i < MAX_CASCADES; i++) {
    float far = view_camera_.GetFarPlane() * kCascadeFarPlanes_[i];
    view_frustrum_far_planes_[i] = far;
    std::array<glm::vec4, 8> frustrum_corners = view_camera_.GetWorldSpaceSplitFrustrum(-near, -far);

    glm::vec4 frustrum_center(0.0f);
    for (auto& corner : frustrum_corners) {
      frustrum_center += corner;
    }
    frustrum_center /= 8;
    
    float radius = std::numeric_limits<float>::lowest();
    for (auto& corner : frustrum_corners) {
      radius = glm::max(radius, glm::length(corner - frustrum_center));
    }
    radius = glm::ceil(radius);
    glm::mat4 light_view = glm::lookAt(glm::vec3(frustrum_center),
                                       glm::vec3(frustrum_center) + light_direction_,
                                       glm::vec3(0.0f, 1.0f, 0.0f));
    glm::vec3 light_frustrum_center = light_view * frustrum_center;
    glm::vec3 min_bounds = glm::vec3(light_frustrum_center) - glm::vec3(radius);
    glm::vec3 max_bounds = glm::vec3(light_frustrum_center) + glm::vec3(radius);

    glm::mat4 cascade_projection = glm::ortho(min_bounds.x,
                                              max_bounds.x,
                                              min_bounds.y,
                                              max_bounds.y,
                                              min_bounds.z,
                                              max_bounds.z);
    cascade_view_projections_[i] = cascade_projection * light_view;

    // Modify the cascades projection view matrix in order to round movement to the nearest
    // texel.  This should prevent shadow "shimmering" by preventing shadow map by moving by
    // fractions of a texel.
    glm::vec4 origin_light_space = cascade_view_projections_[i] * glm::vec4(0.0f, 0.0f, 0.0f, 1.0f);
    origin_light_space.x = origin_light_space.x * static_cast<float>(cascade_x_) / 2.0f;
    origin_light_space.y = origin_light_space.y * static_cast<float>(cascade_y_) / 2.0f;

    glm::vec4 rounded_offset = glm::round(origin_light_space) - origin_light_space;
    rounded_offset.x = rounded_offset.x * 2.0f / static_cast<float>(cascade_x_);
    rounded_offset.y = rounded_offset.y * 2.0f / static_cast<float>(cascade_y_);
    rounded_offset.z = 0;
    rounded_offset.w = 0;
    // Apply offset to cascade view projection
    cascade_projection[3] += rounded_offset;
    cascade_view_projections_[i] = cascade_projection * light_view;

    near = far;
  }
}

void CascadedShadowMap::WriteShaderData(ShaderData & shader_data) const {
  shader_data.cascade_map_dimensions.x = static_cast<float>(cascade_x_);
  shader_data.cascade_map_dimensions.y = static_cast<float>(cascade_y_);
  for (uint32_t i = 0; i < MAX_CASCADES; i++) {
    shader_data.camera_far_planes[i] = view_frustrum_far_planes_[i];
    shader_data.cascade_view_projection[i] = cascade_view_projections_[i];
  }
}

} // namespace renderer