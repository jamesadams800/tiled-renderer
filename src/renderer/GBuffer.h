#ifndef OGL_GBUFFER_H_
#define OGL_GBUFFER_H_

#include <stdint.h>
#include <map>
#include <vector>

#include "Framebuffer.h"

namespace renderer {

class GBuffer : public Framebuffer {
public:
  enum Component : uint32_t {
    POSITION = 0,
    NORMAL,
    ALBEDO,
    SPECULAR,
    MAX_INDEX
  };

  explicit GBuffer(uint32_t x, uint32_t y);
  ~GBuffer();

  void AddComponent(Component,
                    GLuint format = GL_RGB,
                    GLuint type = GL_FLOAT,
                    GLuint internal_format = GL_RGB16F);

  uint32_t BindRead(uint32_t texture_unit) const override {
    this->Framebuffer::BindRead();
    this->BindTextures(texture_unit);
    return static_cast<uint32_t>(textures_.size());
  }

private:
  void BindTextures(uint32_t) const;
  void DestroyTextures();
  void ResizeTarget(uint32_t x, uint32_t y) override;

  struct ComponentDescriptor {
    GLuint format;
    GLuint type;
    GLuint internal_format;
  };

  std::vector<GLuint> attachments_;
  std::map<Component, ComponentDescriptor> component_description_;
  std::map<Component, GLuint> textures_;
  static std::map<Component, std::string> kComponentStrings_;
};

} // namespace renderer

#endif // OGL_GBUFFER_H_