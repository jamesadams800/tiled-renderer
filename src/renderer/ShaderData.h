#ifndef OGL_SHADERDATA_H_
#define OGL_SHADERDATA_H_

/**************************************************************************************************
 * Definitions for ShaderData structs declared as subclasses that are used to pass data to shaders 
 * are defined here. E.g. DirectionalLight::ShaderData.
 **************************************************************************************************/

#include "ShaderBuffer.h"
#include "DirectionalLight.h"
#include "PointLight.h"
#include "CascadedShadowMap.h"

namespace renderer {

struct DirectionalLight::ShaderData : Std140ShaderObject {
  glm::vec4 direction;
  glm::vec4 color;
  bool has_shadow;
};

struct PointLight::ShaderData : Std140ShaderObject {
  glm::mat4 sphere_model;
  glm::vec4 color;
  glm::vec4 position;
  float radius;
  float linear_attenuation;
  float quadratic_attenuation;
};

struct CascadedShadowMap::ShaderData : Std140ShaderObject {
  Std140Array<float, MAX_CASCADES> camera_far_planes;
  Std140Array<glm::mat4, MAX_CASCADES> cascade_view_projection;
  glm::vec2 cascade_map_dimensions;
};

} // namespace renderer

#endif // OGL_SHADERDATA_H_