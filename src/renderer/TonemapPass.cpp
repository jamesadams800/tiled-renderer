#include "TonemapPass.h"

#include <GL/glew.h>

#include "ShaderLibrary.h"
#include "Framebuffer.h"

namespace renderer {

void TonemapPass::InitPass() {
}

void TonemapPass::Execute(const Scene::Impl&, const Framebuffer& input, Framebuffer& output) {
  input.BindRead();
  output.BindWrite();
  glClear(GL_COLOR_BUFFER_BIT);
  glViewport(0, 0, output.GetX(), output.GetY());
  std::string shader_name;

  if (technique_ == Technique::REINHARD) {
    shader_name = "ReinhardTonemap";
  }
  else {
    shader_name = "FilmicTonemap";
  }
  auto tone_map_shader = shader_lib_.GetShader(shader_name);
  assert(tone_map_shader);

  tone_map_shader->Attach();
  tone_map_shader->BindUniform("gamma", gamma_);
  this->DrawFullScreenQuad();
}

} // namespace renderer
