#ifndef OGL_SHADOWPASS_H_
#define OGL_SHADOWPASS_H_

#include "RenderPass.h"
#include "BlurPass.h"

namespace renderer {

class UserInterface;
class TextUiElement;
class Scene;
class DirectionalShadowMap;
class CascadedShadowMap;

class ShadowPass : public RenderPass {
public:
  ShadowPass(ShaderLibrary& lib) 
    : RenderPass {lib} {
  }

  void Execute(const Scene::Impl&, const Framebuffer& input, Framebuffer& output) override;

  void Render(const Scene::Impl&, int32_t map_index, CascadedShadowMap&) const;
protected:
  void InitPass() override;
private:
};

}  // namespace renderer

#endif  // OGL_SHADOWPASS_H_