#include "Texture.h"

#include <string>

#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>
#define STB_IMAGE_RESIZE_IMPLEMENTATION
#include <stb_image_resize.h>

namespace renderer {

std::shared_ptr<Texture<uint8_t>> TextureLoader::LoadFromFile(const std::string & filename,
                                                              bool srgb_to_linear) {
  std::shared_ptr<Texture<uint8_t>> texture = std::make_shared<Texture<uint8_t>>();
  StbImagePtr<uint8_t> data;

  int32_t width;
  int32_t height;
  int32_t channels;
  stbi_set_flip_vertically_on_load(true);
  data.reset(stbi_load(filename.c_str(), &width, &height, &channels, 0));

  if (srgb_to_linear) {
    stbir_resize_uint8_srgb(data.get(), width, height, 0,
                            data.get(), width, height, 0,
                            channels, 0, 0);
  }

  texture->LoadTexture(width, height, channels, data.release());

  return texture;
}

std::shared_ptr<Texture<uint8_t>> TextureLoader::LoadXorTexture(const glm::vec3& color) {
  std::shared_ptr<Texture<uint8_t>> texture = std::make_shared<Texture<uint8_t>>();
  std::unique_ptr<uint8_t[]> xor_data( new uint8_t[512 * 512 * 3] );
  for (uint32_t i = 0; i < 512; i++) {
    for (uint32_t j = 0; j < 512; j++) {
      size_t index = 3 * ((512 * i) + j);
      xor_data[index] = uint8_t((color.r * float(i ^ j) / 512.0f) * 255);
      xor_data[index + 1] = uint8_t((color.g * float(i ^ j) / 512.0f) * 255);
      xor_data[index + 2] = uint8_t((color.b * float(i ^ j) / 512.0f) * 255);
    }
  }
  texture->LoadTexture(512, 512, 3, xor_data.release());
  return texture;
}

std::shared_ptr<Texture<uint8_t>> TextureLoader::LoadGridTexture(const glm::vec3 & color) {
  std::shared_ptr<Texture<uint8_t>> texture = std::make_shared<Texture<uint8_t>>();
  std::unique_ptr<uint8_t[]> grid_data(new uint8_t[512 * 512 * 3]);
  
  for (uint32_t i = 0; i < 512; i++) {
    for (uint32_t j = 0; j < 512; j++) {
      size_t index = 3 * ((512 * i) + j);
      if (i % 16 == 0|| j % 16 == 0 ) {
        grid_data[index] = 128;
        grid_data[index + 1] = 128;
        grid_data[index + 2] = 128;
      }
      else {
        grid_data[index] = uint8_t(color.r * 255);
        grid_data[index + 1] = uint8_t(color.g * 255);
        grid_data[index + 2] = uint8_t(color.b * 255);
      }
    }
  }
  texture->LoadTexture(512, 512, 3, grid_data.release());
  return texture;
}

std::shared_ptr<Texture<uint8_t>> TextureLoader::LoadUniformColorTexture(const glm::vec3 & color) {
  std::shared_ptr<Texture<uint8_t>> texture = std::make_shared<Texture<uint8_t>>();
  std::unique_ptr<uint8_t[]> data(new uint8_t[16 * 16 * 3]);
  for (uint32_t i = 0; i < 16 * 16; i++) {
    data[3 * i] = uint8_t(color.r * 255);
    data[3 * i + 1] = uint8_t(color.g * 255);
    data[3 * i + 2] = uint8_t(color.b * 255);
  }
  texture->LoadTexture(16, 16, 3, data.release());
  return texture;
}

std::shared_ptr<Texture<uint8_t>> TextureLoader::LoadUniformValueTexture(float val) {
  std::shared_ptr<Texture<uint8_t>> texture = std::make_shared<Texture<uint8_t>>();
  std::unique_ptr<uint8_t[]> data(new uint8_t[16 * 16 * 3]);
  for (uint32_t i = 0; i < 16 * 16; i++) {
    data[i] = uint8_t(val * 255);
  }
  texture->LoadTexture(16, 16, 1, data.release());
  return texture;
}

} // namespace raytracer