#ifndef OGL_FRAMEBUFFER_H_
#define OGL_FRAMEBUFFER_H_

#include <stdint.h>

#include <GL/glew.h>

namespace renderer {

class Framebuffer {
public:
  explicit Framebuffer(uint32_t x, uint32_t y);
  ~Framebuffer();

  // Returns consumed texture units.
  virtual uint32_t BindRead(uint32_t = 0) const {
    glBindFramebuffer(GL_READ_FRAMEBUFFER, framebuffer_);
    return 0;
  }

  uint32_t BindReadDepthBuffer(uint32_t index) const {
    glActiveTexture(GL_TEXTURE0 + index);
    glBindTexture(GL_TEXTURE_2D, depth_texture_);
    return 1;
  }

  virtual void BindWriteCompute(uint32_t) {};
  virtual void BindWrite() {
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, framebuffer_);
  }

  void Resize(uint32_t x, uint32_t y);

  uint32_t GetX() const {
    return size_x_;
  }

  uint32_t GetY() const {
    return size_y_;
  }

  void AddDepthBuffer();
  void AddStencilBuffer();

protected:
  virtual void ResizeTarget(uint32_t x, uint32_t y) = 0;

  GLuint framebuffer_;
  uint32_t size_x_;
  uint32_t size_y_;
  GLuint depth_texture_;
private:
  void BuildDepthStencil();
  GLint GetInternalFormat();
  GLenum GetFormat();
  GLenum GetType();

  bool has_depth_buffer_;
  bool has_stencil_buffer_;
};

// Not a huge fan of this solution but works for now.
class DefaultFramebuffer : public Framebuffer {
public:
  DefaultFramebuffer(uint32_t x, uint32_t y)
    : Framebuffer(x, y) {
  }

  uint32_t BindRead(uint32_t) const override {
    glBindFramebuffer(GL_READ_FRAMEBUFFER, 0);
    return 0;
  }

  void BindWrite() override {
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
  }

  void ResizeTarget(uint32_t x, uint32_t y) override {
    size_x_ = x;
    size_y_ = y;
  }
private:
};

} // namespace renderer

#endif // OGL_FRAMEBUFFER_H_
