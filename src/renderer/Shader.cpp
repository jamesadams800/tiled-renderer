#include "Shader.h"

#include <iostream>
#include <sstream>
#include <fstream>
#include <regex>

namespace renderer {

Shader::Shader()
  : source_files_{},
    source_cache_{},
    shader_handles_ { 0 },
    shader_program_{ 0 },
    work_group_size_{ 0 } {
}

Shader::~Shader() {
  this->DestroyShader();
}

void Shader::BindUniform(const std::string & uniform_name, int32_t uniform) const {
  auto uniform_location = glGetUniformLocation(shader_program_, uniform_name.c_str());
  glUniform1i(uniform_location, uniform);
}


void Shader::BindUniform(const std::string & uniform_name, float uniform) const {
  auto uniform_location = glGetUniformLocation(shader_program_, uniform_name.c_str());
  glUniform1f(uniform_location, uniform);
}

void Shader::BindUniform(const std::string & uniform_name, const glm::vec2 & uniform) const {
  auto uniform_location = glGetUniformLocation(shader_program_, uniform_name.c_str());
  glUniform2fv(uniform_location, 1, glm::value_ptr(uniform));
}

void Shader::BindUniform(const std::string & uniform_name, const glm::vec3 & uniform) const {
  auto uniform_location = glGetUniformLocation(shader_program_, uniform_name.c_str());
  glUniform3fv(uniform_location, 1, glm::value_ptr(uniform));
}

void Shader::BindUniform(const std::string & uniform_name, const glm::vec4 & uniform) const {
  auto uniform_location = glGetUniformLocation(shader_program_, uniform_name.c_str());
  glUniform4fv(uniform_location, 1, glm::value_ptr(uniform));
}

void Shader::BindUniform(const std::string & uniform_name, const glm::mat4 & uniform) const {
  auto uniform_location = glGetUniformLocation(shader_program_, uniform_name.c_str());
  glUniformMatrix4fv(uniform_location, 1, GL_FALSE, glm::value_ptr(uniform));
}

void Shader::DestroyShader() {
  glUseProgram(0);
  glDeleteProgram(shader_program_);
  shader_program_ = 0;
  for (auto& handle : shader_handles_) {
    if (handle) {
      glDeleteShader(handle);
      handle = 0;
    }
  }
}

void Shader::AddSource(ShaderType shader_type, const std::string & source_file) {
  assert(shader_handles_[shader_type] == 0);
  assert(shader_type < MAX_SHADER_INDEX && shader_type >= 0);
  this->LoadSource(source_file, shader_type);
}

void Shader::BuildShader() {
  assert(shader_program_ == 0);
  shader_program_ = glCreateProgram();
  for (uint32_t i = 0; i < MAX_SHADER_INDEX; i++) {
    const std::string& source_file = source_files_[i];
    if (!source_file.empty()) {
      this->BuildShaderStage(ShaderType(i));
    }
  }
  this->LinkShaderProgram();
}

void Shader::BuildShaderStage(ShaderType shader_type) {
  std::unique_lock<std::mutex> lock(source_mutex_);
  assert(shader_handles_[shader_type] == 0);
  assert(shader_type < MAX_SHADER_INDEX && shader_type >= 0);
  assert(!source_cache_[shader_type].empty());

  GLenum gl_shader_type;
  const std::string& source_file = source_files_[shader_type];
  switch (shader_type) {
    case VERTEX_SHADER: gl_shader_type = GL_VERTEX_SHADER; break;
    case TESSELATION_CONTROL_SHADER: gl_shader_type = GL_TESS_CONTROL_SHADER; break;
    case TESSELATION_EVAL_SHADER: gl_shader_type = GL_TESS_EVALUATION_SHADER; break;
    case GEOMETRY_SHADER: gl_shader_type = GL_GEOMETRY_SHADER; break;
    case FRAGMENT_SHADER: gl_shader_type = GL_FRAGMENT_SHADER; break;
    case COMPUTE_SHADER: gl_shader_type = GL_COMPUTE_SHADER; break;
    default: {
      throw std::runtime_error("Failed to Add Shader program [" + source_file +"]"
                               " shader type " + std::to_string(shader_type) + " unsupported");
    }
  }
  auto ret = this->CompileShader(gl_shader_type,
                                 source_cache_[shader_type],
                                 shader_handles_[shader_type]);
  source_cache_[shader_type].clear();
  if (ret == GL_FALSE) {
    char error_buffer[255] = { 0 };
    GLsizei output_size = 0;
    glGetShaderInfoLog(shader_handles_[shader_type], sizeof(error_buffer), &output_size, error_buffer);
    throw std::runtime_error("Failed to compile " + source_file + " - Error: " + error_buffer);
  }
  glAttachShader(shader_program_, shader_handles_[shader_type]);
}

void Shader::LinkShaderProgram() {
  std::stringstream ss;
  GLint program_link_status;
  glLinkProgram(shader_program_);
  glGetProgramiv(shader_program_, GL_LINK_STATUS, &program_link_status);
  if (program_link_status == GL_FALSE) {
    char error_buffer[255] = { 0 };
    GLsizei output_size = 0;
    glGetProgramInfoLog(shader_program_, sizeof(error_buffer), &output_size, error_buffer);
    ss << "Failed to Link Shader programs ";
    for (auto& source : source_files_) {
      if (!source.empty()) {
        ss << "[" << source << "] ";
      }
    }
    ss << "Error : " << error_buffer << std::endl;
    throw std::runtime_error(ss.str());
  }
  ss << "Created Shader pipeline ";
  for (auto& source : source_files_) {
    if (!source.empty()) {
      ss << "[" << source << "] ";
    }
  }
  std::cout << ss.str() << std::endl;
  if (shader_handles_[COMPUTE_SHADER]) {
    GLint buffer[3];
    glGetProgramiv(shader_program_, GL_COMPUTE_WORK_GROUP_SIZE, &buffer[0]);
    work_group_size_.x = buffer[0];
    work_group_size_.y = buffer[1];
    work_group_size_.z = buffer[2];
  }
}

GLint Shader::CompileShader(GLenum shader_type, const std::string & source, GLuint & shader_handle) {
  shader_handle = glCreateShader(shader_type);
  const char *c_str = source.c_str();
  glShaderSource(shader_handle, 1, &c_str, nullptr);
  glCompileShader(shader_handle);

  GLint shader_compilation_status;
  glGetShaderiv(shader_handle, GL_COMPILE_STATUS, &shader_compilation_status);
  return shader_compilation_status;
}

void Shader::LoadSource(const std::string & source_file, ShaderType type) {
  assert(type < MAX_SHADER_INDEX && type >= 0);
  std::unique_lock<std::mutex> lock(source_mutex_);
  std::ifstream shader_in_stream(source_file);
  std::string shader_source;
  shader_source.assign(std::istreambuf_iterator<char>(shader_in_stream),
                       std::istreambuf_iterator<char>());
  source_files_[type] = source_file;
  source_cache_[type] = shader_source;
}

void Shader::ReloadSource() {
  for (uint32_t i = 0; i < MAX_SHADER_INDEX; i++) {
    const std::string& source_file = source_files_[i];
    if (!source_file.empty()) {
      this->LoadSource(source_file, ShaderType(i));
    }
  }
}

} // namespace renderer
