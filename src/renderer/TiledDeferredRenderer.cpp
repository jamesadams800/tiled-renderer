#include "TiledDeferredRenderer.h"

#include "SceneImpl.h"
#include "Mesh.h"
#include "TextUiElement.h"
#include "ShapeFactory.h"

namespace renderer {

TiledDeferredRenderer::TiledDeferredRenderer(uint32_t size_x, uint32_t size_y)
  : Renderer(size_x, size_y),
    shadow_pass_{ shader_lib_ },
    geometry_pass_{ shader_lib_ },
    lighting_pass_{ shader_lib_ },
    forward_pass_{shader_lib_},
    bloom_pass_{ shader_lib_ },
    tone_map_pass_{ shader_lib_ },
    fxaa_pass_{ shader_lib_ },
    ui_pass_{ shader_lib_ },
    gbuffer_{ nullptr },
    hdr_buffer_{ nullptr },
    aa_buffer_{ nullptr } {
}

TiledDeferredRenderer::~TiledDeferredRenderer() {
}

void TiledDeferredRenderer::Init() {
  // Init render passes.
  shadow_pass_.Init();
  geometry_pass_.Init();
  lighting_pass_.Init();
  bloom_pass_.Init();
  tone_map_pass_.Init();
  forward_pass_.Init();
  fxaa_pass_.Init();
  ui_pass_.Init();

  // Create Default Framebuffer object.
  default_framebuffer_ = std::make_unique<DefaultFramebuffer>(size_x_, size_y_);

  // Init GBuffer.
  gbuffer_ = std::make_unique<GBuffer>(size_x_, size_y_);
  gbuffer_->AddComponent(GBuffer::Component::POSITION, GL_RGBA, GL_FLOAT, GL_RGB32F);
  gbuffer_->AddComponent(GBuffer::Component::NORMAL);
  gbuffer_->AddComponent(GBuffer::Component::ALBEDO, GL_RGBA, GL_UNSIGNED_INT, GL_RGBA);
  gbuffer_->AddComponent(GBuffer::Component::SPECULAR, GL_RGBA, GL_FLOAT, GL_RGBA16F);

  // Init HDR Framebuffer.
  hdr_buffer_ = std::make_unique<HDRFramebuffer>(size_x_, size_y_);
  hdr_buffer_->AddDepthBuffer();
  aa_buffer_ = std::make_unique<HDRFramebuffer>(size_x_, size_y_);

  //tone_map_pass_.SetTechnique(TonemapPass::Technique::FILMIC);
}

void TiledDeferredRenderer::Render(const Scene::Impl & scene) {
  shadow_pass_.Execute(scene, *default_framebuffer_, *default_framebuffer_);
  geometry_pass_.Execute(scene, *default_framebuffer_, *gbuffer_);
  lighting_pass_.Execute(scene, *gbuffer_, *hdr_buffer_);
  forward_pass_.Execute(scene, *hdr_buffer_, *hdr_buffer_);
  bloom_pass_.Execute(scene, *hdr_buffer_, *hdr_buffer_);
  tone_map_pass_.Execute(scene, *hdr_buffer_, *aa_buffer_);
  fxaa_pass_.Execute(scene, *aa_buffer_, *default_framebuffer_);
  ui_pass_.Execute(scene, *default_framebuffer_, *default_framebuffer_);
}

void TiledDeferredRenderer::ResizeCallback() {
  default_framebuffer_->Resize(size_x_, size_y_);
  hdr_buffer_->Resize(size_x_, size_y_);
  aa_buffer_->Resize(size_x_, size_y_);
  gbuffer_->Resize(size_x_, size_y_);
}

} // namespace renderer
