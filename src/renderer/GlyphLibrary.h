#ifndef OGL_GLYPHLIBRARY_H_
#define OGL_GLYPHLIBRARY_H_

#include <stdint.h>
#include <unordered_map>
#include <memory>

#pragma warning(push, 0)  
#include <glm/glm.hpp>
#include <glm/ext.hpp>
#pragma warning(pop)
#include <ft2build.h>
#include FT_FREETYPE_H  

namespace renderer {

struct Glyph {
  glm::ivec2 size;
  glm::ivec2 bearing;
  int32_t advance;
  std::unique_ptr<uint8_t[]> buffer;
};

class GlyphLibrary {
public:
  GlyphLibrary();
  ~GlyphLibrary();

  const Glyph& GetGlyph(char);

private:
  void LoadGlyphs();
  void LoadCharacterGlyph(char);

  FT_Library ft_lib_;
  FT_Face ft_face_;
  std::unordered_map< char, Glyph > glyphs_;
};

} // namespace renderer 

#endif // OGL_GLYPHLIBRARY_H_