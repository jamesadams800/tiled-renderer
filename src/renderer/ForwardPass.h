#ifndef OGL_FORWARDPASS_H_
#define OGL_FORWARDPASS_H_

#include "RenderPass.h"

namespace renderer {

class ForwardPass : public RenderPass {
public:
  ForwardPass(ShaderLibrary& lib)
    : RenderPass{ lib } {
  }
  virtual ~ForwardPass() {}

  void Execute(const Scene::Impl& scene, const Framebuffer& input, Framebuffer & output) override;

protected:
  void InitPass() override;
private:
  std::unique_ptr<Mesh> light_sphere_;
};

} // namespace renderer

#endif // OGL_FORWARDPASS_H_