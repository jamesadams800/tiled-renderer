#include "GlyphLibrary.h"

#include <stdexcept>
#include <string>
#include <iostream>

namespace renderer {

GlyphLibrary::GlyphLibrary()
  : ft_lib_{ 0 },
    ft_face_{ 0 } {
  auto err = FT_Init_FreeType(&ft_lib_);
  if (err) {
    throw std::runtime_error("GlphyLibrary : Failed to initialise Freetype.  Error code: " + err);
  }
  this->LoadGlyphs();
}

GlyphLibrary::~GlyphLibrary() {
  FT_Done_Face(ft_face_);
  FT_Done_FreeType(ft_lib_);
}

const Glyph& GlyphLibrary::GetGlyph(char character) {
  auto found = glyphs_.find(character);
  if (found == glyphs_.end()) {
    std::cerr << "GlyphLibrary::GetGlyph : Glyph for character \'" 
              << character << "\' is not present. Attempting Freetype load." << std::endl;
    this->LoadCharacterGlyph(character);
    found = glyphs_.find(character);
  }
  return found->second;
}

void GlyphLibrary::LoadGlyphs() {
  // Hard coding this for now...
  auto err = FT_New_Face(ft_lib_, "assets/fonts/arial.ttf", 0, &ft_face_);
  if (err) {
    throw std::runtime_error("GlphyLibrary : Failed to load Freetype font \'arial\'."
                             "  Error code: " + err);
  }
  FT_Set_Pixel_Sizes(ft_face_, 0, 64);
  for (char character = 0; character < std::numeric_limits<char>::max(); character++) {
    this->LoadCharacterGlyph(character);
  }
}

void GlyphLibrary::LoadCharacterGlyph(char character) {
  if (FT_Load_Char(ft_face_, character, FT_LOAD_RENDER)) {
    throw std::runtime_error("GlyphLibrary::LoadGlyphs : Failed to load glyph \'" +
                             std::to_string(character) + "\'");
  }
  assert(ft_face_->glyph->bitmap.width == ft_face_->glyph->bitmap.pitch);
  Glyph new_glyph;
  new_glyph.size = { ft_face_->glyph->bitmap.width, ft_face_->glyph->bitmap.rows };
  new_glyph.bearing = { ft_face_->glyph->bitmap_left, ft_face_->glyph->bitmap_top };
  new_glyph.advance = ft_face_->glyph->advance.x / 64; // Convert advance to pixels.
  size_t glyph_size = new_glyph.size.x * new_glyph.size.y;
  if (glyph_size > 0) {
    new_glyph.buffer.reset(new uint8_t[glyph_size]);
    std::memcpy(new_glyph.buffer.get(),
                ft_face_->glyph->bitmap.buffer,
                glyph_size);
  }
  glyphs_.insert(std::make_pair(character, std::move(new_glyph)));
}

} // namespace renderer