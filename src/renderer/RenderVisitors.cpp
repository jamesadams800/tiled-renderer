#include "RenderVisitors.h"

#include <GL/glew.h>

#include "Mesh.h"

namespace renderer {

///////////////////////////////////////////////////////////////////////////////////////////////////
// FillRenderVisitor
///////////////////////////////////////////////////////////////////////////////////////////////////
FillRenderVisitor::FillRenderVisitor() {
  glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
}

FillRenderVisitor::~FillRenderVisitor() {
}

void FillRenderVisitor::Visit(const Mesh & mesh) {
  if (!mesh.IsVisible()) {
    return;
  }
  mesh.GetGeometry().Bind();
  if (mesh.GetGeometry().IsIndexed()) {
    glDrawElements(GL_TRIANGLES,
                   mesh.GetGeometry().GetIndexBuffer().Size(),
                   GL_UNSIGNED_INT,
                   nullptr);
  }
  else {
    glDrawArrays(GL_TRIANGLES, 0, mesh.GetGeometry().GetVertexBuffer().Size());
  }
}

///////////////////////////////////////////////////////////////////////////////////////////////////
// WireframeRenderVisitor
///////////////////////////////////////////////////////////////////////////////////////////////////
WireframeRenderVisitor::WireframeRenderVisitor() {
  glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
}

WireframeRenderVisitor::~WireframeRenderVisitor() {
}

void WireframeRenderVisitor::Visit(const Mesh & mesh) {
  if (!mesh.IsVisible()) {
    return;
  }
  
  mesh.GetGeometry().Bind();
  if (mesh.GetGeometry().IsIndexed()) {
    glDrawElements(GL_TRIANGLES,
      mesh.GetGeometry().GetIndexBuffer().Size(),
      GL_UNSIGNED_INT,
      nullptr);
  }
  else {
    glDrawArrays(GL_TRIANGLES, 0, mesh.GetGeometry().GetVertexBuffer().Size());
  }
}

///////////////////////////////////////////////////////////////////////////////////////////////////
// BatchRenderVisitor
///////////////////////////////////////////////////////////////////////////////////////////////////
BatchRenderVisitor::BatchRenderVisitor()
  : BatchRenderVisitor{ 0 } {
}

BatchRenderVisitor::BatchRenderVisitor(uint32_t inst) 
  : instance_count_{ inst } {
}

BatchRenderVisitor::~BatchRenderVisitor() {
}

void BatchRenderVisitor::Visit(const Mesh & mesh) {
  if (!mesh.IsVisible()) {
    return;
  }
  mesh.GetGeometry().Bind();
  if (mesh.GetGeometry().IsIndexed()) {
    glDrawElementsInstanced(GL_TRIANGLES,
                            mesh.GetGeometry().GetIndexBuffer().Size(),
                            GL_UNSIGNED_INT,
                            nullptr,
                            instance_count_);
  }
  else {
    glDrawArraysInstanced(GL_TRIANGLES,
                          0,
                          mesh.GetGeometry().GetVertexBuffer().Size(),
                          instance_count_);
  }
}


} // namespace renderer
