#ifndef OGL_BLOOMPASS_H_
#define OGL_BLOOMPASS_H_

#include <assert.h>

#include "RenderPass.h"
#include "BlurPass.h"
#include "TextureFramebuffer.h"

namespace renderer {

class BloomPass : public RenderPass {
public:
  BloomPass(ShaderLibrary& lib)
    : blur_pass_1_{ lib },
      blur_pass_2_{ lib },
      high_intensity_buffer_{nullptr},
      RenderPass(lib) {
  }

  void Execute(const Scene::Impl&, const Framebuffer & input, Framebuffer & output) override;
  void InitPass() override;
private:
  BlurPass blur_pass_1_;
  BlurPass blur_pass_2_;

  std::unique_ptr<HDRFramebuffer> high_intensity_buffer_;
};

}

#endif // OGL_BLOOMPASS_H_