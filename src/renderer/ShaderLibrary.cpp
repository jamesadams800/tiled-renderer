#include "ShaderLibrary.h"

#include <iostream>
#include <regex>

namespace renderer {

std::unordered_map<char, Material::Layers> ShaderLibrary::kGeometryPresence_ = {
  {'A', Material::ALBEDO_MAP },
  {'S', Material::SPECULAR_MAP },
  {'N', Material::NORMAL_MAP },
  {'O', Material::ALPHA_MAP }
};

ShaderLibrary::ShaderLibrary(const std::string & directory) 
  : shader_directory_{ directory },
    shader_storage_{},
    geometry_pass_shaders_{},
    exit_thread_{false},
    file_watcher_thread_{nullptr},
    file_watcher_thread_mutex_{} {
}

ShaderLibrary::~ShaderLibrary() {
  {
    std::unique_lock<std::mutex> lock(file_watcher_thread_mutex_);
    exit_thread_ = true;
  }
  if (file_watcher_thread_) {
    file_watcher_thread_->join();
  }
}

const Shader* ShaderLibrary::GetGeometryPassShader(Material::LayerPresence presence) {
  auto search_it = geometry_pass_shaders_.find(presence);
  if (search_it != geometry_pass_shaders_.end()) {
    auto& shader_entry = search_it->second;
    if (shader_entry->swap_shader) {
      this->RebuildShaderEntry(*shader_entry);
    }
    return shader_entry->shader.get();
  }
  return nullptr;
}

const Shader* ShaderLibrary::GetShader(const std::string & name) {
  auto search_it = shader_storage_.find(name);
  if (search_it != shader_storage_.end()) {
    auto& shader_entry = search_it->second;
    if (shader_entry.swap_shader) {
      this->RebuildShaderEntry(shader_entry);
    }
    return shader_entry.shader.get();
  }
  return nullptr;
}

void ShaderLibrary::RebuildShaderEntry(ShaderEntry & entry) {
  try {
    entry.swap_shader->BuildShader();
    entry.shader = std::move(entry.swap_shader);
    entry.swap_shader.reset();
  }
  catch (std::runtime_error& ex) {
    std::cerr << "ShaderLibrary::GetShader : Failed to compile swap shader. " << std::endl;
    std::cerr << ex.what();
    entry.swap_shader.reset();
  }
}

void ShaderLibrary::CheckEntries() {
  for (auto& pair : shader_storage_) {
    ShaderEntry& entry = pair.second;
    for (auto& source_entry : entry.sources) {
      auto source_write_time = fs::last_write_time(source_entry);
      if (source_write_time > entry.last_write) {
        entry.last_write = source_write_time;
        entry.swap_shader = std::move(CreateShaderFromSources(entry.sources));
        return;
      }
    }
  }
}

// This is a work around for now until i write a GLSL Code generator.  The geometry pass shaders 
// are used to match the material type so that correct values are read into the GBuffer. The prefix
// letters in front of the shader name correspond to the maps that the material uses. The characters
// in the map kGeometryPresence_ show which characters correspond to which maps.
void ShaderLibrary::RegisterGeometryPassShaders() {
  static std::regex geometry_pass_regex("(.*)" + std::string(GEOMETRY_PASS_SHADER_SUFFIX));
  for (auto& shader : shader_storage_) {
    const std::string& shader_name = shader.first;
    std::smatch match;
    if (std::regex_match(shader_name, match, geometry_pass_regex)) {
      // It is a geometry pass shader.  Lets get its capabilities.
      // More than one group so it has prefixes.
      if (match.size() > 1) {
        Material::LayerPresence presence = 0;
        for (char character : match[1].str()) {
          auto presence_it = kGeometryPresence_.find(character);
          if (presence_it == kGeometryPresence_.end()) {
            throw std::runtime_error("ShaderLibrary::RegisterGeometryPassShaders() : "
                                     "no presence mask match found for character " + character);
          }
          presence |= presence_it->second;
        }
        geometry_pass_shaders_.insert(std::make_pair(presence, &shader.second));
      }
    }
  }
}

void ShaderLibrary::WatcherThreadMain() {
  for (;;) {
    {
      std::unique_lock<std::mutex> lock(file_watcher_thread_mutex_);
      if (exit_thread_) {
        return;
      }
    }
    std::this_thread::sleep_for(std::chrono::seconds(1));
    this->CheckEntries();
  }
}

std::unique_ptr<Shader>
ShaderLibrary::CreateShaderFromSources(const std::vector<fs::directory_entry>& sources) {
  auto shader = std::make_unique<Shader>();
  for (auto& source : sources) {
    std::string extension = source.path().extension().string();
    Shader::ShaderType type = Shader::TypeFromExtension(extension);
    if (type == Shader::UNKNOWN_SHADER_TYPE) {
      throw std::runtime_error("CreateShader() : Unsupported shader extension [" + extension +
                               "] for shader [ " + source.path().stem().string() + "]");
    }
    shader->AddSource(type, source.path().string());
  }
  return std::move(shader);
}

void ShaderLibrary::CheckShaderDirectory() {
  std::vector<std::string> file_name_stems;
  for (const auto& entry : fs::directory_iterator(shader_directory_)) {
    file_name_stems.push_back(entry.path().stem().string());
  }
  
  auto end = std::unique(std::begin(file_name_stems), std::end(file_name_stems));
  file_name_stems.erase(end, std::end(file_name_stems));
  for (auto& entry : file_name_stems) {
    this->CreateShader(entry);
  }
  this->RegisterGeometryPassShaders();
}

void ShaderLibrary::Init() {
  this->CheckShaderDirectory();
  file_watcher_thread_.reset(new std::thread(&ShaderLibrary::WatcherThreadMain, this));
}

void ShaderLibrary::CreateShader(const std::string & shader_name) {
  std::vector<fs::directory_entry> shader_sources;
  fs::file_time_type last_write;
  for (const auto& entry : fs::directory_iterator(shader_directory_)) {
    std::string filename = entry.path().stem().string();
    if (filename == shader_name) {
      if (last_write < fs::last_write_time(entry)) {
        last_write = fs::last_write_time(entry);
      }
      shader_sources.push_back(entry);
    }
  }

  auto shader = CreateShaderFromSources(shader_sources);
  // Finalise shader.
  shader->BuildShader();
  {
    std::unique_lock<std::mutex> lock(file_watcher_thread_mutex_);
    auto existing_entry = shader_storage_.find(shader_name);
    if (existing_entry != shader_storage_.end()) {
      std::cout << "Shader " << shader_name << " already exists. Replacing with new entry." 
                << std::endl;
      shader_storage_.erase(existing_entry);
    }
    ShaderEntry entry;
    entry.sources = shader_sources;
    entry.last_write = last_write;
    entry.shader = std::move(shader);
    shader_storage_.insert(std::make_pair(shader_name, std::move(entry)));
  }
}

} // namespace renderer