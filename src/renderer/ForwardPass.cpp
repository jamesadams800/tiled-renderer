#include "ForwardPass.h"

#include "Framebuffer.h"
#include "ShapeFactory.h"
#include "RenderVisitors.h"
#include "SceneImpl.h"
#include "ShaderLibrary.h"

namespace renderer {

void ForwardPass::Execute(const Scene::Impl& scene, const Framebuffer & , Framebuffer & output) {
  output.BindWrite();
  glEnable(GL_DEPTH_TEST);
  BatchRenderVisitor visitor(scene.GetPointLightBuffer().Size());
  glm::mat4 proj_view = scene.GetCamera()->GetProjectionMatrix() * scene.GetCamera()->GetViewMatrix();
  const Shader* point_light_render = shader_lib_.GetShader("PointLightRender");
  point_light_render->Attach();
  point_light_render->BindUniform("vp_matrix", proj_view);
  point_light_render->BindStorage("PointLights", scene.GetPointLightBuffer());
  visitor.Visit(*light_sphere_);

  // TODO: Transparents.
}

void ForwardPass::InitPass() {
  light_sphere_.reset(ShapeFactory::BuildSphere(1.0f, 8));
}

} // namespace renderer
