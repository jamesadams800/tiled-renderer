#ifndef OGL_SHADOWMAP_H_
#define OGL_SHADOWMAP_H_

#include <string>

#pragma warning(push, 0)
#include <glm/ext.hpp>
#include <glm/glm.hpp>
#pragma warning(pop)

#include "Framebuffer.h"

namespace renderer {

class Scene;
class ShadowPass;

class ShadowMap : public Framebuffer {
public:
  static constexpr uint32_t SHADOW_MAP_X_RESOLUTION = 2048u; //8192;//4096;//2048;
  static constexpr uint32_t SHADOW_MAP_Y_RESOLUTION = 2048u; //8192;//4096;//2048;

  explicit ShadowMap(uint32_t x = SHADOW_MAP_X_RESOLUTION,
                     uint32_t y = SHADOW_MAP_Y_RESOLUTION);

  void ResizeTarget(uint32_t, uint32_t) override;
protected:
private:
};

} // namespace renderer

#endif // OGL_SHADOWMAP_H_