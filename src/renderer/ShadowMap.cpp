#include "ShadowMap.h"

#include <array>

namespace renderer {

ShadowMap::ShadowMap(uint32_t x, uint32_t y) 
  : Framebuffer(x, y) {
  this->AddDepthBuffer();
}

void ShadowMap::ResizeTarget(uint32_t, uint32_t) {
}

}  // namespace renderer
