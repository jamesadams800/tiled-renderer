#ifndef OGL_RENDERVISITOR_H_
#define OGL_RENDERVISITOR_H_

#include <stdint.h>
#include <array>
#include <memory>

namespace renderer {

class Mesh;

class RenderVisitor {
public:
  virtual ~RenderVisitor() {}
  virtual void Visit(const Mesh&) = 0;
};

class FillRenderVisitor : public RenderVisitor {
public:
  FillRenderVisitor();
  ~FillRenderVisitor();
  void Visit(const Mesh &) override;
};

class WireframeRenderVisitor : public RenderVisitor {
public:
  WireframeRenderVisitor();
  ~WireframeRenderVisitor();
  void Visit(const Mesh &) override;
};

class BatchRenderVisitor : public RenderVisitor {
public:
  explicit BatchRenderVisitor(uint32_t inst);
  BatchRenderVisitor();
  ~BatchRenderVisitor();

  void SetInstances(uint32_t inst) {
    instance_count_ = inst;
  }
  uint32_t GetInstances() const {
    return instance_count_;
  }
  void Visit(const Mesh &) override;

private:
  uint32_t instance_count_;
};

} // namespace renderer

#endif // OGL_RENDERVISITOR_H_