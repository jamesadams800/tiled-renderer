#include "Renderer.h"

#include "Mesh.h"

namespace renderer {

Renderer::Renderer(uint32_t size_x, uint32_t size_y)
  : clear_color_{ { 0.0f,0.0f,0.0f,0.0f } },
    size_x_{ size_x },
    size_y_{ size_y } {
}

Renderer::~Renderer() {
}

void Renderer::SetClearColor(float r, float g, float b) {
  clear_color_[0] = r;
  clear_color_[1] = g;
  clear_color_[2] = b;
  clear_color_[3] = 0;
}

void Renderer::Resize(uint32_t x, uint32_t y) {
  size_x_ = x;
  size_y_ = y;
  this->ResizeCallback();
}

void Renderer::InitRenderer() {
  shader_lib_.Init();
  // Init Callback.
  this->Init();
}

} // namespace renderer
