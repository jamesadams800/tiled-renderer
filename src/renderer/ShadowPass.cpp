#include "ShadowPass.h"

#include "ShaderLibrary.h"
#include "RenderVisitors.h"
#include "DirectionalLight.h"
#include "CascadedShadowMap.h"
#include "SceneImpl.h"

namespace renderer {

void ShadowPass::InitPass() {
}

void ShadowPass::Execute(const Scene::Impl& scene, const Framebuffer&, Framebuffer&) {
  int32_t light_index = 0;
  for (const auto& directional_light : scene.GetDirectionalLights()) {
    if (const auto& shadow_map = directional_light->GetShadowMap()) {
      this->Render(scene, light_index, *shadow_map);
    }
    ++light_index;
  }
}


void ShadowPass::Render(const Scene::Impl& scene, int32_t light_index, CascadedShadowMap& map) const {
  glDepthMask(GL_TRUE);
  glDisable(GL_BLEND);
  glEnable(GL_DEPTH_TEST);
  glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);
  glEnable(GL_DEPTH_CLAMP);
  glEnable(GL_CULL_FACE);
  glCullFace(GL_FRONT);
  const Shader* cascade = shader_lib_.GetShader("CascadeMultiCast");
  map.BindWrite();
  map.BindViewports();
  glClear(GL_DEPTH_BUFFER_BIT);
  cascade->Attach();
  cascade->BindUniform("CascadeShadowData", scene.GetCascadeData());
  cascade->BindUniform("light_index", light_index);
  BatchRenderVisitor visitor(map.GetNoCascades());
  for (auto& renderable : scene.GetRenderables()) {
    cascade->BindUniform("model_matrix", renderable->GetWorldTransform());
    renderable->Accept(visitor);
  }
  glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
  glCullFace(GL_BACK);
  glDisable(GL_DEPTH_CLAMP);
}

} // namespace renderer
