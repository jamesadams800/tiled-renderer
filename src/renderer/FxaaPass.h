#ifndef OGL_FXAAPASS_H_
#define OGL_FXAAPASS_H_

#include "RenderPass.h"

namespace renderer {

class FxaaPass : public RenderPass {
public:
  FxaaPass(ShaderLibrary& lib)
    : RenderPass{ lib } {
  }
  virtual ~FxaaPass() {}

  void Execute(const Scene::Impl&, const Framebuffer& input, Framebuffer & output) override;

protected:
  void InitPass() override;
private:
};

} // namespace renderer

#endif // OGL_FXAAPASS_H_