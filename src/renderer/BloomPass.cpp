#include "BloomPass.h"

#include "TextureFramebuffer.h"
#include "ShaderLibrary.h"

namespace renderer {

void BloomPass::InitPass() {
  blur_pass_1_.CreatePingPong<HDRFramebuffer>(640, 480);
  blur_pass_1_.SetAttachment(0);
  blur_pass_1_.SetTechnique(BlurPass::Technique::BOX);
  blur_pass_1_.SetKernelSize(9);
  blur_pass_1_.Init();

  blur_pass_2_.CreatePingPong<HDRFramebuffer>(640, 480);
  blur_pass_2_.SetAttachment(0);
  blur_pass_2_.SetTechnique(BlurPass::Technique::GAUSSIAN);
  blur_pass_2_.SetSigma(0.8f);
  blur_pass_2_.SetKernelSize(9);
  blur_pass_2_.Init();
  high_intensity_buffer_ = std::make_unique<HDRFramebuffer>(640, 480);
}

void BloomPass::Execute(const Scene::Impl& scene, const Framebuffer & input, Framebuffer& output) {
  assert(high_intensity_buffer_);
  if (high_intensity_buffer_->GetX() != input.GetX() ||
    high_intensity_buffer_->GetY() != input.GetY()) {
    high_intensity_buffer_->Resize(input.GetX(), input.GetY());
  }
  // Extract high intensity values from HDR framebuffer.
  const Shader* extract_shader = shader_lib_.GetShader("BloomPrePass");
  assert(extract_shader);
  input.BindRead();
  high_intensity_buffer_->BindWrite();
  glClear(GL_COLOR_BUFFER_BIT);
  glDisable(GL_DEPTH_TEST);
  extract_shader->Attach();
  this->DrawFullScreenQuad();

  // Do a two pass blur the High intensity values.
  blur_pass_2_.Execute(scene, *high_intensity_buffer_, *high_intensity_buffer_);
  // On second pass additivly blend the bloom values with the output framebuffer.
  glEnable(GL_BLEND);
  glBlendEquation(GL_FUNC_ADD);
  glBlendFunc(GL_ONE, GL_ONE);
  blur_pass_2_.Execute(scene, *high_intensity_buffer_, output);
}

} // namespace renderer
