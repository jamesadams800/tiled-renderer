#include "FxaaPass.h"

#include <assert.h>

#include "Framebuffer.h"
#include "ShaderLibrary.h"

namespace renderer {

void FxaaPass::Execute(const Scene::Impl&, const Framebuffer& input, Framebuffer & output) {
  const Shader* fxaa = shader_lib_.GetShader("FXAA");
  assert(fxaa);
  input.BindRead();
  output.BindWrite();
  glClear(GL_COLOR_BUFFER_BIT);
  glDisable(GL_DEPTH_TEST);
  fxaa->Attach();
  this->DrawFullScreenQuad();
}

void FxaaPass::InitPass() {
}

} // namespace renderer
