#include "GBuffer.h"

#include <string>
#include <algorithm>

#include <GL/glew.h>

namespace renderer {

std::map<GBuffer::Component, std::string> GBuffer::kComponentStrings_ = {
  { Component::POSITION, "Position" },
  { Component::ALBEDO, "Albedo" },
  { Component::NORMAL, "Normal" },
  { Component::SPECULAR, "Specular" }
};

GBuffer::GBuffer(uint32_t x, uint32_t y)
  : attachments_{},
    component_description_{},
    textures_{}, 
    Framebuffer(x, y) {
  this->AddDepthBuffer();
}

GBuffer::~GBuffer() {
  this->DestroyTextures();
}

void GBuffer::AddComponent(Component component, GLuint format, GLuint type, GLuint internal_format) {
  GLuint texture = 0;
  glBindFramebuffer(GL_FRAMEBUFFER, framebuffer_);
  glGenTextures(1, &texture);
  glBindTexture(GL_TEXTURE_2D, texture);
  glTexImage2D(GL_TEXTURE_2D, 0, internal_format, size_x_, size_y_, 0, format, type, nullptr);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  
  GLuint attachment = GL_COLOR_ATTACHMENT0 + component;
  glFramebufferTexture2D(GL_FRAMEBUFFER,
                         attachment,
                         GL_TEXTURE_2D,
                         texture,
                         0);
  glColorMaski(static_cast<uint32_t>(textures_.size()), GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
  ComponentDescriptor details = { format, type, internal_format };
  component_description_.insert(std::make_pair(component, details));
  textures_.insert(std::make_pair(component, texture));
  attachments_.push_back(attachment);
  std::sort(std::begin(attachments_), std::end(attachments_));
  glDrawBuffers(static_cast<uint32_t>(attachments_.size()), attachments_.data());

  GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
  if (status != GL_FRAMEBUFFER_COMPLETE) {
    throw std::runtime_error("GBuffer::AddComponent : " + kComponentStrings_[component] +
                             " failed to create framebuffer. Status[" + std::to_string(status) +
                             "]");
  }
  glBindTexture(GL_TEXTURE_2D, 0);
  glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void GBuffer::BindTextures(uint32_t texture_unit) const {
  for (auto& texture : textures_) {
    glActiveTexture(GL_TEXTURE0 + texture_unit);
    glBindTexture(GL_TEXTURE_2D, texture.second);
    texture_unit++;
  }
}

void GBuffer::ResizeTarget(uint32_t x, uint32_t y) {
  for (auto& texture : textures_) {
    auto descriptor = component_description_[texture.first];
    glBindTexture(GL_TEXTURE_2D, texture.second);
    glTexImage2D(GL_TEXTURE_2D,
                 0,
                 descriptor.internal_format,
                 x,
                 y,
                 0,
                 descriptor.format,
                 descriptor.type,
                 nullptr);
  }
}

void GBuffer::DestroyTextures() {
  glBindTexture(GL_TEXTURE_2D, 0);
  glDeleteTextures(1, &depth_texture_);
  for (auto& texture : textures_) {
    glDeleteTextures(1, &texture.second);
    texture.second = 0;
  }
}

} // namespace renderer
