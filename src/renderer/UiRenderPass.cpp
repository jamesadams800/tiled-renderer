#include "UiRenderPass.h"

#include <GL/glew.h>

#include "ShaderLibrary.h"
#include "SceneImpl.h"
#include "Framebuffer.h"

#include "UserInterface.h"
#include "TextUiElement.h"

namespace renderer {

void UiRenderPass::InitPass() {
}

void UiRenderPass::Execute(const Scene::Impl& scene, const Framebuffer&, Framebuffer & output) {
  output.BindWrite();
  glDisable(GL_DEPTH_TEST);
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  projection_matrix_ = glm::ortho(0.0f, static_cast<float>(output.GetX()),
                                  0.0f, static_cast<float>(output.GetY()));
  for (auto& element : scene.GetUi().GetElements()) {
    element.second->Render(*this);
  }
}

void UiRenderPass::Render(const TextUiElement & element) {
  auto shader = shader_lib_.GetShader("Text");
  shader->Attach();
  shader->BindUniform("color", element.GetColor());
  shader->BindUniform("projection", projection_matrix_ * element.GetScale());
  shader->BindUniform("offset", element.GetPosition());
  element.Bind();
  glDrawArrays(GL_TRIANGLES, 0, 6);
}

} // namespace renderer
