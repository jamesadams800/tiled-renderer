#ifndef OGL_TEXTUREFRAMEBUFFER_H_
#define OGL_TEXTUREFRAMEBUFFER_H_

#include "Framebuffer.h"

namespace renderer {

template <GLint INTERNAL_FORMAT, GLenum FORMAT, GLenum TYPE, GLint FILTER_TYPE = GL_LINEAR>
class TextureFramebuffer : public Framebuffer {
public:
  explicit TextureFramebuffer(uint32_t x, uint32_t y);
  ~TextureFramebuffer();

  uint32_t BindRead(uint32_t texture_unit) const override {
    this->Framebuffer::BindRead();
    this->BindTextures(texture_unit);
    return 1;
  }

  void BindWriteCompute(uint32_t image_unit) override {
    glBindImageTexture(image_unit, texture_, 0, GL_FALSE, 0, GL_WRITE_ONLY, INTERNAL_FORMAT);
  }
private:
  void BindTextures(uint32_t texture_unit) const;
  void CreateTexture();
  void ResizeTarget(uint32_t x, uint32_t y) override;

  GLuint texture_;
};

// Common Framebuffer Types
///////////////////////////////////////////////////////////////////////////////////////////////////
using HDRFramebuffer = TextureFramebuffer<GL_RGBA16F, GL_RGBA, GL_FLOAT>;

// Implementation
///////////////////////////////////////////////////////////////////////////////////////////////////
template <GLint INTERNAL_FORMAT, GLenum FORMAT, GLenum TYPE, GLint FILTER_TYPE>
TextureFramebuffer<INTERNAL_FORMAT, FORMAT, TYPE, FILTER_TYPE>::TextureFramebuffer(uint32_t x, uint32_t y)
  : texture_{ 0 },
    Framebuffer(x, y) {
  this->CreateTexture();
}

template <GLint INTERNAL_FORMAT, GLenum FORMAT, GLenum TYPE, GLint FILTER_TYPE>
TextureFramebuffer<INTERNAL_FORMAT, FORMAT, TYPE, FILTER_TYPE>::~TextureFramebuffer() {
  glBindFramebuffer(GL_FRAMEBUFFER, 0);
  glDeleteTextures(1, &texture_);
}

template <GLint INTERNAL_FORMAT, GLenum FORMAT, GLenum TYPE, GLint FILTER_TYPE>
void TextureFramebuffer<INTERNAL_FORMAT, FORMAT, TYPE, FILTER_TYPE>::BindTextures(uint32_t texture_unit) const {
  glActiveTexture(GL_TEXTURE0 + texture_unit);
  glBindTexture(GL_TEXTURE_2D, texture_);
}

template <GLint INTERNAL_FORMAT, GLenum FORMAT, GLenum TYPE, GLint FILTER_TYPE>
void TextureFramebuffer<INTERNAL_FORMAT, FORMAT, TYPE, FILTER_TYPE>::CreateTexture() {
  glBindFramebuffer(GL_FRAMEBUFFER, framebuffer_);
  glGenTextures(1, &texture_);
  glBindTexture(GL_TEXTURE_2D, texture_);
  glTexImage2D(GL_TEXTURE_2D, 0, INTERNAL_FORMAT, size_x_, size_y_, 0, FORMAT, TYPE, nullptr);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, FILTER_TYPE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, FILTER_TYPE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  glFramebufferTexture2D(GL_FRAMEBUFFER,
                         GL_COLOR_ATTACHMENT0,
                         GL_TEXTURE_2D,
                         texture_,
                         0);
  std::array<GLuint, 1> attachments = { GL_COLOR_ATTACHMENT0 };
  glDrawBuffers(static_cast<GLsizei>(attachments.size()), attachments.data());

  GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
  if (status != GL_FRAMEBUFFER_COMPLETE) {
    throw std::runtime_error("TextureFramebuffer::CreateHdrTextures : failed to create framebuffer."
      " Status[" + std::to_string(status) + "]");
  }

  glBindTexture(GL_TEXTURE_2D, 0);
  glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

template <GLint INTERNAL_FORMAT, GLenum FORMAT, GLenum TYPE, GLint FILTER_TYPE>
void TextureFramebuffer<INTERNAL_FORMAT, FORMAT, TYPE, FILTER_TYPE>::ResizeTarget(uint32_t x, uint32_t y) {
  glBindTexture(GL_TEXTURE_2D, texture_);
  glTexImage2D(GL_TEXTURE_2D, 0, INTERNAL_FORMAT, x, y, 0, FORMAT, TYPE, nullptr);
  glBindTexture(GL_TEXTURE_2D, 0);
}


} // namespace renderer 

#endif // OGL_TEXTUREFRAMEBUFFER_H_