#include "Geometry.h"

#include <GL/glew.h>

namespace renderer {

Geometry::Geometry(Primitive primitive_type)
  : Geometry(nullptr, nullptr, primitive_type) {
}

Geometry::Geometry(std::unique_ptr<VertexBuffer> vertex_buffer, Primitive primitive_type)
  : Geometry(std::move(vertex_buffer), nullptr, primitive_type) {
}

Geometry::Geometry(std::unique_ptr<VertexBuffer> vertex_buffer,
                   std::unique_ptr<IndexBuffer> index_buffer,
                   Primitive primitive_type)
  : vao_handle_{ 0 },
    primitive_type_{ primitive_type },
    vertex_buffer_{ std::move(vertex_buffer) },
    is_indexed_{ index_buffer != nullptr },
    index_buffer_{ std::move(index_buffer) },
    aabb_{} {
  this->CreateGpuObject();
  this->UpdateGpuObject();
  if (vertex_buffer_) {
    aabb_.FitBox(*vertex_buffer_);
  }
}

Geometry::~Geometry() {
  this->DeleteGpuObject();
}

void Geometry::SetVertexBuffer(std::unique_ptr<VertexBuffer> buffer) {
  vertex_buffer_ = std::move(buffer);
  aabb_.FitBox(*vertex_buffer_);
  this->UpdateGpuObject();
}

void Geometry::SetIndexBuffer(std::unique_ptr<IndexBuffer> buffer) {
  is_indexed_ = true;
  index_buffer_ = std::move(buffer);
  this->UpdateGpuObject();
}

inline RENDER_API void Geometry::SetPrimitiveType(Primitive primitive_type) {
  primitive_type_ = primitive_type;
}

void Geometry::Bind() const {
  glBindVertexArray(vao_handle_);
}

void Geometry::CreateGpuObject() {
  assert(vao_handle_ == 0);
  glGenVertexArrays(1, &vao_handle_);
}

void Geometry::UpdateGpuObject() {
  glBindVertexArray(vao_handle_);
  if (auto vertex_buffer = vertex_buffer_.get()) {
    vertex_buffer->Bind();
    if (this->IsIndexed()) {
      this->GetIndexBuffer().Bind();
    }
    uint32_t attribute_index = 0;
    for (auto& map_attribute_entry : vertex_buffer->GetVertexAttributeMap()) {
      const auto& attribute_info = map_attribute_entry.second;
      glEnableVertexAttribArray(attribute_index);
      glVertexAttribPointer(attribute_index, attribute_info.vertex_size,
                            GL_FLOAT, GL_FALSE, attribute_info.vertex_stride,
                            reinterpret_cast<void*>(attribute_info.vertex_offset * sizeof(float)));
      attribute_index++;
    }
  }
  glBindVertexArray(0);
}

void Geometry::DeleteGpuObject() {
  glDeleteVertexArrays(1, &vao_handle_);
}


} // namespace renderer
