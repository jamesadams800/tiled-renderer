#include "Scene.h"

#include "SceneImpl.h"

namespace renderer {

Scene::Scene(Impl& impl)
  : impl_{ impl } {
}

void Scene::SetCamera(std::shared_ptr<Camera> camera) {
  impl_.SetCamera(camera);
}

void Scene::AddRenderable(std::shared_ptr<Renderable> renderable) {
  impl_.AddRenderable(renderable);
}

void Scene::AddLight(std::shared_ptr<DirectionalLight> light) {
  impl_.AddLight(light);
}

void Scene::AddLight(std::shared_ptr<PointLight> light) {
  impl_.AddLight(light);
}

void Scene::AddTexture(const std::string& string, std::shared_ptr<Texture<uint8_t>> texture) {
  impl_.AddTexture(string, texture);
}

void Scene::AddMaterial(uint64_t material_index, std::shared_ptr<Material> material) {
  impl_.AddMaterial(material_index, material);
}

std::shared_ptr<Material> Scene::GetMaterial(uint64_t id) {
  return impl_.GetMaterial(id);
}

std::shared_ptr<Texture<uint8_t>> Scene::GetTexture(const std::string& name) {
  return impl_.GetTexture(name);
}

UserInterface& Scene::GetUi() {
  return impl_.GetUi();
}

const UserInterface& Scene::GetUi() const {
  return impl_.GetUi();
}

uint64_t Scene::GetNextMaterialId() {
  return impl_.GetNextMaterialId();
}

} // namespace renderer
