#include "TextUiElement.h"

#include <algorithm>
#include <iostream>

#include "Texture.h"
#include "UiRenderPass.h"
#include "GlyphLibrary.h"
#include "Renderer.h"

namespace renderer {

GlyphLibrary TextUiElement::glyphs_;

std::vector<float> TextUiElement::kPositions_ = { 0.0f, 0.0f, 0.0f,
                                                  1.0f, 0.0f, 0.0f,
                                                  0.0f, 1.0f, 0.0f,
                                                  0.0f, 1.0f, 0.0f,
                                                  1.0f, 0.0f, 0.0f,
                                                  1.0f, 1.0f, 0.0f };

std::vector<float> TextUiElement::kTexCoords_ = { 0.0f, 1.0f,
                                                  1.0f, 1.0f,
                                                  0.0f, 0.0f,
                                                  0.0f, 0.0f,
                                                  1.0f, 1.0f,
                                                  1.0f, 0.0f };

std::shared_ptr<TextUiElement> TextUiElement::Create(const std::string & text,
                                                     const glm::vec2 & position,
                                                     const glm::vec3 & color) {
  return CreateSharedObject<TextUiElement>(text, position, color);
}

TextUiElement::TextUiElement()
  : TextUiElement("", { 0.0f,0.0f }, { 1.0f,1.0f,1.0f }) {
}

TextUiElement::TextUiElement(const std::string & text, const glm::vec2 & position) 
  : TextUiElement(text, position, { 1.0f,1.0f,1.0f }) {
}

TextUiElement::TextUiElement(const std::string & text, 
                             const glm::vec2 & position,
                             const glm::vec3 & color) 
  : text_{ text },
    position_{ position },
    color_{ color },
    font_size_{ 12 },
    text_geometry_{ Geometry::TRIANGLES },
    string_texture_{new Texture<uint8_t>() } {
  std::unique_ptr<VertexBuffer> vertices(new VertexBuffer());
  vertices->AddVertexAttributeData(VertexBuffer::POSITION, 3, 0, kPositions_);
  vertices->AddVertexAttributeData(VertexBuffer::TEXCOORD, 2, 0, kTexCoords_);
  text_geometry_.SetVertexBuffer(std::move(vertices));
  this->BuildTexture();
}

void TextUiElement::Render(UiRenderPass& pass) {
  pass.Render(*this);
}

void TextUiElement::UpdateText(const std::string & text) {
  text_ = text;
  this->BuildTexture();
}

void TextUiElement::SetFontSize(uint32_t size) {
  font_size_ = size;
}

void TextUiElement::SetColor(const glm::vec3 & color) {
  color_ = color;
}

uint32_t TextUiElement::GetFontSize() const {
  return font_size_;
}

glm::vec2 TextUiElement::GetPosition() const {
  return position_;
}

glm::vec2 TextUiElement::GetTextDims() const {
  float height_in_pixel = font_size_ * 96.0f / 72.0f;
  return { height_in_pixel * length_ratio_, height_in_pixel };
}

glm::vec3 TextUiElement::GetColor() const {
  return color_;
}

void TextUiElement::Bind() const {
  text_geometry_.Bind();
  glActiveTexture(GL_TEXTURE0);
  string_texture_->Bind();
}

void TextUiElement::BuildTexture() {
  // First Calculate texture bounds.
  int32_t max_glyph_height = std::numeric_limits<int32_t>::min();
  int32_t total_width = 0;
  int32_t min_offset = std::numeric_limits<int32_t>::max();
  for (auto& character : text_) {
    const Glyph& glyph = glyphs_.GetGlyph(character);
    max_glyph_height = std::max(max_glyph_height, glyph.bearing.y);
    min_offset = std::min(min_offset, glyph.bearing.y - glyph.size.y);
    total_width += std::max(glyph.size.x, glyph.advance);
  }
  int32_t max_height = std::abs(min_offset) + max_glyph_height;

  size_t string_texture_size = max_height * total_width;
  std::unique_ptr<uint8_t[]> string_texture(new uint8_t[string_texture_size]);
  std::memset(string_texture.get(), 0x0, string_texture_size);
  int32_t character_offset = 0;
  for (auto& character : text_) {
    const Glyph& glyph = glyphs_.GetGlyph(character);
    int32_t offset = (max_glyph_height - glyph.bearing.y);
    if (glyph.size.x > 0) {
      for (int32_t row = 0; row < glyph.size.y; row++) {
          int32_t row_offest = character_offset + ((offset + row) * total_width);
          std::memcpy(&string_texture[row_offest],
                      glyph.buffer.get() + (row * glyph.size.x),
                      glyph.size.x);
      }
    }
    character_offset += std::max(glyph.size.x, glyph.advance);
  }
  string_texture_->LoadTexture(total_width, max_height, 1, string_texture.release());
  length_ratio_ = total_width / static_cast<float>(max_height);

  glm::vec2 pixel_dims = this->GetTextDims();
  text_scale_ = glm::scale(glm::mat4(1), glm::vec3(pixel_dims.x, pixel_dims.y, 1.0f));
}

void TextUiElement::Update() {
}

} // namespace renderer