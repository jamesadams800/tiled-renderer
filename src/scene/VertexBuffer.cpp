#include "VertexBuffer.h"

#include <GL/glew.h>

#include <assert.h>
#include <string>
#include <iostream>

namespace renderer {

std::shared_ptr<VertexBuffer> VertexBuffer::Create() {
  return CreateSharedObject<VertexBuffer>();
}

VertexBuffer::VertexBuffer()
  : vertex_buffer_handle_{ 0 },
    vertex_count_{0},
    vertex_array_{},
    attribute_format_{} {
  this->CreateGpuObject();
}

VertexBuffer::~VertexBuffer() {
  this->DeleteGpuObject();
}

VertexBuffer::VertexBuffer(const VertexBuffer& other)
  : vertex_buffer_handle_{ 0 },
    vertex_count_{ other.vertex_count_ },
    vertex_array_{other.vertex_array_ },
    attribute_format_{ other.attribute_format_ } {
  this->CreateGpuObject();
}

VertexBuffer::VertexBuffer(VertexBuffer&& other)
  : vertex_buffer_handle_{ other.vertex_buffer_handle_ },
    vertex_count_{ other.vertex_count_ },
    vertex_array_{ std::move(other.vertex_array_) },
    attribute_format_{ std::move(other.attribute_format_) } {
}

VertexBuffer& VertexBuffer::operator=(VertexBuffer other) {
  swap(*this, other);
  return *this;
}

VertexBuffer& VertexBuffer::operator=(const VertexBuffer && other) {
  vertex_buffer_handle_ = std::move(other.vertex_buffer_handle_);
  vertex_array_ = std::move(other.vertex_array_);
  attribute_format_ = std::move(other.attribute_format_);
  return *this;
}

void VertexBuffer::UpdateVertexArray(VertexAttribute attrib, const std::vector<float>& data) {
  auto format = this->GetAttributeFormat(attrib);
  uint32_t attribute_data_size = format.vertex_size * vertex_count_;
  assert(data.size() == attribute_data_size);
  uint32_t i = 0;
  for (uint32_t j = format.vertex_offset; j < format.vertex_offset + attribute_data_size; j++) {
    this->vertex_array_[j] = data[i];
    i++;
  }
  this->UpdateGpuObject();
}

void VertexBuffer::AddVertexAttributeData(VertexAttribute attrib,
                                          uint32_t vertex_size,
                                          uint32_t stride,
                                          const std::vector<float>& data) {
  assert(!this->IsAttributePresent(attrib));
  size_t vertex_count = static_cast<uint32_t>(data.size() / vertex_size);
  if (vertex_count_ == 0) {
    vertex_count_ = vertex_count;
  }
  else {
    assert(vertex_count_ == vertex_count);
  }
  uint32_t offset = static_cast<uint32_t>(this->vertex_array_.size());
  VertexAttributeFormat new_attrib_format = { offset,
                                              vertex_size, 
                                              stride };
  this->attribute_format_.insert(std::make_pair(attrib, new_attrib_format));
  this->vertex_array_.insert(std::end(this->vertex_array_),
                             std::begin(data), std::end(data));
  this->UpdateGpuObject();
}

bool VertexBuffer::IsAttributePresent(VertexAttribute attribute_type) const {
  auto entry = this->attribute_format_.find(attribute_type);
  if (entry != this->attribute_format_.end()) {
    return true;
  }
  return false;
}

VertexBuffer::VertexAttributeFormat VertexBuffer::GetAttributeFormat(VertexAttribute attribute_type) const {
  auto entry = this->attribute_format_.find(attribute_type);
  if (entry != this->attribute_format_.end()) {
    return entry->second;
  }
  throw std::runtime_error("VertexBuffer: Unable to retrieve attribute format for buffer attribute type: ");
}

VertexBuffer::Proxy VertexBuffer::operator[](VertexAttribute attribute) {
  auto format = this->GetAttributeFormat(attribute);
  return Proxy(*this, format);
}

VertexBuffer::ConstProxy VertexBuffer::operator[](VertexAttribute attribute) const {
  auto format = this->GetAttributeFormat(attribute);
  return ConstProxy(*this, format);
}

void swap(VertexBuffer& first, VertexBuffer& second) {
  using std::swap;
  swap(first.vertex_buffer_handle_, second.vertex_buffer_handle_);
  swap(first.attribute_format_, second.attribute_format_);
  swap(first.vertex_array_, second.vertex_array_);
}


std::shared_ptr<VertexBuffer> VertexBuffer::Clone() {
  return std::shared_ptr<VertexBuffer>(new VertexBuffer(*this), BaseDeleter());
}

size_t VertexBuffer::Size() const {
  return vertex_count_;
}

const std::map<VertexBuffer::VertexAttribute, VertexBuffer::VertexAttributeFormat>&
VertexBuffer::GetVertexAttributeMap() const {
  return attribute_format_;
}

void VertexBuffer::Bind() const {
  glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer_handle_);
}

void VertexBuffer::CreateGpuObject() {
  if (vertex_buffer_handle_ == 0) {
    glGenBuffers(1, &vertex_buffer_handle_);
    if (!vertex_array_.empty()) {
      this->UpdateGpuObject();
    }
  }
}

void VertexBuffer::UpdateGpuObject() {
  glBindVertexArray(0);
  glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer_handle_);
  glBufferData(GL_ARRAY_BUFFER, (vertex_array_.size() * sizeof(float)),
               vertex_array_.data(), GL_STATIC_DRAW);
  glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void VertexBuffer::DeleteGpuObject() {
  glDeleteBuffers(1, &vertex_buffer_handle_);
  vertex_buffer_handle_ = 0;
}

} // namespace renderer
