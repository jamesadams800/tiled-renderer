#include "AABB.h"

#include <numeric>
#include <ostream>

#include "VertexBuffer.h"

namespace renderer {

std::shared_ptr<AABB> AABB::Create() {
  return CreateSharedObject<AABB>();
}

AABB::AABB()
  : AABB(glm::vec3(std::numeric_limits<float>::infinity()),
         glm::vec3(-std::numeric_limits<float>::infinity())) {
}

AABB::AABB(const glm::vec3& min, const glm::vec3& max)
  : min_{ min },
    max_{ max }{
}

AABB::~AABB() {
}

AABB::AABB(const AABB& other)
  : AABB(other.min_, other.max_) {
}

AABB& AABB::operator=(const AABB& other) {
  min_ = other.min_;
  max_ = other.max_;
  return *this;
}

void AABB::Set(const glm::vec3& min, const glm::vec3& max) {
  min_ = min;
  max_ = max;
}

void AABB::Merge(const AABB& other) {
  min_ = glm::min(min_, other.min_);
  max_ = glm::max(max_, other.max_);
}

const glm::vec3 & AABB::GetMin() const {
  return min_;
}

const glm::vec3 & AABB::GetMax() const {
  return max_;
}

void AABB::SetTransform(const glm::mat4 & transform) {
  glm::vec4 vertices[] = { {min_.x, min_.y, min_.z, 1.0f},
                           {min_.x, min_.y, max_.z, 1.0f},
                           {min_.x, max_.y, min_.z, 1.0f},
                           {max_.x, min_.y, min_.z, 1.0f},
                           {min_.x, max_.y, max_.z, 1.0f},
                           {max_.x, min_.y, max_.z, 1.0f},
                           {max_.x, max_.y, min_.z, 1.0f},
                           {max_.x, max_.y, max_.z, 1.0f} };
  min_ = glm::vec3(std::numeric_limits<float>::infinity());
  max_ = glm::vec3(-std::numeric_limits<float>::infinity());
  for (auto& vertex : vertices) {
    glm::vec4 transformed_vertex = transform * vertex;
    min_ = glm::min(transformed_vertex, glm::vec4(min_,1.0f));
    max_ = glm::max(transformed_vertex, glm::vec4(max_, 1.0f));
  }
}

std::pair<glm::vec3, glm::vec3> AABB::GetBounds() const {
  return std::make_pair(min_, max_);
}

void AABB::FitBox(const VertexBuffer& vertex_buffer) {
  auto positions = vertex_buffer[VertexBuffer::POSITION];
  for (uint32_t i = 0; i < vertex_buffer.Size(); i++) {
    const float& x = positions[i][0];
    const float& y = positions[i][1];
    const float& z = positions[i][2];

    min_.x = glm::min(x, min_.x);
    min_.y = glm::min(y, min_.y);
    min_.z = glm::min(z, min_.z);

    max_.x = glm::max(x, max_.x);
    max_.y = glm::max(y, max_.y);
    max_.z = glm::max(z, max_.z);
  }
}

std::ostream& operator<<(std::ostream& os, const AABB& aabb) {
  os << "AABB{ Min[" << aabb.min_.x << ", " << aabb.min_.y << ", " << aabb.min_.z << "]" <<
        " Max[" << aabb.max_.x << ", " << aabb.max_.y << ", " << aabb.max_.z << "] }";
  return os;
}

AABB operator*(const AABB& aabb, const glm::mat4& transform) {
  AABB result(aabb);
  result.SetTransform(transform);
  return result;
}

} // namespace renderer
