#include "Mesh.h"

#include <string>

#include "Renderer.h"
#include "RenderVisitors.h"

namespace renderer {

uint32_t Mesh::mesh_id_counter_ = 0;

std::shared_ptr<Mesh> Mesh::Create() {
  return CreateSharedObject<Mesh>();
}

Mesh::Mesh()
  : Mesh( nullptr, nullptr ) {
}

Mesh::Mesh(std::unique_ptr<VertexBuffer> vb)
  : Mesh(std::move(vb), nullptr) {
}

Mesh::Mesh(std::unique_ptr<VertexBuffer> vb, std::unique_ptr<IndexBuffer> ib) 
  : name_{ std::to_string(mesh_id_counter_++) },
    geometry_{std::move(vb), std::move(ib),  Geometry::TRIANGLES } {
}

void Mesh::SetName(const std::string & name) {
  name_ = name;
}

std::string Mesh::GetName() const {
  return name_;
}

const Geometry & Mesh::GetGeometry() const {
  return geometry_;
}

Geometry & Mesh::GetGeometry() {
  return geometry_;
}

const AABB & Mesh::GetAABB() const {
  return geometry_.GetAABB();
}

void Mesh::Accept(RenderVisitor& visitor) {
  visitor.Visit(*this);
}

} // namespace renderer