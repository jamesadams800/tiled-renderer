#include "Ray.h"
#include "Mesh.h"

namespace renderer {

std::shared_ptr<Ray> Ray::Create() {
  return CreateSharedObject<Ray>();
}

Ray::Ray()
  : origin_{},
    direction_{} {
}

Ray::Ray(const glm::vec3& origin, const glm::vec3& direction)
  : origin_{ origin },
  direction_{ direction } {
}

void Ray::SetOrigin(const glm::vec3 & origin) {
  origin_ = origin;
}

void Ray::SetDirection(const glm::vec3 & direction) {
  direction_ = direction;
}

const glm::vec3 & Ray::GetOrigin() const {
  return origin_;
}

const glm::vec3 & Ray::GetDirection() const {
  return direction_;
}

bool Ray::Intersect(const Mesh& mesh, float& t, glm::vec3& barycentric) {
  uint32_t closest_index;
  return this->Intersect(mesh, t, barycentric, closest_index);
}

bool Ray::Intersect(const Mesh& mesh, float& t, glm::vec3& barycentric, uint32_t& closest_index) {
  assert(mesh.GetGeometry().IsIndexed()); // For now lets just support indexing...
  auto vertices = mesh.GetGeometry().GetVertexBuffer()[VertexBuffer::POSITION];
  auto index_array = mesh.GetGeometry().GetIndexBuffer().GetIndexArray();

  for (uint32_t i = 0; i < index_array.size() - 1; i += 3) {
    float x = vertices[index_array[i]][0];
    float y = vertices[index_array[i]][1];
    float z = vertices[index_array[i]][2];
    glm::vec4 v0(x, y, z, 1.0f);
    v0 = mesh.GetWorldTransform() * v0;

    x = vertices[index_array[i + 1]][0];
    y = vertices[index_array[i + 1]][1];
    z = vertices[index_array[i + 1]][2];
    glm::vec4 v1(x, y, z, 1.0f);
    v1 = mesh.GetWorldTransform() * v1;

    x = vertices[index_array[i + 2]][0];
    y = vertices[index_array[i + 2]][1];
    z = vertices[index_array[i + 2]][2];
    glm::vec4 v2(x, y, z, 1.0f);
    v2 = mesh.GetWorldTransform() * v2;

    if (this->Intersect(v0, v1, v2, t, barycentric)) {
      if (barycentric.x > barycentric.y && barycentric.x > barycentric.z) {
        closest_index = index_array[i];
      }
      else if (barycentric.y > barycentric.x && barycentric.y > barycentric.z) {
        closest_index = index_array[i + 1];
      }
      else {
        closest_index = index_array[i + 2];
      }
      return true;
    }
  }

  return false;
}


// Implementation of triangle intersection follows algorithm detailed in:
// M�ller, T. and Trumbore, B., 2005, July.Fast,
// minimum storage ray / triangle intersection.
// In ACM SIGGRAPH 2005 Courses(p. 7).ACM.
bool Ray::Intersect(const glm::vec3& v0, const glm::vec3& v1,
  const glm::vec3& v2, float& t_out,
  glm::vec3& barycentric) {
  glm::vec3 edge1 = v1 - v0;
  glm::vec3 edge2 = v2 - v0;
  glm::vec3 pvec = glm::cross(this->GetDirection(), edge2);

  float det = glm::dot(edge1, pvec);
  if (det < 0.000001) {
    return false;
  }

  glm::vec3 tvec = this->GetOrigin() - v0;
  float u = glm::dot(tvec, pvec);
  if (u < 0 || u > det) {
    return false;
  }

  glm::vec3 qvec = glm::cross(tvec, edge1);
  float v = glm::dot(this->GetDirection(), qvec);
  if (v < 0 || u + v > det) {
    return false;
  }
  // t = intersection point in terms of O + tD where O is ray origin and D is ray direction.
  float t = glm::dot(edge2, qvec);
  float inv_det = 1.0f / det;
  t_out = t * inv_det;
  // u and v are barycentric coordinates of intersection on triangle.
  u *= inv_det;
  v *= inv_det;
  barycentric = { 1 - u - v, u, v };
  return true;
}

} // namespace renderer
