#include "Object.h"

namespace glapp {

void Object::BaseDeleter::operator()(Object * base) {
  delete base;
}

} // namespace glapp 