#include "Camera.h"

#include <iostream>

#define GLM_ENABLE_EXPERIMENTAL
#include<glm/gtx/string_cast.hpp>

namespace renderer {
// Up and at are built assuming that camera is initialised to { 0, 0, 0 }
// (Which it should be because an entities transformation is initialised as
// a identity matrix.
Camera::Camera()
  : Camera(1.0f) {
}

Camera::Camera(float aspect_ratio)
  : aspect_ratio_{aspect_ratio},
    at_{ glm::vec3(0.0, 0.0, -1.0) },
    up_{ glm::vec3(0.0, 1.0, 0.0) },
    eye_{ glm::vec3(0.0f,0.0f,0.0f) },
    near_plane_{ 0.1f },
    far_plane_{ 100.0f },
    camera_space_frustrum_corners_{} {
}

// and the rest....

const glm::mat4 & Camera::GetViewMatrix() const {
  return view_matrix_;
}

const glm::mat4 & Camera::GetInvViewMatrix() const {
  return inv_view_matrix_;
}

const glm::mat4 & Camera::GetProjectionMatrix() const {
  return projection_matrix_;
}

const glm::mat4 & Camera::GetInvProjectionMatrix() const {
  return inv_projection_matrix_;
}

glm::vec4 Camera::GetPosition() const {
  return this->GetWorldTransform() * glm::vec4(eye_, 1.0f);
}

const glm::vec3 & Camera::GetAtVec() const {
  return at_;
}

const glm::vec3 & Camera::GetUpVec() const {
  return up_;
}

void Camera::Resize(uint32_t x_res, uint32_t y_res) {
  aspect_ratio_ = static_cast<float>(x_res) / static_cast<float>(y_res);
  this->CalculateProjectionMatrix();
}

float Camera::GetAspectRatio() const {
  return aspect_ratio_;
}

void Camera::SetAspectRatio(float aspect) {
  aspect_ratio_ = aspect;
}

float Camera::GetNearPlane() const {
  return near_plane_;
}

void Camera::SetNearPlane(float near_plane_distance) {
  near_plane_ = near_plane_distance;
  this->CalculateProjectionMatrix();
}

float Camera::GetFarPlane() const {
  return far_plane_;
}

void Camera::SetFarPlane(float far_plane_distance) {
  far_plane_ = far_plane_distance;
  this->CalculateProjectionMatrix();
}

void Camera::LookAt(float x, float y, float z) {
  this->LookAt({ x,y,z });
}

void Camera::LookAt(const glm::vec3 & at_vec) {
  at_ = at_vec;
  this->CalculateViewMatrix();
}

const std::array<glm::vec4, 8>& Camera::GetFrustrumCorners() const {
  return camera_space_frustrum_corners_;
}

void Camera::CalculateViewMatrix() const {
  // Truncate vec4 into vec3, not worried about w
  const glm::vec3 position = this->GetWorldTransform() * glm::vec4(eye_, 1.0f);
  view_matrix_ = glm::lookAt(position, position + at_, up_);
  inv_view_matrix_ = glm::inverse(view_matrix_);
}

Ray Camera::CreateCameraRay(float xcoord, float ycoord) const {
  glm::vec4 ray_origin = this->GetWorldTransform() * glm::vec4(eye_, 1.0f);
  // Calculate ray direction in clip space.
  glm::vec4 ray_direction_clip = { (2 * xcoord) - 1.0f, // x
                                  1.0f - (2 * ycoord),  // y
                                  1.0f,  // z
                                  0.0f };// w
  glm::vec4 ray_eye = inv_projection_matrix_ * ray_direction_clip;
  glm::vec3 world_ray_direction = inv_view_matrix_ * glm::vec4{ ray_eye.x, ray_eye.y, -1, 0 };
  return Ray(ray_origin, glm::normalize(world_ray_direction));
}

} // namespace renderer
