#include "Material.h"

#include <iostream>
#include <string>

#include <GL/glew.h>

#include "Texture.h"
#include "ShaderBuffer.h"

namespace renderer {

uint32_t Material::uuid_counter_ = 0;

std::shared_ptr<Material> Material::Create() {
  return CreateSharedObject<Material>();
}

Material::Material()
  : layer_presence_{ 0 },
    textures_ {0},
    material_constants_{new UniformBuffer<ConstantComponents>()},
    uuid_{ uuid_counter_++ } {
}

void Material::SetName(const std::string & name) {
  material_name_ = name;
}

void Material::Bind() const {
  uint32_t texture_unit_inc = 0;
  for (auto& texture : textures_) {
    if (texture) {
      glActiveTexture(GL_TEXTURE0 + texture_unit_inc);
      texture->Bind();
    }
    texture_unit_inc++;
  }
}

const UniformBuffer<Material::ConstantComponents>& 
Material::GetConstantsUniform() const {
  return *material_constants_;
}

const Texture<uint8_t>* Material::GetTexture(LayerIdentifier type) {
  return textures_[type].get();
}

void Material::SetTexture(std::shared_ptr<Texture<uint8_t>> texture, LayerIdentifier id) {
  if (id >= MAX_TEXTURES) {
    throw std::runtime_error("Material::SetTexture : Attempting to set texture ID:"
                             + std::to_string(id) + " when max supported textures is "
                             + std::to_string(MAX_TEXTURES));
  }
  if (texture == nullptr) {
    std::cerr << "Material::SetTexture : Attempting to set texture ID:" << id
              << " with nullptr texture." << std::endl;
    return;
  }
  if (textures_[id] != nullptr) {
    std::cerr << "Material::SetTexture : Attempting to set texture ID:" << id
              << " when texture is already set. Replacing existing texture" << std::endl;
  }
  layer_presence_ |= (1 << id);
  textures_[id] = texture;
}

void Material::SetEmissive(const glm::vec3 & color) {
  (*material_constants_)->emissive_color = glm::vec4(color, 0.0f);
  material_constants_->UpdateBuffer();
}

void Material::SetDiffuse(const glm::vec3 & color) {
  (*material_constants_)->diffuse_color = glm::vec4(color, 0.0f);
  material_constants_->UpdateBuffer();
}

void Material::SetSpecular(const glm::vec3 & color) {
  (*material_constants_)->specular_color = glm::vec4(color, 0.0f);
  material_constants_->UpdateBuffer();
}

void Material::SetShininess(float shininess) {
  (*material_constants_)->shininess = shininess;
  material_constants_->UpdateBuffer();
}

} // namespace renderer