#include "Renderable.h"

namespace renderer {

Renderable::Renderable()
  : visible_{ true } {
}

void Renderable::SetScale(float x, float y, float z) {
  transform_matrix_ = glm::scale(transform_matrix_, { x, y, z });
}

void Renderable::SetScale(const glm::vec3 & scale) {
  transform_matrix_ = glm::scale(transform_matrix_, scale);
}

bool Renderable::IsVisible() const {
  return visible_;
}

void Renderable::SetVisible(bool visible) {
  visible_ = visible;
}

void Renderable::SetMaterial(std::shared_ptr<Material> material) {
  material_ = material;
}

const Material * Renderable::GetMaterial() const {
  return material_.get();
}

} // namespace renderer
