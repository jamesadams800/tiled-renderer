#include "OrthographicCamera.h"

namespace renderer {

std::shared_ptr<OrthographicCamera> OrthographicCamera::Create() {
  return CreateSharedObject<OrthographicCamera>();
}

OrthographicCamera::OrthographicCamera()
  : left_{},
    right_{},
    top_{},
    bottom_{} {
  this->OrthographicCamera::CalculateProjectionMatrix();
  this->CalculateViewMatrix();
}

void OrthographicCamera::SetLeft(float left) {
  left_ = left;
}

void OrthographicCamera::SetRight(float right) {
  right_ = right;
}

void OrthographicCamera::SetTop(float top) {
  top_ = top;
}

void OrthographicCamera::SetBottom(float bottom) {
  bottom_ = bottom;
}

void OrthographicCamera::CalculateProjectionMatrix() {
  projection_matrix_ = glm::ortho(left_, right_,
                                  bottom_, top_,
                                  this->GetNearPlane(), this->GetFarPlane());
  inv_projection_matrix_ = glm::inverse(projection_matrix_);
}

std::array<glm::vec4, 8> OrthographicCamera::GetSplitFrustrum(float, float) const {
  //TODO: THIS
  return std::array<glm::vec4, 8>();
}

std::array<glm::vec4, 8> OrthographicCamera::GetWorldSpaceSplitFrustrum(float, float) const {
  //TODO: THIS
  return std::array<glm::vec4, 8>();
}

void OrthographicCamera::CalculateFrustrum() {
  //TODO: THIS
}

} // namespace renderer
