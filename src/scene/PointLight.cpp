#include "PointLight.h"

#include "ShaderData.h"

namespace renderer {

std::shared_ptr<PointLight> PointLight::Create(const glm::vec3 & color,
                                               float intensity) {
  return CreateSharedObject<PointLight>(color, intensity);
}

PointLight::PointLight(const glm::vec3 & color, float intensity)
  : Light(color, intensity),
    linear_attenuation_{ 1.0f },
    quadratic_attenuation_{ 1.0f }, 
    light_radius_{0.0f} {
  this->CalculateRadius();
}

void PointLight::SetLinearAttenuation(float linear) {
  dirty_ = true;
  linear_attenuation_ = linear;
  this->CalculateRadius();
}

void PointLight::SetQuadraticAttenuation(float quadratic) {
  dirty_ = true;
  quadratic_attenuation_ = quadratic;
  this->CalculateRadius();
}

float PointLight::GetLinearAttenuation() const {
  return linear_attenuation_;
}

float PointLight::GetQuadraticAttenuation() const {
  return quadratic_attenuation_;
}

float PointLight::GetRadius() const {
  return light_radius_;
}

void PointLight::SetIntensity(float intensity) {
  dirty_ = true;
  this->Light::SetIntensity(intensity);
  this->CalculateRadius();
}

void PointLight::SetColor(const glm::vec3 & color) {
  dirty_ = true;
  this->Light::SetColor(color);
  this->CalculateRadius();
}

void PointLight::WriteShaderData(ShaderData& shader_data) const {
  dirty_ = false;
  shader_data.sphere_model = this->GetWorldTransform();
  shader_data.color = glm::vec4(this->GetColor() * this->GetIntensity(), 0.0f);
  shader_data.position = this->GetWorldTransform() * glm::vec4(0.0f, 0.0f, 0.0f, 1.0f);
  shader_data.radius = this->GetRadius();
  shader_data.linear_attenuation = this->GetLinearAttenuation();
  shader_data.quadratic_attenuation = this->GetQuadraticAttenuation();
}

void PointLight::CalculateRadius() {
  float light_intensity = RgbToLuminosity(this->GetColor() * this->GetIntensity());
  float a = this->GetQuadraticAttenuation();
  float b = this->GetLinearAttenuation();
  float c = 1.0f - light_intensity * (1.0f / MINIMUM_LIGHT_INTENSITY);
  float distance = (-b + sqrtf( (b * b) - 4 * a * c)) / (2 * a);
  light_radius_ = distance;
}

} // namespace renderer