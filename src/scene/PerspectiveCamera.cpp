#include "PerspectiveCamera.h"

namespace renderer {

std::shared_ptr<PerspectiveCamera> PerspectiveCamera::Create() {
  return CreateSharedObject<PerspectiveCamera>();
}

PerspectiveCamera::PerspectiveCamera()
  : fov_{ 90.0f },
    vertical_fov_tan_{ 0.0f },
    horizontal_fov_tan_{ 0.0f }{
  this->PerspectiveCamera::CalculateProjectionMatrix();
  this->CalculateViewMatrix();
}

void PerspectiveCamera::SetVerticalFov(float fov) {
  fov_ = fov;
}

float PerspectiveCamera::GetVerticalFov() const {
  return fov_;
}

void PerspectiveCamera::CalculateProjectionMatrix() {
  projection_matrix_ = glm::perspective(glm::radians(fov_),
                                        this->GetAspectRatio(),
                                        this->GetNearPlane(),
                                        this->GetFarPlane());
  inv_projection_matrix_ = glm::inverse(projection_matrix_);
  vertical_fov_tan_ = std::tanf(glm::radians(fov_ / 2.0f));
  horizontal_fov_tan_ = std::tanf(glm::radians( ( fov_ * this->GetAspectRatio() ) / 2.0f));
}

void PerspectiveCamera::CalculateFrustrum() {
  float x1 = this->GetNearPlane() * horizontal_fov_tan_;
  float x2 = this->GetFarPlane() * horizontal_fov_tan_;
  float y1 = this->GetNearPlane() * vertical_fov_tan_;
  float y2 = this->GetFarPlane() * vertical_fov_tan_;

  camera_space_frustrum_corners_[0] = glm::vec4(-x1, -y1, this->GetNearPlane(), 1.0f);
  camera_space_frustrum_corners_[1] = glm::vec4(x1, -y1, this->GetNearPlane(), 1.0f);
  camera_space_frustrum_corners_[2] = glm::vec4(x1, y1, this->GetNearPlane(), 1.0f);
  camera_space_frustrum_corners_[3] = glm::vec4(-x1, y1, this->GetNearPlane(), 1.0f);
  camera_space_frustrum_corners_[4] = glm::vec4(-x2, -y2, this->GetFarPlane(), 1.0f);
  camera_space_frustrum_corners_[5] = glm::vec4(x2, -y2, this->GetFarPlane(), 1.0f);
  camera_space_frustrum_corners_[6] = glm::vec4(x2, y2, this->GetFarPlane(), 1.0f);
  camera_space_frustrum_corners_[7] = glm::vec4(-x2, y2, this->GetFarPlane(), 1.0f);
}

std::array<glm::vec4, 8> PerspectiveCamera::GetSplitFrustrum(float near, float far) const {
  std::array<glm::vec4, 8> corners;

  float x1 = near * horizontal_fov_tan_;
  float x2 = far * horizontal_fov_tan_;
  float y1 = near * vertical_fov_tan_;
  float y2 = far * vertical_fov_tan_;

  corners[0] = glm::vec4(-x1, -y1, near, 1.0f);
  corners[1] = glm::vec4(x1, -y1, near, 1.0f);
  corners[2] = glm::vec4(x1, y1, near, 1.0f);
  corners[3] = glm::vec4(-x1, y1, near, 1.0f);
  corners[4] = glm::vec4(-x2, -y2, far, 1.0f);
  corners[5] = glm::vec4(x2, -y2, far, 1.0f);
  corners[6] = glm::vec4(x2, y2, far, 1.0f);
  corners[7] = glm::vec4(-x2, y2, far, 1.0f);

  return corners;
}

std::array<glm::vec4, 8> PerspectiveCamera::GetWorldSpaceSplitFrustrum(float near, float far) const {
  std::array<glm::vec4, 8> corners = this->GetSplitFrustrum(near, far);
  for (auto& point : corners) {
    point = inv_view_matrix_ * point;
  }
  return corners;
}


} // namespace renderer
