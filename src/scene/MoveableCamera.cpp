#include "MoveableCamera.h"

namespace renderer {

std::shared_ptr<MoveableCamera> MoveableCamera::Create() {
  return CreateSharedObject<MoveableCamera>();
}

MoveableCamera::MoveableCamera()
  : camera_move_speed_{ 1.0f },
    cursor_dx_{ 0.0f },
    cursor_dy_{ 0.0f },
    move_camera_{ true } {
}

bool MoveableCamera::IsLocked() const {
  return !move_camera_;
}

float MoveableCamera::GetMoveSpeed() const {
  return camera_move_speed_;
}

void MoveableCamera::SetMoveSpeed(float camera_speed) {
  camera_move_speed_ = camera_speed;
}

void MoveableCamera::KeyEventPerformed(Key key, KeyEvent event) {
  if (event == KEY_PRESSED) {
    switch (key) {
      case KEY_W: {
        this->eye_ += at_ * camera_move_speed_;
        break;
      }
      case KEY_A: {
        this->eye_ += glm::normalize(glm::cross(up_, at_)) * camera_move_speed_;
        break;
      }
      case KEY_S: {
        this->eye_ -= at_ * camera_move_speed_;
        break;
      }
      case KEY_D: {
        this->eye_ -= glm::normalize(glm::cross(up_, at_)) * camera_move_speed_;
        break;
      }
      case KEY_SPACE: {
        move_camera_ = !move_camera_;
        break;
      }
    }
  }
}

void MoveableCamera::MouseMoved(double dx, double dy) {
  float frame_dx = 0.5f - dx;
  float frame_dy = 0.5f - dy;

  if (move_camera_ && (frame_dx || frame_dy)) {
    cursor_dx_ += frame_dx;
    cursor_dy_ += frame_dy;

    if (cursor_dy_ > (3.1415f / 2.0f) - glm::epsilon<float>()) {
      cursor_dy_ = (3.1415f / 2.0f) - glm::epsilon<float>();
    }
    if (cursor_dy_ < -((3.1415f / 2.0f) - glm::epsilon<float>())) {
      cursor_dy_ = -(3.1415f / 2.0f) + glm::epsilon<float>();
    }

    float x = cosf(cursor_dx_) * cosf(cursor_dy_);
    float y = sinf(cursor_dy_);
    float z = cosf(cursor_dy_) * sinf(cursor_dx_);

    at_ = glm::normalize(glm::vec3(x, y, -z));
  }
}

void MoveableCamera::MousePressed(double, double, MouseButton, MouseEvent) {
}

} // namespace renderer
