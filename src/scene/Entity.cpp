#include "Entity.h"

namespace renderer {

Entity::Entity()
  : transform_matrix_{ 1.0f } { // Transform matrix initialised to Identity matrix.
}

Entity::~Entity() {
}

void Entity::SetTransform(const glm::mat4 & transform) {
  transform_matrix_ = transform;
}

glm::vec3 Entity::GetTranslation() const {
  return glm::vec3(transform_matrix_[3][0],
                   transform_matrix_[3][1],
                   transform_matrix_[3][2]);
}

const glm::mat4 & Entity::GetWorldTransform() const {
  return transform_matrix_;
}

void Entity::AddTranslation(const glm::vec3 & vec) {
  transform_matrix_ = glm::translate(transform_matrix_, vec);
}

void Entity::SetTranslation(const glm::vec3 & vec) {
  transform_matrix_[3][0] = vec.x;
  transform_matrix_[3][1] = vec.y;
  transform_matrix_[3][2] = vec.z;
}

void Entity::SetTranslation(float x, float y, float z) {
  this->SetTranslation(glm::vec3(x, y, z));
}

void Entity::SetRotation(const glm::vec3 & axis, float rad) {
  transform_matrix_ = glm::rotate(transform_matrix_, rad, axis);
}

} // renderer
