#include "Light.h"

namespace renderer {

Light::Light(const glm::vec3 & color, float intensity)
  : dirty_{true},
    color_{ color },
    intensity_{ intensity } {
}

void Light::SetTransform(const glm::mat4 & transform) {
  dirty_ = true;
  this->Entity::SetTransform(transform);
}

void Light::SetTranslation(const glm::vec3 & vec) {
  dirty_ = true;
  this->Entity::SetTranslation(vec);
}

void Light::SetTranslation(float x, float y, float z) {
  dirty_ = true;
  this->Entity::SetTranslation(x, y, z);
}

void Light::SetRotation(const glm::vec3 & axis, float rad) {
  dirty_ = true;
  this->Entity::SetRotation(axis, rad);
}

void Light::SetIntensity(float intensity) {
  dirty_ = true;
  intensity_ = intensity;
}

float Light::GetIntensity() const {
  return intensity_;
}

void Light::SetColor(const glm::vec3 & color) {
  dirty_ = true;
  color_ = color;
}

} // namespace renderer
