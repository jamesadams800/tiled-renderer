#ifndef OGL_SCENEIMPL_H_
#define OGL_SCENEIMPL_H_

#include <vector>
#include <memory>
#include <iostream>
#include <unordered_map>

#include "Scene.h"
#include "Renderable.h"
#include "Light.h"
#include "DirectionalLight.h"
#include "CascadedShadowMap.h"
#include "PointLight.h"
#include "ShaderData.h"
#include "Camera.h"
#include "UserInterface.h"
#include "AABB.h"

namespace renderer {

class Scene::Impl {
public:
  static constexpr uint32_t MAX_DIRECTIONAL_LIGHTS = 5;
  Impl();

  // Called once a frame, updates all uniform buffers and shader storage objects.
  void UpdateBuffers();

  void SetCamera(std::shared_ptr<Camera> camera) {
    default_camera_ = camera;
  }

  const std::shared_ptr<Camera> GetCamera() const {
    return default_camera_;
  }

  std::shared_ptr<Camera> GetCamera() {
    return default_camera_;
  }

  void AddRenderable(std::shared_ptr<Renderable> renderable);
  const std::vector<std::shared_ptr<Renderable>>& GetRenderables() const {
    return renderables_;
  }

  void AddLight(std::shared_ptr<DirectionalLight> light) {
    assert(MAX_DIRECTIONAL_LIGHTS > directional_lights_.size());
    directional_lights_.push_back(light);
    lights_.push_back(light);
  }

  void AddLight(std::shared_ptr<PointLight> light) {
    point_lights_.push_back(light);
    lights_.push_back(light);
  }

  const std::vector<std::shared_ptr<Light>>& GetLights() const {
    return lights_;
  }

  std::vector<std::shared_ptr<Light>>& GetLights() {
    return lights_;
  }

  const std::vector<std::shared_ptr<DirectionalLight>>& GetDirectionalLights() const {
    return directional_lights_;
  }

  const std::vector<std::shared_ptr<PointLight>>& GetPointLights() const {
    return point_lights_;
  }

  const StorageBuffer<DirectionalLight::ShaderData[]>& GetDirectLightBuffer() const {
    return directional_lights_buffer_;
  }

  const StorageBuffer<PointLight::ShaderData[]>& GetPointLightBuffer() const {
    return point_lights_buffer_;
  }

  const UniformBuffer<Std140Array<CascadedShadowMap::ShaderData, MAX_DIRECTIONAL_LIGHTS>>&
    GetCascadeData() const {
    return cascade_data_;
  }

  void AddTexture(const std::string& string, std::shared_ptr<Texture<uint8_t>> texture);
  void AddMaterial(uint64_t material_index, std::shared_ptr<Material> material);

  std::shared_ptr<Material> GetMaterial(uint64_t id);
  std::shared_ptr<Texture<uint8_t>> GetTexture(const std::string& name);

  UserInterface& GetUi() {
    return ui_;
  }

  const UserInterface& GetUi() const {
    return ui_;
  }

  uint64_t GetNextMaterialId() {
    return material_id_counter_++;
  }

  const AABB& GetAABB() const {
    return aabb_;
  }
private:
  struct RenderableSorter {
    bool operator()(const std::shared_ptr<Renderable>& renderable1,
                    const std::shared_ptr<Renderable>& renderable2);
  } sorter;

  std::shared_ptr<Camera> default_camera_;
  std::vector<std::shared_ptr<Renderable>> renderables_;
  std::vector<std::shared_ptr<DirectionalLight>> directional_lights_;
  std::vector<std::shared_ptr<PointLight>> point_lights_;
  std::vector<std::shared_ptr<Light>> lights_;

  // Light Shader Data.
  StorageBuffer<DirectionalLight::ShaderData[]> directional_lights_buffer_;
  StorageBuffer<PointLight::ShaderData[]> point_lights_buffer_;
  // Cascaded Shadow map Shader Data
  UniformBuffer<Std140Array<CascadedShadowMap::ShaderData, MAX_DIRECTIONAL_LIGHTS>> cascade_data_;

  // Scene Material Data.
  uint64_t material_id_counter_;
  std::unordered_map< uint64_t, std::shared_ptr<Material> > material_library_;
  std::unordered_map< std::string, std::shared_ptr<Texture<uint8_t>> > texture_library_;
  // User interface....
  UserInterface ui_;
  // AABB containing all renderables in scene.
  AABB aabb_;
};

} // namespace renderer

#endif // OGL_SCENEIMPL_H_