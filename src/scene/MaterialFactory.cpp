#include "MaterialFactory.h"

#include "Texture.h"

namespace renderer {

std::shared_ptr<Material> MaterialFactory::BuildXorMaterial(const glm::vec3 & color, float shininess) {
  std::shared_ptr<Material> material = Material::Create();
  material->SetShininess(shininess);
  material->SetTexture(TextureLoader::LoadXorTexture(color), Material::ALBEDO_MAP_ID);
  return material;
}

std::shared_ptr<Material> MaterialFactory::BuildGridMaterial(const glm::vec3 &, float) {
  return nullptr;
}

} // namespace renderer 