#include "IndexBuffer.h"

#include <GL/glew.h>

namespace renderer {

std::shared_ptr<IndexBuffer> IndexBuffer::Create() {
  return CreateSharedObject<IndexBuffer>();
}
IndexBuffer::IndexBuffer()
  : index_buffer_handle_{ 0 },
    index_array_{} {
  this->CreateGpuObject();
}
IndexBuffer::IndexBuffer(const std::vector<uint32_t>& data)
  : index_buffer_handle_{ 0 },
    index_array_ { data } {
  this->CreateGpuObject();
  this->UpdateGpuObject();
}

IndexBuffer::~IndexBuffer() {
  this->DeleteGpuObject();
}

IndexBuffer::IndexBuffer(const IndexBuffer& buffer)
  : index_buffer_handle_{ buffer.index_buffer_handle_ },
    index_array_{ buffer.index_array_ } {
  this->CreateGpuObject();
}

IndexBuffer & IndexBuffer::operator=(IndexBuffer buffer) {
  swap(*this, buffer);
  return *this;
}


IndexBuffer::IndexBuffer(const IndexBuffer&& buffer)
  : index_buffer_handle_{ std::move(buffer.index_buffer_handle_) },
    index_array_{ std::move(buffer.index_array_) } {
}

IndexBuffer & IndexBuffer::operator=(const IndexBuffer&& buffer) {
  index_buffer_handle_ = std::move(buffer.index_buffer_handle_);
  index_array_ = std::move(buffer.index_array_);
  return *this;
}

void IndexBuffer::AddIndexData(const std::vector<uint32_t>& data) {
  index_array_.insert(std::end(index_array_), std::begin(data), std::end(data));
  this->UpdateGpuObject();
}

bool IndexBuffer::IsIndexPresent(uint32_t index_test) const {
  for (uint32_t index : index_array_) {
    if (index == index_test) {
      return true;
    }
  }
  return false;
}

void IndexBuffer::AddIndex(uint32_t index) {
  if (!this->IsIndexPresent(index)) {
    index_array_.push_back(index);
    this->UpdateGpuObject();
  }
}

void IndexBuffer::RemoveIndex(uint32_t index) {
  for (auto it = std::begin(index_array_); it != std::end(index_array_); it++) {
    if (*it == index) {
      it = index_array_.erase(it);
      this->UpdateGpuObject();
      break;
    }
  }
}

void IndexBuffer::ClearIndexData() {
  index_array_.clear();
}

void swap(IndexBuffer& first, IndexBuffer& second) {
  using std::swap;
  swap(first.index_buffer_handle_, second.index_buffer_handle_);
  swap(first.index_array_, second.index_array_);
}

std::shared_ptr<IndexBuffer> IndexBuffer::Clone() {
  return std::shared_ptr<IndexBuffer>(new IndexBuffer(*this), Object::BaseDeleter());
}

void IndexBuffer::Bind() const {
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, index_buffer_handle_);
}

void IndexBuffer::CreateGpuObject() {
  if (index_buffer_handle_ == 0) {
    glGenBuffers(1, &index_buffer_handle_);
    if (!this->GetIndexArray().empty()) {
      this->UpdateGpuObject();
    }
  }
}

void IndexBuffer::UpdateGpuObject() {
  glBindVertexArray(0);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, index_buffer_handle_);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, this->GetIndexArray().size() * sizeof(uint32_t),
    this->GetIndexArray().data(), GL_DYNAMIC_DRAW);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

void IndexBuffer::DeleteGpuObject() {
  glDeleteBuffers(1, &index_buffer_handle_);
}

} // namespace renderer
