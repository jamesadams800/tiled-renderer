#include "SceneImpl.h"

#include "PerspectiveCamera.h"

namespace renderer {

Scene::Impl::Impl()
  : default_camera_{ new PerspectiveCamera() },
    renderables_{},
    directional_lights_{},
    lights_{},
    material_id_counter_{ 0 },
    material_library_{},
    texture_library_{},
    ui_{} {
}

void Scene::Impl::UpdateBuffers() {
  default_camera_->CalculateViewMatrix();
  // Arrays resized so, for now we are going to have to recopy everything.
  if (directional_lights_.size() != directional_lights_buffer_.Size()) {
    directional_lights_buffer_.Resize(directional_lights_.size());
    for (uint32_t i = 0; i < directional_lights_.size(); i++) {
      directional_lights_[i]->WriteShaderData(directional_lights_buffer_[i]);
    }
    directional_lights_buffer_.UpdateBuffer();
  }
  else {
    for (uint32_t i = 0; i < directional_lights_.size(); i++) {
      if (directional_lights_[i]->PendingWrite()) {
        directional_lights_[i]->WriteShaderData(directional_lights_buffer_[i]);
        directional_lights_buffer_.UpdateBuffer(i);
      }
    }
  }

  if (point_lights_.size() != point_lights_buffer_.Size()) {
    point_lights_buffer_.Resize(point_lights_.size());
    for (uint32_t i = 0; i < point_lights_.size(); i++) {
      point_lights_[i]->WriteShaderData(point_lights_buffer_[i]);
    }
    point_lights_buffer_.UpdateBuffer();
  }
  else {
    for (uint32_t i = 0; i < point_lights_.size(); i++) {
      if (point_lights_[i]->PendingWrite()) {
        point_lights_[i]->WriteShaderData(point_lights_buffer_[i]);
        point_lights_buffer_.UpdateBuffer(i);
      }
    }
  }
  // Update Shadow maps.
  for (uint32_t i = 0; i < directional_lights_.size(); i++) {
    if (auto* shadow_map = directional_lights_[i]->GetShadowMap()) {
      shadow_map->CalculateCascadeMatrices();
      shadow_map->WriteShaderData((*cascade_data_)[i]);
    }
  }
  cascade_data_.UpdateBuffer();
}

void Scene::Impl::AddRenderable(std::shared_ptr<Renderable> renderable) {
  const Material* material = renderable->GetMaterial();
  if (material == nullptr) {
    throw std::runtime_error("Scene::AddRenderable : Attempting to add incomplete "
      "renderable with no material.");
  }
  aabb_.Merge(renderable->GetAABB());
  renderables_.push_back(renderable);
  std::sort(std::begin(renderables_), std::end(renderables_), sorter);
}

void Scene::Impl::AddTexture(const std::string & string, std::shared_ptr<Texture<uint8_t>> texture) {
  texture_library_.insert(std::make_pair(string, texture));
}

void Scene::Impl::AddMaterial(uint64_t material_index, std::shared_ptr<Material> material) {
  material_library_.insert(std::make_pair(material_index, material));
}

std::shared_ptr<Material> Scene::Impl::GetMaterial(uint64_t id) {
  const auto& material = material_library_.find(id);
  if (material != material_library_.end()) {
    return material->second;
  }
  return nullptr;
}

std::shared_ptr<Texture<uint8_t>> Scene::Impl::GetTexture(const std::string & name) {
  const auto& texture = texture_library_.find(name);
  if (texture != texture_library_.end()) {
    return texture->second;
  }
  return nullptr;
}

// Renderable sorter.  Used to make sure that all renderables are sorted by first material
// map presence and second by material uuid.  This is done to foremost limit shader switches
// and secondly to limit texture switches.
bool Scene::Impl::RenderableSorter::operator()(const std::shared_ptr<Renderable>& renderable1,
  const std::shared_ptr<Renderable>& renderable2) {
  if (renderable1->GetMaterial()->GetLayersPresent() >
    renderable2->GetMaterial()->GetLayersPresent()) {
    return false;
  }
  else if (renderable1->GetMaterial()->GetLayersPresent() <
    renderable2->GetMaterial()->GetLayersPresent()) {
    return true;
  }
  else {
    return renderable1->GetMaterial()->GetUuid() <
      renderable2->GetMaterial()->GetUuid();
  }
}

} // namespace renderer