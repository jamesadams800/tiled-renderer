#include "DirectionalLight.h"

#include <iostream>

#include "ShaderData.h"
#include "ShadowPass.h"
#include "CascadedShadowMap.h"

namespace renderer {

std::shared_ptr<DirectionalLight> DirectionalLight::Create(const glm::vec3& direction,
                                                           const glm::vec3& color,
                                                           float intensity) {
  return CreateSharedObject<DirectionalLight>(direction, color, intensity);
}
DirectionalLight::DirectionalLight(const glm::vec3& direction,
                                   const glm::vec3& color, float intensity)
  : shadow_map_{nullptr},
    direction_{direction}, 
    Light(color, intensity) {
}

DirectionalLight::DirectionalLight(const glm::vec3& color, float intensity)
  : DirectionalLight(glm::vec3(0.0f, -1.0f, 0.0f), color, intensity) {
}

void DirectionalLight::WriteShaderData(ShaderData& shader_data) const {
  dirty_ = false;
  shader_data.color = glm::vec4(this->GetColor() * this->GetIntensity(), 0.0f);
  shader_data.direction = glm::vec4(this->GetDirection(), 0.0f);
  shader_data.has_shadow = shadow_map_ != nullptr;
}

void DirectionalLight::AddShadowMap(const Camera& camera) {
  shadow_map_.reset(new CascadedShadowMap(camera));
  shadow_map_->UpdateLightDirection(direction_);
}

void DirectionalLight::SetDirection(const glm::vec3 & direction) {
  dirty_ = true;
  direction_ = direction;
  if (shadow_map_) {
    shadow_map_->UpdateLightDirection(direction);
  }
}

inline RENDER_API glm::vec3 DirectionalLight::GetDirection() const {
  return direction_;
}

}  // namespace renderer
