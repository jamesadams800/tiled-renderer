#include "UserInterface.h"

namespace renderer {

UserInterface::UserInterface()
  : interface_id_counter_{ 0 },
    ui_elements_{}{
}

ElementID UserInterface::AddElement(std::shared_ptr<UiElement> element) {
  element->SetId(interface_id_counter_++);
  ui_elements_.insert(std::make_pair(element->GetId(), element));
  return element->GetId();
}

void UserInterface::DeleteElement(ElementID id) {
  auto found = ui_elements_.find(id);
  if (found != ui_elements_.end()) {
    ui_elements_.erase(found);
  }
}

ElementID UiElement::GetId() const {
  return element_id_;
}

} // namespace renderer
