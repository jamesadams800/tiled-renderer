#include "ShapeFactory.h"

namespace renderer {

std::shared_ptr<Mesh> ShapeFactory::BuildSphereShared(float radius, uint32_t resolution) {
  return std::shared_ptr<Mesh>(BuildSphere(radius, resolution), Object::BaseDeleter());
}

std::shared_ptr<Mesh> ShapeFactory::BuildCuboidShared(float width, float height, float length) {
  return std::shared_ptr<Mesh>(BuildCuboid(width, height, length), Object::BaseDeleter());
}

std::shared_ptr<Mesh> ShapeFactory::BuildPlaneShared(float width, float length) {
  return std::shared_ptr<Mesh>(BuildPlane(width, length), Object::BaseDeleter());
}

Mesh* ShapeFactory::BuildSphere(float radius, uint32_t resolution) {
  std::unique_ptr<VertexBuffer> vertex_buffer = std::make_unique<VertexBuffer>();
  std::vector<float> positions;
  std::vector<float> normals;
  std::vector<float> tex_coords;

  // 1. First build a grid of vertices lying on the surface of the sphere.
  for (uint32_t i = 0; i < resolution + 1; ++i) {
    for (uint32_t j = 0; j < resolution + 1; ++j) {
      const float x_step = static_cast<float>(j) / static_cast<float>(resolution);
      const float y_step = static_cast<float>(i) / static_cast<float>(resolution);

      static constexpr float PI = 3.141592f;
      const float x = cosf(2.0f * PI * x_step) * sinf(PI * y_step);
      const float y = cosf(PI * y_step);
      const float z = sinf(2.0f * PI * x_step) * sinf(PI * y_step);

      positions.push_back(radius * x);
      positions.push_back(radius * y);
      positions.push_back(radius * z);

      normals.push_back(x);
      normals.push_back(y);
      normals.push_back(z);

      tex_coords.push_back(x_step);
      tex_coords.push_back(y_step);
    }
  }
  vertex_buffer->AddVertexAttributeData(VertexBuffer::POSITION, 3, 0, positions);
  vertex_buffer->AddVertexAttributeData(VertexBuffer::NORMAL, 3, 0, normals);
  vertex_buffer->AddVertexAttributeData(VertexBuffer::TEXCOORD, 2, 0, tex_coords);

  std::unique_ptr<IndexBuffer> index_buffer = std::make_unique<IndexBuffer>();
  std::vector<uint32_t> indicies;
  // 2. Build triangles out of the vertices.
  for (uint32_t i = 0; i < resolution; ++i) {
    for (uint32_t j = 0; j < resolution; ++j) {
      // v0---v3
      // |     |
      // v1---v2
      const uint32_t v0 = (i * (resolution + 1)) + j;
      const uint32_t v1 = ((i + 1) * (resolution + 1)) + j;
      const uint32_t v2 = ((i + 1) * (resolution + 1)) + j + 1;
      const uint32_t v3 = (i * (resolution + 1)) + j + 1;

      indicies.push_back(v0);
      indicies.push_back(v2);
      indicies.push_back(v1);

      indicies.push_back(v0);
      indicies.push_back(v3);
      indicies.push_back(v2);
    }
  }
  index_buffer->AddIndexData(indicies);

  return new Mesh(std::move(vertex_buffer), std::move(index_buffer));
}

Mesh* ShapeFactory::BuildPlane(float width, float length) {
  std::unique_ptr<VertexBuffer> vertex_buffer = std::make_unique<VertexBuffer>();
  std::vector<float> positions;
  std::vector<float> normals;
  std::vector<float> tex_coords;

  const float x1 = width / 2.0f;
  const float x2 = -x1;
  const float z1 = length / 2.0f;
  const float z2 = -z1;

  // v0
  positions.push_back(x2);
  positions.push_back(0.0f);
  positions.push_back(z1);

  normals.push_back(0.0f);
  normals.push_back(1.0f);
  normals.push_back(0.0f);

  tex_coords.push_back(1.0f);
  tex_coords.push_back(0.0f);

  // v1
  positions.push_back(x1);
  positions.push_back(0.0f);
  positions.push_back(z2);

  normals.push_back(0.0f);
  normals.push_back(1.0f);
  normals.push_back(0.0f);

  tex_coords.push_back(0.0f);
  tex_coords.push_back(1.0f);

  // v2
  positions.push_back(x2);
  positions.push_back(0.0f);
  positions.push_back(z2);

  normals.push_back(0.0f);
  normals.push_back(1.0f);
  normals.push_back(0.0f);

  tex_coords.push_back(0.0f);
  tex_coords.push_back(0.0f);

  //v3
  positions.push_back(x1);
  positions.push_back(0.0f);
  positions.push_back(z1);

  normals.push_back(0.0f);
  normals.push_back(1.0f);
  normals.push_back(0.0f);

  tex_coords.push_back(1.0f);
  tex_coords.push_back(1.0f);

  vertex_buffer->AddVertexAttributeData(VertexBuffer::POSITION, 3, 0, positions);
  vertex_buffer->AddVertexAttributeData(VertexBuffer::NORMAL, 3, 0, normals);
  vertex_buffer->AddVertexAttributeData(VertexBuffer::TEXCOORD, 2, 0, tex_coords);

  std::unique_ptr<IndexBuffer> index_buffer = std::make_unique<IndexBuffer>();
  std::vector<uint32_t> indicies;

  indicies.push_back(0);
  indicies.push_back(1);
  indicies.push_back(2);

  indicies.push_back(3);
  indicies.push_back(1);
  indicies.push_back(0);

  index_buffer->AddIndexData(indicies);

  return new Mesh(std::move(vertex_buffer), std::move(index_buffer));
}

//TODO: Finish implementing this.
Mesh * ShapeFactory::BuildCuboid(float x, float y, float z) {
  std::unique_ptr<VertexBuffer> vertex_buffer = std::make_unique<VertexBuffer>();
  std::vector<float> positions;
  std::vector<float> normals;
  std::vector<float> tex_coords;

  const float x1 = x / 2.0f;
  const float x2 = -x1;
  const float y1 = y / 2.0f;
  const float y2 = -y1;
  const float z1 = z / 2.0f;
  const float z2 = -z1;

  const glm::vec3 up(0.0f, 1.0f, 0.0f);
  const glm::vec3 down(0.0f, -1.0f, 0.0f);
  const glm::vec3 left(-1.0f, 0.0f, 0.0f);
  const glm::vec3 right(1.0f, 0.0f, 0.0f);
  const glm::vec3 front(0.0f, 0.0f, 1.0f);
  const glm::vec3 back(0.0f, 0.0f, -1.0f);

  const glm::vec3 left_top_back(x2, y1, z2);
  const glm::vec3 left_bottom_back(x2, y2, z2);
  const glm::vec3 right_bottom_back(x1, y2, z2);
  const glm::vec3 right_top_back(x1, y1, z2);
  const glm::vec3 left_top_front(x2, y1, z1);
  const glm::vec3 left_bottom_front(x2, y2, z1);
  const glm::vec3 right_bottom_front(x1, y2, z1);
  const glm::vec3 right_top_front(x1, y1, z1);

  const glm::vec2 uv0(0.0f, 0.0f);
  const glm::vec2 uv1(1.0f, 0.0f);
  const glm::vec2 uv2(0.0f, 1.0f);
  const glm::vec2 uv3(1.0f, 1.0f);

  vertex_buffer->AddVertexAttributeData(VertexBuffer::POSITION, 3, 0, positions);
  vertex_buffer->AddVertexAttributeData(VertexBuffer::NORMAL, 3, 0, normals);
  vertex_buffer->AddVertexAttributeData(VertexBuffer::TEXCOORD, 2, 0, tex_coords);

  return new Mesh(std::move(vertex_buffer));
}

}